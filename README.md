# com-wui-framework-studio v2019.2.0

> Project focused on tools for maintenance of WUI Framework environment and advanced developer tools.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.0.0
Update of WUI Dashboard.
### v2018.0.0
Update of WUI Core.
### v1.0.0
Initial release

## License

This software is owned or controlled by Oidis. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar,
Copyright (c) 2015-2019 [WUI Framework Authors](http://www.wuiframework.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)
