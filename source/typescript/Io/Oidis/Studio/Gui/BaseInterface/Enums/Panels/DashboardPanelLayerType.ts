/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels {
    "use strict";
    import BaseViewerLayerType = Io.Oidis.Gui.Enums.BaseViewerLayerType;

    export class DashboardPanelLayerType extends BaseViewerLayerType {
        public static readonly LOCALHOST_INFO : string = "LocalhostInfo";
        public static readonly TOOLCHAIN_INFO : string = "ToolchainInfo";
        public static readonly PROJECTS : string = "Projects";
    }
}
