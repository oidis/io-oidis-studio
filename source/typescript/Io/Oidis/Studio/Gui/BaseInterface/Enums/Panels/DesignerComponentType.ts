/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels {
    "use strict";

    export class DesignerComponentType extends Io.Oidis.Commons.Primitives.BaseEnum {
        public static readonly PANEL : string = "Panel";
        public static readonly COMPONENT : string = "Component";
        public static readonly USER_CONTROL : string = "UserControl";
    }
}
