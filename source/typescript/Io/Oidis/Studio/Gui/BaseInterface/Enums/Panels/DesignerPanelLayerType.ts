/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels {
    "use strict";
    import BaseViewerLayerType = Io.Oidis.Gui.Enums.BaseViewerLayerType;

    export class DesignerPanelLayerType extends BaseViewerLayerType {
        public static readonly EDITOR : string = "Editor";
        public static readonly COMPONENTS : string = "Components";
        public static readonly CONSOLE : string = "Console";
        public static readonly HIERARCHY : string = "Hierarchy";
        public static readonly PREVIEW : string = "Preview";
        public static readonly PICK_AREA : string = "PickArea";
        public static readonly SELECT_AREA : string = "SelectArea";
        public static readonly DRAG_AREA : string = "DragArea";
    }
}
