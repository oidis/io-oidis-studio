/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Button = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.Button;
    import LocalhostInfoPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard.LocalhostInfoPanelViewer;
    import ToolchainInfoPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard.ToolchainInfoPanelViewer;
    import DashboardPanelViewerArgs = Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.DashboardPanelViewerArgs;
    import ProjectsManagerPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard.ProjectsManagerPanelViewer;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import ImageButton = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.ImageButton;
    import ImageButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.IconType;

    export class DashboardPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public localhostInfoButton : Button;
        public toolchainInfoButton : Button;
        public projectsButton : Button;
        public backButton : ImageButton;
        public headerLabel : Label;
        public localhostInfo : LocalhostInfoPanel;
        public toolchainInfo : ToolchainInfoPanel;
        public projects : ProjectsManagerPanel;

        constructor($id? : string) {
            super($id);

            this.localhostInfoButton = new Button();
            this.toolchainInfoButton = new Button();
            this.projectsButton = new Button();
            this.backButton = new ImageButton(ImageButtonType.ROUND);
            this.headerLabel = new Label();

            this.addChildPanel(LocalhostInfoPanelViewer, null,
                ($parent : DashboardPanel, $child : LocalhostInfoPanel) : void => {
                    $parent.localhostInfo = $child;
                });

            this.addChildPanel(ToolchainInfoPanelViewer, null,
                ($parent : DashboardPanel, $child : ToolchainInfoPanel) : void => {
                    $parent.toolchainInfo = $child;
                });

            this.addChildPanel(ProjectsManagerPanelViewer, null,
                ($parent : DashboardPanel, $child : ProjectsManagerPanel) : void => {
                    $parent.projects = $child;
                });
        }

        public Value($value? : DashboardPanelViewerArgs) : DashboardPanelViewerArgs {
            return <DashboardPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.localhostInfoButton.Width(800);
            this.toolchainInfoButton.Width(300);
            this.toolchainInfoButton.StyleClassName("Toolchain");
            this.projectsButton.Width(300);
            this.projectsButton.StyleClassName("Projects");
            this.backButton.IconName(IconType.ARROW_LEFT);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement(this.Id() + "_Header").Visible(false).StyleClassName("Header")
                    .Add(this.backButton)
                    .Add(this.headerLabel)
                    .Add(this.addElement().StyleClassName("Splitter"))
                )
                .Add(this.addElement(this.Id() + "_Logo").StyleClassName("Logo"))
                .Add(this.addElement(this.Id() + "_Buttons").StyleClassName("Buttons")
                    .Add(this.projectsButton)
                    .Add(this.localhostInfoButton)
                    .Add(this.toolchainInfoButton)
                )
                .Add(this.localhostInfo)
                .Add(this.toolchainInfo)
                .Add(this.projects);
        }
    }
}
