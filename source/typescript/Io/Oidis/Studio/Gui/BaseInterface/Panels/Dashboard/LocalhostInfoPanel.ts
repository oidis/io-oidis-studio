/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import LocalhostInfoPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.LocalhostInfoPanelViewerArgs;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;

    export class LocalhostInfoPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public localhostSwitch : Button;
        public location : FormInput;
        public serverRoot : FormInput;
        public serverLogs : FormInput;
        public configFiles : FormInput;
        public certsUpdate : Button;

        constructor($id? : string) {
            super($id);

            this.localhostSwitch = new Button();
            this.location = new FormInput(new DropDownListFormArgs());
            this.serverRoot = new FormInput(new TextFieldFormArgs());
            this.serverLogs = new FormInput(new TextFieldFormArgs());
            this.configFiles = new FormInput(new DropDownListFormArgs());
            this.certsUpdate = new Button();
        }

        public Value($value? : LocalhostInfoPanelViewerArgs) : LocalhostInfoPanelViewerArgs {
            return <LocalhostInfoPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.localhostSwitch.Width(800);
            this.location.Configuration().Width(800);
            this.serverRoot.Configuration().Width(800);
            (<TextFieldFormArgs>this.serverRoot.Configuration()).ReadOnly(true);
            this.serverLogs.Configuration().Width(800);
            (<TextFieldFormArgs>this.serverLogs.Configuration()).ReadOnly(true);
            this.configFiles.Configuration().Width(800);
            this.certsUpdate.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.certsUpdate.Width(800);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.localhostSwitch)
                .Add(this.location)
                .Add(this.serverRoot)
                .Add(this.serverLogs)
                .Add(this.configFiles)
                .Add(this.certsUpdate);
        }
    }
}
