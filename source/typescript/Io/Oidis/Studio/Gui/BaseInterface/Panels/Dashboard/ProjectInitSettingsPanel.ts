/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import ProjectInitSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectInitSettingsPanelViewerArgs;

    export class ProjectInitSettingsPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public namespaceField : FormInput;
        public extendsList : FormInput;
        public targetField : FormInput;
        public nameField : FormInput;
        public titleField : FormInput;
        public descriptionField : FormInput;
        public repositoryField : FormInput;

        constructor($id? : string) {
            super($id);

            this.namespaceField = new FormInput(new TextFieldFormArgs());
            this.extendsList = new FormInput(new DropDownListFormArgs());
            this.targetField = new FormInput(new TextFieldFormArgs());
            this.nameField = new FormInput(new TextFieldFormArgs());
            this.titleField = new FormInput(new TextFieldFormArgs());
            this.descriptionField = new FormInput(new TextFieldFormArgs());
            this.repositoryField = new FormInput(new TextFieldFormArgs());
        }

        public Value($value? : ProjectInitSettingsPanelViewerArgs) : ProjectInitSettingsPanelViewerArgs {
            return <ProjectInitSettingsPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.namespaceField.Configuration().Width(800);
            this.extendsList.Configuration().Width(800);
            this.targetField.Configuration().Width(800);
            this.nameField.Configuration().Width(800);
            this.titleField.Configuration().Width(800);
            this.descriptionField.Configuration().Width(800);
            this.repositoryField.Configuration().Width(800);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.nameField)
                .Add(this.namespaceField)
                .Add(this.extendsList)
                .Add(this.targetField)
                .Add(this.titleField)
                .Add(this.descriptionField)
                .Add(this.repositoryField);
        }
    }
}
