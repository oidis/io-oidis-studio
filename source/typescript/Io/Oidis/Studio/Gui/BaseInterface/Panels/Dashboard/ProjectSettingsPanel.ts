/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import FitToParent = Io.Oidis.Gui.Enums.FitToParent;
    import Alignment = Io.Oidis.Gui.Enums.Alignment;
    import Button = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import IResponsiveElement = Io.Oidis.Gui.Interfaces.Primitives.IResponsiveElement;
    import BaseFormInputArgs = Io.Oidis.UserControls.Structures.BaseFormInputArgs;
    import ProjectSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectSettingsPanelViewerArgs;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    export class ProjectSettingsPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public save : Button;
        private fields : string[];
        private args : ProjectSettingsPanelViewerArgs;

        constructor($id? : string) {
            super($id);
            this.fields = [];
            this.args = new ProjectSettingsPanelViewerArgs();
            this.save = new Button(ButtonType.GRAY_MEDIUM);
        }

        public Value($value? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value)) {
                this.args = $value;
                $value.Items().foreach(($arg : BaseFormInputArgs) : void => {
                    const name : string = $arg.Name();
                    if (!this.hasOwnProperty(name)) {
                        $arg.Width(750);
                        this[name] = new FormInput($arg);
                        this.fields.push(name);
                    }
                });
            } else {
                this.args.Items().Clear();
                this.fields.forEach(($name : string) : void => {
                    const input : FormInput = this[$name];
                    input.Configuration().Value(input.Value());
                    this.args.Items().Add(input.Configuration());
                });
            }
            return this.args;
        }

        protected innerCode() : IGuiElement {
            this.Width(1024);
            this.Height(800);
            this.Scrollable(true);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const html : IResponsiveElement =
                this.addColumn()
                    .WidthOfColumn("100%")
                    .HeightOfRow("25px", true)
                    .FitToParent(FitToParent.FULL)
                    .Alignment(Alignment.TOP_PROPAGATED);
            this.fields.forEach(($name : string) : void => {
                html.Add(this[$name]);
            });
            html.Add(this.addRow()
                .Alignment(Alignment.CENTER_PROPAGATED).FitToParent(FitToParent.HORIZONTAL)
                .Add(this.save)
            );
            return this.addRow().Add(html);
        }
    }
}
