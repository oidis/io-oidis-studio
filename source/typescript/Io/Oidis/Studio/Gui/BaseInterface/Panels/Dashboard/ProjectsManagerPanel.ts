/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import ProjectsManagerPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectsManagerPanelViewerArgs;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import BasePanel = Io.Oidis.UserControls.Primitives.BasePanel;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class ProjectsManagerPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public remoteSources : FormInput;
        public workplacePath : FormInput;
        public installProjectButton : Button;
        public localSources : FormInput;
        private readonly console : BasePanel;

        constructor($id? : string) {
            super($id);

            this.remoteSources = new FormInput(new DropDownListFormArgs());
            this.workplacePath = new FormInput(new DropDownListFormArgs());
            this.installProjectButton = new Button();
            this.localSources = new FormInput(new DropDownListFormArgs());
            this.console = new BasePanel();
        }

        public Value($value? : ProjectsManagerPanelViewerArgs) : ProjectsManagerPanelViewerArgs {
            return <ProjectsManagerPanelViewerArgs>super.Value($value);
        }

        public ConsoleVisible($value : boolean) : void {
            if ($value) {
                ElementManager.Show(this.Id() + "_Console");
                this.console.Visible(true);
            } else {
                ElementManager.Hide(this.Id() + "_Console");
                this.console.Visible(false);
            }
        }

        public PrintToConsole($message : string) : void {
            this.getEvents().FireAsynchronousMethod(() : void => {
                if (StringUtils.StartsWith($message, "\"") && StringUtils.EndsWith($message, "\"")) {
                    $message = StringUtils.Substring($message, 1, StringUtils.Length($message) - 1);
                }
                $message = StringUtils.Replace($message, "<", "&lt;");
                $message = StringUtils.Replace($message, ">", "&gt;");
                $message = StringUtils.Replace($message, "\\r\\n", "\\n");
                $message = StringUtils.Replace($message, "\\n", StringUtils.NewLine());
                $message = StringUtils.Replace($message, "\\\"", "\"");

                ElementManager.getElement(this.console.Id() + "_PanelContent").innerHTML += $message;
                ElementManager.ClearCssProperty(this.console.Id() + "_PanelContent", "min-height");
                BasePanel.scrollTop(this.console, 100);

                $message = StringUtils.Replace($message, "&lt;", "<");
                $message = StringUtils.Replace($message, "&gt;", ">");
                $message = StringUtils.StripTags($message);
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    LogIt.Debug($message);
                }
            }, false);
        }

        protected innerCode() : IGuiElement {
            this.remoteSources.Configuration().Width(800);
            this.workplacePath.Configuration().Width(800);
            this.installProjectButton.Width(800);
            this.localSources.Configuration().Width(800);

            this.console.Visible(false);
            this.console.Scrollable(true);
            this.console.StyleClassName("Console");
            this.console.Width(760);
            this.console.Height(360);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.workplacePath)
                .Add(this.remoteSources)
                .Add(this.installProjectButton)
                .Add(this.localSources)
                .Add(this.addElement(this.Id() + "_Console").StyleClassName("ConsoleEnvelop").Visible(false)
                    .Add(this.console)
                );
        }
    }
}
