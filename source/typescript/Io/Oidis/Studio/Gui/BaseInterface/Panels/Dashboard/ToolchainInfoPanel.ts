/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import ToolchainInfoPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ToolchainInfoPanelViewerArgs;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import BasePanel = Io.Oidis.UserControls.Primitives.BasePanel;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;

    export class ToolchainInfoPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public checkButton : Button;
        public headerLabel : Label;
        private readonly content : BasePanel;

        constructor($id? : string) {
            super($id);

            this.checkButton = new Button();
            this.headerLabel = new Label();
            this.content = new BasePanel();
        }

        public Value($value? : ToolchainInfoPanelViewerArgs) : ToolchainInfoPanelViewerArgs {
            return <ToolchainInfoPanelViewerArgs>super.Value($value);
        }

        public setContent($value : string) : void {
            if (this.IsCompleted()) {
                ElementManager.setInnerHtml(this.content.Id() + "_PanelContent",
                    "<div style=\"width: 770px; padding: 10px;\">" + $value + "</div>");
                ElementManager.ClearCssProperty(this.content.Id() + "_PanelContent", "min-height");
            }
        }

        protected innerCode() : IGuiElement {
            this.checkButton.Width(800);
            this.content.StyleClassName("Info");
            this.content.Scrollable(true);
            this.content.Width(800);
            this.content.Height(325);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.checkButton)
                .Add(this.headerLabel)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(this.content);
        }
    }
}
