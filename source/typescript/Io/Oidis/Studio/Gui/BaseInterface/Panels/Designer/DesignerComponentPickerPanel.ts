/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import Size = Io.Oidis.Gui.Structures.Size;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import GuiCommons = Io.Oidis.Gui.Primitives.GuiCommons;
    import AbstractGuiObject = Io.Oidis.UserControls.Primitives.AbstractGuiObject;
    import IDesignerInstanceMapItem = Io.Oidis.Gui.Interfaces.Designer.IDesignerInstanceMapItem;
    import DesignerProtocolConstants = Io.Oidis.Gui.Enums.DesignerProtocolConstants;
    import GeneralCssNames = Io.Oidis.Gui.Enums.GeneralCssNames;
    import DesignerComponentPickerPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerComponentPickerPanelViewerArgs;

    export class DesignerComponentPickerPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        private readonly components : ArrayList<GuiCommons>;
        private readonly selected : ArrayList<GuiCommons>;

        constructor($id? : string) {
            super($id);

            this.components = new ArrayList<GuiCommons>();
            this.selected = new ArrayList<GuiCommons>();
        }

        public setStructure($data : IDesignerInstanceMapItem[], $zoom : number) : void {
            this.components.Clear();
            if (!ObjectValidator.IsEmptyOrNull($data)) {
                $zoom = $zoom / 100;
                const dataLookup : ($data : IDesignerInstanceMapItem[]) => void = ($data : IDesignerInstanceMapItem[]) : void => {
                    $data.forEach(($item : IDesignerInstanceMapItem) : void => {
                        if ($item.name !== DesignerProtocolConstants.ROOT_ELEMENT_VARIABLE) {
                            const newItem : AbstractGuiObject = new AbstractGuiObject();
                            newItem.InstanceOwner(this.InstanceOwner());
                            newItem.Parent(this);
                            newItem.Visible(false);
                            newItem.StyleClassName("ItemHolder");
                            newItem.StyleAttributes("display: none;");
                            if (ObjectValidator.IsSet($item.top)) {
                                newItem.getEvents().setOnComplete(() : void => {
                                    ElementManager.setCssProperty(newItem, "top", $item.top * $zoom);
                                    ElementManager.setCssProperty(newItem, "left", $item.left * $zoom);
                                    ElementManager.setSize(newItem, $item.width * $zoom, $item.height * $zoom);
                                });
                            }
                            this.components.Add(newItem, $item.id);
                            if (ObjectValidator.IsSet($item.map)) {
                                dataLookup(<IDesignerInstanceMapItem[]>$item.map);
                            }
                        } else if (ObjectValidator.IsSet($item.map)) {
                            dataLookup(<IDesignerInstanceMapItem[]>$item.map);
                        }
                    });
                };
                dataLookup($data);
            }
            if (this.IsCompleted() && this.Visible()) {
                this.showStructure();
            }
        }

        public getItems() : ArrayList<GuiCommons> {
            return this.components;
        }

        public SelectItem($element : GuiCommons) : void {
            if (this.components.Contains($element)) {
                if ($element.StyleClassName() === GeneralCssNames.ACTIVE) {
                    $element.StyleClassName("ItemHolder");
                    this.selected.RemoveAt(this.selected.IndexOf($element));
                } else {
                    $element.StyleClassName(GeneralCssNames.ACTIVE);
                    this.selected.Add($element, $element.Id());
                }
            }
        }

        public SelectArea($topX : number, $topY : number, $bottomX : number, $bottomY : number) : void {
            this.components.foreach(($item : GuiCommons) : void => {
                const element : HTMLElement = ElementManager.getElement($item);
                const size : Size = $item.getSize();
                const itemTopX : number = element.offsetLeft;
                const itemTopY : number = element.offsetTop;
                const itemBottomX : number = element.offsetLeft + size.Width();
                const itemBottomY : number = element.offsetTop + size.Height();

                if ($topY <= itemBottomY && $bottomY >= itemTopY && $topX <= itemBottomX && $bottomX >= itemTopX) {
                    $item.StyleClassName(GeneralCssNames.ACTIVE);
                    this.selected.Add($item, $item.Id());
                }
            });
        }

        public Clear() : void {
            this.components.foreach(($item : GuiCommons) : void => {
                $item.StyleClassName("ItemHolder");
            });
            this.selected.Clear();
        }

        public getSelectedItems() : ArrayList<GuiCommons> {
            return this.selected;
        }

        public Value($value? : DesignerComponentPickerPanelViewerArgs) : DesignerComponentPickerPanelViewerArgs {
            return <DesignerComponentPickerPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnComplete(() : void => {
                this.showStructure();
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Items");
        }

        private showStructure() : void {
            const target : HTMLElement = ElementManager.getElement(this.Id() + "_Items");
            ElementManager.setInnerHtml(target, "");
            this.components.foreach(($item : GuiCommons) : void => {
                target.appendChild(this.addElement().Add($item).ToDOMElement(this.outputEndOfLine));
                $item.Visible(true);
            });
        }
    }
}
