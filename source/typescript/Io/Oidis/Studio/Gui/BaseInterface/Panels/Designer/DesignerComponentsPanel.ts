/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Reflection = Io.Oidis.Commons.Utils.Reflection;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import DropDownList = Io.Oidis.UserControls.BaseInterface.UserControls.DropDownList;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import WindowManager = Io.Oidis.Gui.Utils.WindowManager;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import GuiObjectManager = Io.Oidis.Gui.GuiObjectManager;
    import DesignerComponentType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DesignerComponentType;
    import EventArgs = Io.Oidis.Commons.Events.Args.EventArgs;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;
    import DragBar = Io.Oidis.UserControls.BaseInterface.Components.DragBar;
    import MoveEventArgs = Io.Oidis.Gui.Events.Args.MoveEventArgs;
    import ElementOffset = Io.Oidis.Gui.Structures.ElementOffset;
    import Size = Io.Oidis.Gui.Structures.Size;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import DesignerComponentsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerComponentsPanelViewerArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DesignerComponentsPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public userControlsTypeList : DropDownList;
        public userControlsGroupList : DropDownList;
        public addButton : Button;
        public deleteButton : Button;
        public dragElement : DragBar;
        private componentsList : ArrayList<ArrayList<string>>;
        private userControlsItems : ArrayList<Button>;
        private selected : Button;
        private dragOffset : ElementOffset;

        private static onDragStartEndEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                const element : DragBar = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    const parent : DesignerComponentsPanel = <DesignerComponentsPanel>element.Parent();
                    if ($reflection.IsMemberOf(parent, DesignerComponentsPanel)) {
                        parent.dragOffset = ElementManager.getAbsoluteOffset(element);
                    }
                }
            }
        }

        private static onDragChangeEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                const element : DragBar = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    const parent : DesignerComponentsPanel = <DesignerComponentsPanel>element.Parent();
                    if ($reflection.IsMemberOf(parent, DesignerComponentsPanel)) {
                        const windowSize : Size = WindowManager.getSize();
                        let top : number = parent.dragOffset.Top() + $eventArgs.getDistanceY();
                        let left : number = parent.dragOffset.Left() + $eventArgs.getDistanceX();

                        if (left < 10) {
                            left = 10;
                        }
                        if (top < 10) {
                            top = 10;
                        }
                        if (300 + left > windowSize.Width() - 10) {
                            left = windowSize.Width() - 290;
                        }
                        if (30 + top > windowSize.Height() - 10) {
                            top = windowSize.Height() - 20;
                        }

                        ElementManager.setCssProperty(element, "top", top);
                        ElementManager.setCssProperty(element, "left", left);
                    }
                }
            }
        }

        constructor($id? : string) {
            super($id);

            this.componentsList = new ArrayList<ArrayList<string>>();
            this.componentsList.Add(new ArrayList<string>(), DesignerComponentType.PANEL);
            this.componentsList.Add(new ArrayList<string>(), DesignerComponentType.USER_CONTROL);
            this.componentsList.Add(new ArrayList<string>(), DesignerComponentType.COMPONENT);
            this.userControlsItems = new ArrayList<Button>();
            this.selected = null;

            this.userControlsTypeList = new DropDownList();
            this.userControlsGroupList = new DropDownList();
            this.addButton = new Button();
            this.deleteButton = new Button();
            this.dragElement = new DragBar();
            this.dragOffset = new ElementOffset();
        }

        public Value($value? : DesignerComponentsPanelViewerArgs) : DesignerComponentsPanelViewerArgs {
            return <DesignerComponentsPanelViewerArgs>super.Value($value);
        }

        public Add($items : ArrayList<string>, $type : string) : void {
            this.componentsList.Add($items, $type);
        }

        public Clear() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            this.userControlsItems.foreach(($item : Button) : void => {
                manager.Clear($item);
            });
            this.userControlsItems.Clear();
            this.hideComponents();
        }

        public SelectType($type : DesignerComponentType) : void {
            this.userControlsGroupList.Clear();
            this.userControlsGroupList.Height(-1);
            this.userControlsGroupList.MaxVisibleItemsCount(10);

            const items : ArrayList<string> = new ArrayList<string>();
            this.componentsList.getItem($type + "").foreach(($className : string) : void => {
                const namespaceName : string = StringUtils.Substring($className, 0, StringUtils.IndexOf($className, ".", false));
                if (!items.Contains(namespaceName)) {
                    items.Add(namespaceName);
                    this.userControlsGroupList.Add(this.wrapItemText(namespaceName), namespaceName);
                }
            });

            this.hideComponents();
        }

        public SelectGroup($name : string) : void {
            this.hideComponents();
            const type : string = this.userControlsTypeList.Value();
            ElementManager.Show(this.Id() + "_" + type);

            if (this.IsCompleted() && ObjectValidator.IsEmptyOrNull(ElementManager.getInnerHtml(this.Id() + "_" + type))) {
                const target : HTMLElement = ElementManager.getElement(this.Id() + "_" + type);
                this.componentsList.getItem(type + "").foreach(($className : string) : void => {
                    if (StringUtils.Contains($className, $name)) {
                        const newItem : Button = new Button();
                        newItem.getGuiOptions().Add(GuiOptionType.SELECTED);
                        newItem.Width(300);
                        newItem.InstanceOwner(this.InstanceOwner());
                        newItem.Parent(this);
                        newItem.Text(StringUtils.Substring($className, StringUtils.IndexOf($className, ".", false) + 1));
                        newItem.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                            const item : Button = <Button>$eventArgs.Owner();
                            item.IsSelected(true);
                            ElementManager.setPosition(this.dragElement, item.getScreenPosition());
                            this.dragElement.Visible(true);
                            const selected : Button = (<DesignerComponentsPanel>item.Parent()).selected;
                            if (!ObjectValidator.IsEmptyOrNull(selected)) {
                                selected.IsSelected(false);
                            }
                            (<DesignerComponentsPanel>item.Parent()).selected = item;
                            this.addButton.Enabled(true);
                        });
                        target.appendChild(this.addElement().Add(newItem).ToDOMElement(this.outputEndOfLine));
                        newItem.Visible(true);
                        this.userControlsItems.Add(newItem);
                    }
                });
            }
        }

        public getSelected() : Button {
            return this.selected;
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.userControlsTypeList.Width(300);
            this.userControlsTypeList.MaxVisibleItemsCount(10);
            this.userControlsGroupList.Width(300);
            this.userControlsGroupList.MaxVisibleItemsCount(10);

            this.dragElement.getEvents().setOnDragStart(DesignerComponentsPanel.onDragStartEndEventHandler);
            this.dragElement.getEvents().setOnDragChange(DesignerComponentsPanel.onDragChangeEventHandler);
            this.dragElement.getEvents().setOnDragComplete(DesignerComponentsPanel.onDragStartEndEventHandler);
            this.dragElement.Visible(false);

            this.addButton.Width(300);
            this.addButton.Enabled(false);
            this.deleteButton.Width(300);
            this.deleteButton.Enabled(false);

            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                ElementManager.setHeight($eventArgs.Owner().Id() + "_Items", $eventArgs.Owner().Height() - 180);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.userControlsTypeList)
                .Add(this.userControlsGroupList)
                .Add(this.addElement(this.Id() + "_Items").StyleClassName("Items")
                    .Add(this.addElement(this.Id() + "_" + DesignerComponentType.PANEL).Visible(false))
                    .Add(this.addElement(this.Id() + "_" + DesignerComponentType.COMPONENT).Visible(false))
                    .Add(this.addElement(this.Id() + "_" + DesignerComponentType.USER_CONTROL).Visible(false))
                )
                .Add(this.dragElement)
                .Add(this.addElement().StyleClassName("ManageButtons")
                    .Add(this.addButton)
                    .Add(this.deleteButton)
                );
        }

        private wrapItemText($input : string, $maxNameLength : number = 32) : string {
            if (StringUtils.Length($input) > $maxNameLength) {
                $input = StringUtils.Substring($input, 0, $maxNameLength) + " " + StringUtils.Substring($input, $maxNameLength);
            }
            return $input;
        }

        private hideComponents() : void {
            if (this.IsCompleted()) {
                this.dragElement.Visible(false);
                this.addButton.Enabled(false);
                ElementManager.setInnerHtml(this.Id() + "_" + DesignerComponentType.PANEL, "");
                ElementManager.setInnerHtml(this.Id() + "_" + DesignerComponentType.COMPONENT, "");
                ElementManager.setInnerHtml(this.Id() + "_" + DesignerComponentType.USER_CONTROL, "");
            }
        }
    }
}
