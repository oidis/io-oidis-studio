/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Reflection = Io.Oidis.Commons.Utils.Reflection;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import EventArgs = Io.Oidis.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Io.Oidis.Gui.GuiObjectManager;
    import DesignerConsolePanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerConsolePanelViewerArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DesignerConsolePanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public contentLabel : Label;
        private contentText : string;

        private static onChangeEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void {
            let parent : DesignerConsolePanel = <DesignerConsolePanel>$eventArgs.Owner();
            if ($reflection.IsInstanceOf(parent, Label)) {
                parent = <DesignerConsolePanel>(<Label>$eventArgs.Owner()).Parent();
                DesignerConsolePanel.scrollTop(parent, 100);
                DesignerConsolePanel.ContentFocusHandler($eventArgs, $manager, $reflection);
            }
        }

        constructor($id? : string) {
            super($id);

            this.contentLabel = new Label();
            this.contentText = "";
        }

        public Value($value? : DesignerConsolePanelViewerArgs) : DesignerConsolePanelViewerArgs {
            return <DesignerConsolePanelViewerArgs>super.Value($value);
        }

        public Append($value : string) : void {
            $value = StringUtils.Replace($value, StringUtils.NewLine(), "##__EOL__##");
            $value = StringUtils.Replace($value, "<", "&lt;");
            $value = StringUtils.Replace($value, ">", "&gt;");
            $value = StringUtils.Replace($value, "##__EOL__##", StringUtils.NewLine());
            $value = StringUtils.Replace($value, StringUtils.NewLine(false), StringUtils.NewLine());
            this.contentText += $value;
            this.contentLabel.Text(this.contentText);
        }

        public Clear() : void {
            this.contentText = "";
            this.contentLabel.Text("");
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.getEvents().setOnDoubleClick(() : void => {
                this.Visible(false);
            });

            this.contentLabel.getEvents().setOnChange(DesignerConsolePanel.onChangeEventHandler);
            this.getEvents().setOnShow(DesignerConsolePanel.onChangeEventHandler);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.contentLabel);
        }
    }
}
