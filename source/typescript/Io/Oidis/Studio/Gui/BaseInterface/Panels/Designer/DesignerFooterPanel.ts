/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;
    import DesignerFooterPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerFooterPanelViewerArgs;

    export class DesignerFooterPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public componentsButton : Button;
        public hierarchyButton : Button;
        public consoleButton : Button;
        public pickerButton : Button;
        public selectButton : Button;
        public previewButton : Button;

        constructor($id? : string) {
            super($id);

            this.componentsButton = new Button();
            this.hierarchyButton = new Button();
            this.consoleButton = new Button();
            this.pickerButton = new Button();
            this.selectButton = new Button();
            this.previewButton = new Button();
        }

        public Value($value? : DesignerFooterPanelViewerArgs) : DesignerFooterPanelViewerArgs {
            return <DesignerFooterPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Height(40);
            this.componentsButton.getGuiOptions().Add(GuiOptionType.SELECTED);
            this.hierarchyButton.getGuiOptions().Add(GuiOptionType.SELECTED);
            this.consoleButton.getGuiOptions().Add(GuiOptionType.SELECTED);
            this.selectButton.getGuiOptions().Add(GuiOptionType.SELECTED);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.componentsButton)
                .Add(this.hierarchyButton)
                .Add(this.consoleButton)
                .Add(this.previewButton)
                .Add(this.pickerButton)
                .Add(this.selectButton);
        }
    }
}
