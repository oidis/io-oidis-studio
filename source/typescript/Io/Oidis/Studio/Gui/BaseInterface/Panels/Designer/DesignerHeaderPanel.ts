/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;
    import ButtonType = Io.Oidis.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import NumberPicker = Io.Oidis.UserControls.BaseInterface.UserControls.NumberPicker;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import DropDownList = Io.Oidis.UserControls.BaseInterface.UserControls.DropDownList;
    import DesignerHeaderPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerHeaderPanelViewerArgs;

    export class DesignerHeaderPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public backButton : Button;
        public saveButton : Button;
        public layerLabel : Label;
        public layerList : DropDownList;
        public zoomLabel : Label;
        public zoom : NumberPicker;

        constructor($id? : string) {
            super($id);

            this.backButton = new Button();
            this.saveButton = new Button(ButtonType.GREEN);
            this.layerLabel = new Label();
            this.layerList = new DropDownList();
            this.zoomLabel = new Label();
            this.zoom = new NumberPicker();
        }

        public Value($value? : DesignerHeaderPanelViewerArgs) : DesignerHeaderPanelViewerArgs {
            return <DesignerHeaderPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Height(40);
            this.backButton.Width(250);
            this.backButton.StyleClassName("Back");

            this.saveButton.Width(250);
            this.saveButton.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.saveButton.StyleClassName("Save");

            this.layerList.Width(250);

            this.zoom.Width(250);
            this.zoom.RangeStart(25);
            this.zoom.RangeEnd(200);
            this.zoom.Value(100);
            this.zoom.Text("{0} %");
            this.zoom.DecimalPlaces(0);
            this.zoom.ValueStep(5);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.backButton)
                .Add(this.saveButton)
                .Add(this.addElement().StyleClassName("Zoom")
                    .Add(this.zoomLabel)
                    .Add(this.zoom)
                )
                .Add(this.addElement().StyleClassName("Layers")
                    .Add(this.layerLabel)
                    .Add(this.layerList)
                );
        }
    }
}
