/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import GuiCommons = Io.Oidis.Gui.Primitives.GuiCommons;
    import IDesignerInstanceMapItem = Io.Oidis.Gui.Interfaces.Designer.IDesignerInstanceMapItem;
    import DesignerProtocolConstants = Io.Oidis.Gui.Enums.DesignerProtocolConstants;
    import GeneralCssNames = Io.Oidis.Gui.Enums.GeneralCssNames;
    import DesignerHierarchyPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerHierarchyPanelViewerArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DesignerHierarchyPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        private readonly hierarchyItems : ArrayList<GuiCommons>;
        private selectedItem : GuiCommons;

        constructor($id? : string) {
            super($id);

            this.hierarchyItems = new ArrayList<GuiCommons>();
            this.selectedItem = null;
        }

        public Value($value? : DesignerHierarchyPanelViewerArgs) : DesignerHierarchyPanelViewerArgs {
            return <DesignerHierarchyPanelViewerArgs>super.Value($value);
        }

        public setStructure($data : IDesignerInstanceMapItem[]) : void {
            this.hierarchyItems.Clear();
            this.selectedItem = null;
            if (!ObjectValidator.IsEmptyOrNull($data)) {
                const dataLookup : ($data : IDesignerInstanceMapItem[], $prefix : string) => void =
                    ($data : IDesignerInstanceMapItem[], $prefix : string) : void => {
                        $data.forEach(($item : IDesignerInstanceMapItem) : void => {
                            const newItem : Label = new Label();
                            newItem.InstanceOwner(this.InstanceOwner());
                            newItem.Parent(this);
                            if (ObjectValidator.IsSet($item.name)) {
                                if ($item.name !== DesignerProtocolConstants.ROOT_ELEMENT_VARIABLE) {
                                    newItem.Text($prefix + $item.name);
                                } else {
                                    newItem.Text("root element");
                                    newItem.StyleClassName(GeneralCssNames.ACTIVE);
                                }
                            } else {
                                newItem.Text($prefix + $item.type + " (" + $item.id + ")");
                            }
                            this.hierarchyItems.Add(newItem, $item.id);

                            if (ObjectValidator.IsSet($item.map)) {
                                dataLookup(<IDesignerInstanceMapItem[]>$item.map, $prefix + StringUtils.Space(4));
                            }
                        });
                    };
                dataLookup($data, "");
            }
            if (this.IsCompleted()) {
                this.showStructure();
            }
        }

        public getItems() : ArrayList<GuiCommons> {
            return this.hierarchyItems;
        }

        public SelectItem($element : GuiCommons) : void {
            if (this.hierarchyItems.Contains($element)) {
                if (!ObjectValidator.IsEmptyOrNull(this.selectedItem)) {
                    this.selectedItem.StyleClassName("");
                    this.selectedItem = null;
                }
                if ($element.StyleClassName() === GeneralCssNames.ACTIVE) {
                    $element.StyleClassName("");
                } else {
                    $element.StyleClassName(GeneralCssNames.ACTIVE);
                    this.selectedItem = $element;
                }
            }
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.getEvents().setOnComplete(() : void => {
                this.showStructure();
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Items");
        }

        private showStructure() : void {
            const target : HTMLElement = ElementManager.getElement(this.Id() + "_Items");
            ElementManager.setInnerHtml(target, "");
            if (!this.hierarchyItems.IsEmpty()) {
                this.hierarchyItems.foreach(($item : GuiCommons) : void => {
                    target.appendChild(this.addElement().Add($item).ToDOMElement(this.outputEndOfLine));
                    $item.Visible(true);
                    if ($item.StyleClassName() === GeneralCssNames.ACTIVE) {
                        this.selectedItem = $item;
                    }
                });
            }
        }
    }
}
