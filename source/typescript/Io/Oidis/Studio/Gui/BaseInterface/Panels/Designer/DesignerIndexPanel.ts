/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DropDownList = Io.Oidis.UserControls.BaseInterface.UserControls.DropDownList;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import TextField = Io.Oidis.UserControls.BaseInterface.UserControls.TextField;
    import DirectoryBrowser = Io.Oidis.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;
    import FileSystemFilter = Io.Oidis.Commons.Utils.FileSystemFilter;
    import DesignerIndexPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerIndexPanelViewerArgs;

    export class DesignerIndexPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public targetField : TextField;
        public browseButton : Button;
        public directoryBrowser : DirectoryBrowser;
        public selectButton : Button;
        public componentsLabel : Label;
        public componentsList : DropDownList;
        public testsLabel : Label;
        public testsList : DropDownList;
        public blankButton : Button;

        constructor($id? : string) {
            super($id);

            this.targetField = new TextField();
            this.browseButton = new Button();
            this.directoryBrowser = new DirectoryBrowser();
            this.selectButton = new Button();
            this.componentsLabel = new Label();
            this.componentsList = new DropDownList();
            this.testsLabel = new Label();
            this.testsList = new DropDownList();
            this.blankButton = new Button();
        }

        public Value($value? : DesignerIndexPanelViewerArgs) : DesignerIndexPanelViewerArgs {
            return <DesignerIndexPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.targetField.Width(650);
            this.targetField.ReadOnly(true);

            this.browseButton.Width(150);
            this.browseButton.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.browseButton.getEvents().setOnClick(() : void => {
                this.browseButton.Enabled(false);
                this.directoryBrowser.Visible(true);
                this.selectButton.Visible(true);
                this.componentsLabel.Visible(false);
                this.componentsList.Visible(false);
                this.testsLabel.Visible(false);
                this.testsList.Visible(false);
                this.blankButton.Visible(false);
            });

            this.directoryBrowser.Width(800);
            this.directoryBrowser.Height(300);
            this.directoryBrowser.Visible(false);
            this.directoryBrowser.Filter(FileSystemFilter.DIRECTORIES);
            this.directoryBrowser.Path("C:/");
            this.directoryBrowser.getEvents().setOnChange(() : void => {
                this.targetField.Value(this.directoryBrowser.Value());
            });

            this.selectButton.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.selectButton.Visible(false);
            this.selectButton.Width(800);
            this.selectButton.StyleClassName("WideButton");
            this.selectButton.getEvents().setOnClick(() : void => {
                this.browseButton.Enabled(true);
                this.directoryBrowser.Visible(false);
                this.selectButton.Visible(false);
                this.componentsLabel.Visible(true);
                this.componentsList.Visible(true);
                this.testsLabel.Visible(true);
                this.testsList.Visible(true);
                this.blankButton.Visible(true);
            });

            this.componentsList.Width(800);
            this.componentsList.MaxVisibleItemsCount(10);

            this.testsList.Width(800);
            this.testsList.MaxVisibleItemsCount(10);

            this.blankButton.Width(800);
            this.blankButton.StyleClassName("WideButton");

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.targetField)
                .Add(this.browseButton)
                .Add(this.directoryBrowser)
                .Add(this.selectButton)
                .Add(this.componentsLabel)
                .Add(this.componentsList)
                .Add(this.testsLabel)
                .Add(this.testsList)
                .Add(this.blankButton);
        }
    }
}
