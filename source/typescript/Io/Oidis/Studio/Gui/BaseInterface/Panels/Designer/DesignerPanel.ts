/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import GeneralEventOwner = Io.Oidis.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Io.Oidis.Gui.Enums.Events.EventType;
    import ExceptionsManager = Io.Oidis.Commons.Exceptions.ExceptionsManager;
    import Reflection = Io.Oidis.Commons.Utils.Reflection;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import WindowManager = Io.Oidis.Gui.Utils.WindowManager;
    import DesignerComponentsPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerComponentsPanelViewer;
    import DesignerPropertiesPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerPropertiesPanelViewer;
    import Property = Io.Oidis.Commons.Utils.Property;
    import Icon = Io.Oidis.UserControls.BaseInterface.UserControls.Icon;
    import IconType = Io.Oidis.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ElementOffset = Io.Oidis.Gui.Structures.ElementOffset;
    import DesignerIndexPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerIndexPanelViewer;
    import DesignerConsolePanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerConsolePanelViewer;
    import DesignerHierarchyPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerHierarchyPanelViewer;
    import DesignerFooterPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerFooterPanelViewer;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import CropBox = Io.Oidis.UserControls.BaseInterface.Components.CropBox;
    import DragBar = Io.Oidis.UserControls.BaseInterface.Components.DragBar;
    import MoveEventArgs = Io.Oidis.Gui.Events.Args.MoveEventArgs;
    import GuiObjectManager = Io.Oidis.Gui.GuiObjectManager;
    import Size = Io.Oidis.Gui.Structures.Size;
    import DesignerComponentPickerPanelViewer =
        Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerComponentPickerPanelViewer;
    import DesignerHeaderPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerHeaderPanelViewer;
    import TextSelectionManager = Io.Oidis.Gui.Utils.TextSelectionManager;
    import DesignerPanelViewerArgs = Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerPanelViewerArgs;

    export class DesignerPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public index : DesignerIndexPanel;
        public header : DesignerHeaderPanel;
        public components : DesignerComponentsPanel;
        public hierarchy : DesignerHierarchyPanel;
        public content : HTMLIFrameElement;
        public picker : DesignerComponentPickerPanel;
        public console : DesignerConsolePanel;
        public properties : DesignerPropertiesPanel;
        public footer : DesignerFooterPanel;
        public itemSelector : CropBox;
        public itemDrag : DragBar;
        private contentTimeout : any;
        private targetPath : string;
        private readonly contentLoader : Icon;
        private selectedAreaOffset : ElementOffset;
        private selectedAreaSize : Size;

        private static onDragStartEndEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                const element : DragBar = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    const parent : DesignerPanel = <DesignerPanel>element.Parent();
                    if ($reflection.IsMemberOf(parent, DesignerPanel)) {
                        const elementOffset : ElementOffset = ElementManager.getAbsoluteOffset(element);
                        const contentOffset : ElementOffset = ElementManager.getAbsoluteOffset(parent.Id() + "_ItemManagers");
                        parent.selectedAreaOffset.Top(elementOffset.Top() - contentOffset.Top());
                        parent.selectedAreaOffset.Left(elementOffset.Left() - contentOffset.Left());
                    }
                }
            }
        }

        private static onDragChangeEventHandler($eventArgs : MoveEventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>DragBar)) {
                const element : DragBar = <DragBar>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, DragBar)) {
                    const parent : DesignerPanel = <DesignerPanel>element.Parent();
                    if ($reflection.IsMemberOf(parent, DesignerPanel)) {
                        const windowSize : Size = new Size();
                        // windowSize.Width(parent.content.width);
                        // windowSize.Height(parent.content.height);
                        let top : number = parent.selectedAreaOffset.Top() + $eventArgs.getDistanceY();
                        let left : number = parent.selectedAreaOffset.Left() + $eventArgs.getDistanceX();

                        if (left < 0) {
                            left = 0;
                        }
                        if (top < 0) {
                            top = 0;
                        }
                        if (left > windowSize.Width() - parent.selectedAreaSize.Width()) {
                            left = windowSize.Width() - parent.selectedAreaSize.Width();
                        }
                        if (top > windowSize.Height() - parent.selectedAreaSize.Height()) {
                            top = windowSize.Height() - parent.selectedAreaSize.Height();
                        }

                        parent.SelectArea(top, left, parent.selectedAreaSize.Width(), parent.selectedAreaSize.Height());
                    }
                }
            }
        }

        constructor($id? : string) {
            super($id);

            this.addChildPanel(DesignerIndexPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerIndexPanel) : void => {
                    $parent.index = $child;
                });

            this.addChildPanel(DesignerHeaderPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerHeaderPanel) : void => {
                    $parent.header = $child;
                });

            this.addChildPanel(DesignerComponentsPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerComponentsPanel) : void => {
                    $parent.components = $child;
                });

            this.addChildPanel(DesignerHierarchyPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerHierarchyPanel) : void => {
                    $parent.hierarchy = $child;
                });

            this.content = document.createElement("iframe");
            this.content.id = this.Id() + "_Content";
            this.contentLoader = new Icon(IconType.SPINNER_BIG);

            this.addChildPanel(DesignerComponentPickerPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerComponentPickerPanel) : void => {
                    $parent.picker = $child;
                });

            this.addChildPanel(DesignerConsolePanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerConsolePanel) : void => {
                    $parent.console = $child;
                });

            this.addChildPanel(DesignerPropertiesPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerPropertiesPanel) : void => {
                    $parent.properties = $child;
                });

            this.addChildPanel(DesignerFooterPanelViewer, null,
                ($parent : DesignerPanel, $child : DesignerFooterPanel) : void => {
                    $parent.footer = $child;
                });

            this.itemSelector = new CropBox();
            this.itemDrag = new DragBar();
            this.selectedAreaOffset = new ElementOffset();
            this.selectedAreaSize = new Size();
        }

        public Value($value? : DesignerPanelViewerArgs) : DesignerPanelViewerArgs {
            return <DesignerPanelViewerArgs>super.Value($value);
        }

        public TargetPath($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.targetPath = Property.String(this.targetPath, $value);
                if (this.IsCompleted()) {
                    this.content = <HTMLIFrameElement>ElementManager.getElement(this.content.id, true);
                    if (this.content.src !== this.targetPath) {
                        this.ShowContentLoader(true);
                        this.resetTimeout();
                        this.content.src = this.targetPath;
                    }
                }
            }
            return this.targetPath;
        }

        public ShowContentLoader($value : boolean) : void {
            this.contentLoader.Visible($value);
            if (this.contentLoader.Visible() && this.IsCompleted()) {
                ElementManager.setPosition(this.contentLoader, new ElementOffset(this.Height() / 2, this.Width() / 2));
            }
        }

        public ShowItemManagers($value : boolean) : void {
            if (this.IsCompleted()) {
                if ($value) {
                    ElementManager.Show(this.Id() + "_ItemManagers");
                } else {
                    ElementManager.Hide(this.Id() + "_ItemManagers");
                }
            }
        }

        public SelectArea($top : number, $left : number, $width : number, $height : number) : void {
            const zoom : number = this.header.zoom.Value() / 100;
            $top *= zoom;
            $left *= zoom;
            $width *= zoom;
            $height *= zoom;
            this.itemSelector.setDimensions($left, $top, $left + $width, $top + $height);
            ElementManager.setCssProperty(this.itemDrag, "top", $top);
            ElementManager.setCssProperty(this.itemDrag, "left", $left);
            ElementManager.setSize(this.itemDrag, $width, $height);
            this.selectedAreaSize.Width($width);
            this.selectedAreaSize.Height($height);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);
            let windowHeight : number = WindowManager.getSize().Height() - 100;
            this.Height(windowHeight);
            this.index.Height(windowHeight);
            windowHeight -= 100;
            this.components.Height(windowHeight);
            this.hierarchy.Height(windowHeight);
            ElementManager.setCssProperty(this.content, "height", windowHeight);
            this.picker.Height(windowHeight);
            this.console.Height(windowHeight);
            this.properties.Height(windowHeight);

            this.contentLoader.StyleClassName("ContentLoader");
            ElementManager.setPosition(this.contentLoader, new ElementOffset(this.Height() / 2, WindowManager.getSize().Width() / 2));
            this.ShowContentLoader(false);

            this.getEvents().setOnComplete(() : void => {
                this.ShowContentLoader(true);
                this.content = <HTMLIFrameElement>ElementManager.getElement(this.content.id, true);
                this.content.contentWindow.name = this.Id() + "_TargetViewer";
                this.content.onload = () : void => {
                    try {
                        this.ShowContentLoader(false);
                    } catch (ex) {
                        ExceptionsManager.HandleException(ex);
                    }
                };
                this.resetTimeout();
                this.content.src = this.TargetPath();
            });

            this.itemSelector.EnvelopOwner(this.content.id);
            this.itemSelector.Enabled(false);

            this.itemDrag.getEvents().setOnDragStart(DesignerPanel.onDragStartEndEventHandler);
            this.itemDrag.getEvents().setOnDragChange(DesignerPanel.onDragChangeEventHandler);
            this.itemDrag.getEvents().setOnDragComplete(DesignerPanel.onDragStartEndEventHandler);

            let selectorOffset : ElementOffset = new ElementOffset();
            let selectorIsActive : boolean = false;

            const getArea : ($eventArgs : MoveEventArgs) => number[] = ($eventArgs : MoveEventArgs) : number[] => {
                let topX : number = $eventArgs.getPositionX() - selectorOffset.Left() - $eventArgs.getDistanceX();
                let topY : number = $eventArgs.getPositionY() - selectorOffset.Top() - $eventArgs.getDistanceY();
                let bottomX : number = $eventArgs.getPositionX() - selectorOffset.Left();
                let bottomY : number = $eventArgs.getPositionY() - selectorOffset.Top();
                if ($eventArgs.getDistanceX() < 0) {
                    bottomX = $eventArgs.getPositionX() - selectorOffset.Left() - $eventArgs.getDistanceX();
                    topX = $eventArgs.getPositionX() - selectorOffset.Left();
                }
                if ($eventArgs.getDistanceY() < 0) {
                    bottomY = $eventArgs.getPositionY() - selectorOffset.Top() - $eventArgs.getDistanceY();
                    topY = $eventArgs.getPositionY() - selectorOffset.Top();
                }
                return [topX, topY, bottomX, bottomY];
            };

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START, ($eventArgs : MoveEventArgs) : void => {
                if (this.footer.selectButton.IsSelected()) {
                    this.ShowItemManagers(true);
                    selectorOffset = this.itemSelector.getScreenPosition();
                    this.ShowItemManagers(false);
                    const area : number[] = getArea($eventArgs);
                    if (area[0] >= 0 && area[1] >= 0 && area[2] <= this.content.offsetWidth && area[3] <= this.content.offsetHeight) {
                        this.itemDrag.Visible(false);
                        this.itemSelector.setDimensions(area[0], area[1], area[2], area[3]);
                        selectorIsActive = true;
                    } else {
                        selectorIsActive = false;
                    }
                }
            });
            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, ($eventArgs : MoveEventArgs) : void => {
                if (selectorIsActive && this.footer.selectButton.IsSelected()) {
                    this.ShowItemManagers(true);
                    const area : number[] = getArea($eventArgs);
                    this.itemSelector.setDimensions(area[0], area[1], area[2], area[3]);
                    TextSelectionManager.Clear();
                }
            });
            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, ($eventArgs : MoveEventArgs) : void => {
                if (this.footer.selectButton.IsSelected()) {
                    this.ShowItemManagers(false);
                    this.itemDrag.Visible(true);
                    if (selectorIsActive) {
                        const area : number[] = getArea($eventArgs);
                        if (area[2] - area[0] > 0 || area[3] - area[1] > 0) {
                            this.picker.SelectArea(area[0], area[1], area[2], area[3]);
                        }
                    }
                    selectorIsActive = false;
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.contentLoader)
                .Add(this.index)
                .Add(this.header)
                .Add(this.addElement().StyleClassName("Tools")
                    .Add(this.components)
                    .Add(this.hierarchy)
                    .Add(this.console)
                    .Add(this.addElement(this.Id() + "_ItemManagers").StyleClassName("ItemManagers")
                        .Add(this.itemSelector)
                        .Add(this.itemDrag)
                    )
                    .Add(this.picker)
                    .Add(this.content)
                    .Add(this.properties)
                )
                .Add(this.footer);
        }

        private resetTimeout() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.contentTimeout)) {
                clearTimeout(this.contentTimeout);
            }
            this.contentTimeout = setTimeout(() : void => {
                try {
                    this.ShowContentLoader(false);
                    /// todo: handle not builded project
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }, 2000);
        }
    }
}
