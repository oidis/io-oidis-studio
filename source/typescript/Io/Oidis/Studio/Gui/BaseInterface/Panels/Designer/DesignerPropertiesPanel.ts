/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Reflection = Io.Oidis.Commons.Utils.Reflection;
    import GuiOptionType = Io.Oidis.Gui.Enums.GuiOptionType;
    import IGuiCommonsArg = Io.Oidis.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Io.Oidis.Gui.Enums.GuiCommonsArgType;
    import DesignerProtocolConstants = Io.Oidis.Gui.Enums.DesignerProtocolConstants;
    import IGuiCommonsListArg = Io.Oidis.Gui.Interfaces.Primitives.IGuiCommonsListArg;
    import EventArgs = Io.Oidis.Commons.Events.Args.EventArgs;
    import IFormsObject = Io.Oidis.Gui.Interfaces.Primitives.IFormsObject;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;
    import CheckBoxFormArgs = Io.Oidis.UserControls.Structures.CheckBoxFormArgs;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import FormsObject = Io.Oidis.UserControls.Primitives.FormsObject;
    import DesignerPropertiesPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerPropertiesPanelViewerArgs;

    export class DesignerPropertiesPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public idField : FormInput;
        public nameField : FormInput;
        public styleClassField : FormInput;
        public enabledBox : FormInput;
        public visibleBox : FormInput;
        public titleField : FormInput;
        public valueField : FormInput;
        public widthField : FormInput;
        public heightField : FormInput;
        public errorBox : FormInput;
        public tabIndexField : FormInput;
        public topField : FormInput;
        public leftField : FormInput;
        public guiTypeList : FormInput;

        constructor($id? : string) {
            super($id);

            this.idField = new FormInput(new TextFieldFormArgs());
            this.nameField = new FormInput(new TextFieldFormArgs());
            this.styleClassField = new FormInput(new TextFieldFormArgs());
            this.enabledBox = new FormInput(new CheckBoxFormArgs());
            this.visibleBox = new FormInput(new CheckBoxFormArgs());
            this.titleField = new FormInput(new TextFieldFormArgs());
            this.valueField = new FormInput(new TextFieldFormArgs());
            this.widthField = new FormInput(new TextFieldFormArgs());
            this.heightField = new FormInput(new TextFieldFormArgs());
            this.errorBox = new FormInput(new CheckBoxFormArgs());
            this.tabIndexField = new FormInput(new TextFieldFormArgs());
            this.topField = new FormInput(new TextFieldFormArgs());
            this.leftField = new FormInput(new TextFieldFormArgs());
            this.guiTypeList = new FormInput(new DropDownListFormArgs());
        }

        public Value($value? : DesignerPropertiesPanelViewerArgs) : DesignerPropertiesPanelViewerArgs {
            return <DesignerPropertiesPanelViewerArgs>super.Value($value);
        }

        public setProperty($property : IGuiCommonsArg) : void {
            switch ($property.type) {
            case GuiCommonsArgType.TEXT:
                switch ($property.name) {
                case "Id":
                    this.setFieldValue(this.idField, $property.value);
                    break;
                case DesignerProtocolConstants.VARIABLE_NAME:
                    if ($property.value === DesignerProtocolConstants.ROOT_ELEMENT_VARIABLE) {
                        this.setFieldValue(this.nameField, "root element");
                        this.nameField.Enabled(false);
                    } else if ($property.value === DesignerProtocolConstants.HTML_ELEMENT_VARIABLE) {
                        this.setFieldValue(this.nameField, "HTML element");
                        this.nameField.Enabled(false);
                    } else {
                        this.setFieldValue(this.nameField, $property.value);
                    }
                    break;
                case "StyleClassName":
                    this.setFieldValue(this.styleClassField, $property.value);
                    break;
                case "Title":
                    this.titleField.Visible(true);
                    this.setFieldValue(this.titleField, $property.value);
                    break;
                case "Value":
                    this.valueField.Visible(true);
                    this.setFieldValue(this.valueField, $property.value);
                    break;

                default:
                    break;
                }
                break;
            case GuiCommonsArgType.NUMBER:
                switch ($property.name) {
                case "Width":
                    this.widthField.Visible(true);
                    this.setFieldValue(this.widthField, $property.value);
                    break;
                case "TabIndex":
                    this.tabIndexField.Visible(true);
                    this.setFieldValue(this.tabIndexField, $property.value);
                    break;
                case "Height":
                    this.heightField.Visible(true);
                    this.setFieldValue(this.heightField, $property.value);
                    break;
                case "Top":
                    this.topField.Visible(true);
                    this.setFieldValue(this.topField, $property.value);
                    break;
                case "Left":
                    this.leftField.Visible(true);
                    this.setFieldValue(this.leftField, $property.value);
                    break;
                default:
                    break;
                }
                break;
            case GuiCommonsArgType.BOOLEAN:
                switch ($property.name) {
                case "Enabled":
                    this.enabledBox.Visible(true);
                    this.enabledBox.Value(<boolean>$property.value);
                    break;
                case "Visible":
                    this.visibleBox.Value(<boolean>$property.value);
                    break;
                case "Error":
                    this.errorBox.Visible(true);
                    this.errorBox.Value(<boolean>$property.value);
                    break;
                default:
                    break;
                }
                break;
            case GuiCommonsArgType.LIST:
                switch ($property.name) {
                case "GuiType":
                    this.guiTypeList.Visible(true);
                    this.guiTypeList.Clear();
                    const listArgs : DropDownListFormArgs = <DropDownListFormArgs>this.guiTypeList.Configuration();
                    (<IGuiCommonsListArg>$property).items.forEach(($value : string) : void => {
                        listArgs.AddItem($value);
                    });
                    this.guiTypeList.Configuration(listArgs);
                    this.setFieldValue(this.guiTypeList, $property.value);
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }

        public setPropertyError($element : FormInput, $value : boolean) : void {
            $element.Error($value);
            if ($element.IsCompleted()) {
                const input : FormsObject = (<any>$element).getInputElement();
                Reflection.getInstance().getClass(input.getClassName()).setErrorStyle(input, $value);
                (<any>input).error = $value;
            } else {
                $element.Error($value);
            }
        }

        public setOnPropertyChange(setProperty : ($id : string, $type : string, $value : any) => void) : void {
            const propertyHandler : ($name : string, $eventArgs : EventArgs) => void =
                ($name : string, $eventArgs : EventArgs) : void => {
                    setProperty(
                        <string>(<DesignerPropertiesPanel>(<IFormsObject>$eventArgs.Owner()).Parent()).idField.Value(),
                        $name,
                        (<IFormsObject>$eventArgs.Owner()).Value());
                };

            this.nameField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler(DesignerProtocolConstants.VARIABLE_NAME, $eventArgs);
            });
            this.styleClassField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("StyleClassName", $eventArgs);
            });
            this.enabledBox.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Enabled", $eventArgs);
            });
            this.visibleBox.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Visible", $eventArgs);
            });
            this.titleField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Title", $eventArgs);
            });
            this.valueField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Value", $eventArgs);
            });
            this.widthField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Width", $eventArgs);
            });
            this.heightField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Height", $eventArgs);
            });
            this.errorBox.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Error", $eventArgs);
            });
            this.tabIndexField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("TabIndex", $eventArgs);
            });
            this.topField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Top", $eventArgs);
            });
            this.leftField.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("Left", $eventArgs);
            });
            this.guiTypeList.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                propertyHandler("GuiType", $eventArgs);
            });
        }

        public ClearProperties() : void {
            this.titleField.Visible(false);
            this.valueField.Visible(false);
            this.widthField.Visible(false);
            this.heightField.Visible(false);
            this.errorBox.Visible(false);
            this.errorBox.Value(false);
            this.tabIndexField.Visible(false);
            this.enabledBox.Visible(false);
            this.enabledBox.Value(false);
            this.visibleBox.Value(false);
            this.nameField.Enabled(true);
            this.topField.Visible(false);
            this.leftField.Visible(false);
            this.guiTypeList.Visible(false);
            this.setPropertyError(this.nameField, false);
            this.setFieldValue(this.idField, "");
            this.setFieldValue(this.nameField, "");
            this.setFieldValue(this.styleClassField, "");
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            (<TextFieldFormArgs>this.idField.Configuration()).ReadOnly(true);
            this.fillToParent(this.idField);
            this.fillToParent(this.nameField);
            this.nameField.Configuration().AddGuiOption(GuiOptionType.DISABLE);
            this.fillToParent(this.styleClassField);
            this.fillToParent(this.titleField);
            this.fillToParent(this.valueField);
            this.fillToParent(this.widthField);
            (<TextFieldFormArgs>this.widthField.Configuration()).IntegerOnly(true);
            this.fillToParent(this.heightField);
            (<TextFieldFormArgs>this.heightField.Configuration()).IntegerOnly(true);
            this.fillToParent(this.tabIndexField);
            (<TextFieldFormArgs>this.tabIndexField.Configuration()).IntegerOnly(true);
            this.fillToParent(this.topField);
            (<TextFieldFormArgs>this.topField.Configuration()).IntegerOnly(true);
            this.fillToParent(this.leftField);
            (<TextFieldFormArgs>this.leftField.Configuration()).IntegerOnly(true);
            this.fillToParent(this.guiTypeList);
            (<DropDownListFormArgs>this.guiTypeList.Configuration()).MaxVisibleItemsCount(10);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.idField)
                .Add(this.nameField)
                .Add(this.styleClassField)
                .Add(this.guiTypeList)
                .Add(this.enabledBox)
                .Add(this.visibleBox)
                .Add(this.titleField)
                .Add(this.valueField)
                .Add(this.widthField)
                .Add(this.heightField)
                .Add(this.errorBox)
                .Add(this.tabIndexField)
                .Add(this.topField)
                .Add(this.leftField);
        }

        private setFieldValue($element : FormInput, $value : any) : void {
            if ($element.IsCompleted()) {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    const input : FormsObject = (<any>$element).getInputElement();
                    Reflection.getInstance().getClass(input.getClassName()).forceSetValue(input, $value);
                    (<any>input).value = $value;
                }, false);
            } else {
                $element.Value($value);
            }
        }

        private fillToParent($element : FormInput) : void {
            $element.Configuration().Width(300);
        }
    }
}
