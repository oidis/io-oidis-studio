/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Panels.Installer {
    "use strict";
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Button = Io.Oidis.UserControls.BaseInterface.UserControls.Button;
    import ProgressBar = Io.Oidis.UserControls.BaseInterface.UserControls.ProgressBar;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import InstallerInitialScreenPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Installer.InstallerInitialScreenPanelViewerArgs;
    import FormInput = Io.Oidis.UserControls.BaseInterface.UserControls.FormInput;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import TextField = Io.Oidis.UserControls.BaseInterface.UserControls.TextField;

    export class InstallerInitialScreenPanel extends Io.Oidis.UserControls.Primitives.BasePanel {
        public installationLabel : Label;
        public progressBar : ProgressBar;
        public statusLabel : Label;
        public installPath : FormInput;
        public workplacePath : FormInput;
        public installButton : Button;

        constructor($id? : string) {
            super($id);

            this.installationLabel = new Label();
            this.progressBar = new ProgressBar();
            this.statusLabel = new Label();
            this.installPath = new FormInput(new TextFieldFormArgs());
            this.workplacePath = new FormInput(new TextFieldFormArgs());
            this.installButton = new Button();
        }

        public Value($value? : InstallerInitialScreenPanelViewerArgs) : InstallerInitialScreenPanelViewerArgs {
            return <InstallerInitialScreenPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.installationLabel.StyleClassName("Header");
            this.installPath.Configuration().Width(800);
            this.workplacePath.Configuration().Width(800);
            this.progressBar.Width(800);
            this.installButton.Width(800);

            (<TextField>(<any>this.installPath).getInputElement()).getEvents().setOnChange(() : void => {
                this.installButton.Enabled(!ObjectValidator.IsEmptyOrNull(this.installPath.Value()));
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.installationLabel)
                .Add(this.installPath)
                .Add(this.workplacePath)
                .Add(this.addElement().StyleClassName("Progress")
                    .Add(this.progressBar)
                    .Add(this.statusLabel)
                )
                .Add(this.installButton)
                .Add(this.addElement().StyleClassName("Logo"));
        }
    }
}
