/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DashboardPanelViewerArgs = Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.DashboardPanelViewerArgs;
    import DashboardPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.DashboardPanel;
    import DashboardPanelLayerType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DashboardPanelLayerType;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;

    export class DashboardPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DashboardPanelViewerArgs {
            const args : DashboardPanelViewerArgs = new DashboardPanelViewerArgs();
            args.LocalhostInfoButtonText("Localhost info");
            args.ToolchainInfoButtonText("Toolchain");
            args.ProjectsButtonText("Projects");
            args.HeaderLabelText("Projects");
            args.LocalhostInfoArgs((<any>LocalhostInfoPanelViewer).getTestViewerArgs());
            args.ToolchainInfoArgs((<any>ToolchainInfoPanelViewer).getTestViewerArgs());
            args.ProjectsArgs((<any>ProjectsManagerPanelViewer).getTestViewerArgs());

            return args;
        }

        /* dev:end */

        constructor($args? : DashboardPanelViewerArgs) {
            super($args);
            this.setInstance(new DashboardPanel());
        }

        public getInstance() : DashboardPanel {
            return <DashboardPanel>super.getInstance();
        }

        public ViewerArgs($args? : DashboardPanelViewerArgs) : DashboardPanelViewerArgs {
            return <DashboardPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DashboardPanel, $args : DashboardPanelViewerArgs) : void {
            $instance.localhostInfoButton.Text($args.LocalhostInfoButtonText());
            $instance.toolchainInfoButton.Text($args.ToolchainInfoButtonText());
            $instance.projectsButton.Text($args.ProjectsButtonText());
            $instance.headerLabel.Text($args.HeaderLabelText());
            $instance.localhostInfo.Value($args.LocalhostInfoArgs());
            $instance.toolchainInfo.Value($args.ToolchainInfoArgs());
            $instance.projects.Value($args.ProjectsArgs());
        }

        protected layerHandler($layer : DashboardPanelLayerType, $instance : DashboardPanel) : void {
            ElementManager.Show($instance.Id() + "_Header");
            ElementManager.Hide($instance.Id() + "_Logo");
            ElementManager.Hide($instance.Id() + "_Buttons");

            $instance.localhostInfoButton.Visible(false);
            $instance.localhostInfo.Visible(false);
            $instance.toolchainInfo.Visible(false);
            $instance.projects.Visible(false);

            switch ($layer) {
            case DashboardPanelLayerType.LOCALHOST_INFO:
                $instance.localhostInfo.Visible(true);
                break;
            case DashboardPanelLayerType.TOOLCHAIN_INFO:
                $instance.headerLabel.Text("Toolchain");
                $instance.toolchainInfo.Visible(true);
                break;
            case DashboardPanelLayerType.PROJECTS:
                $instance.headerLabel.Text("Projects");
                $instance.projects.Visible(true);
                break;
            default:
                ElementManager.Hide($instance.Id() + "_Header");
                ElementManager.Show($instance.Id() + "_Logo");
                ElementManager.Show($instance.Id() + "_Buttons");
                break;
            }
        }

        protected beforeLoad($instance : DashboardPanel) : void {
            $instance.backButton.getEvents().setOnClick(() : void => {
                this.setLayer(DashboardPanelLayerType.DEFAULT);
            });
            $instance.localhostInfoButton.getEvents().setOnClick(() : void => {
                this.setLayer(DashboardPanelLayerType.LOCALHOST_INFO);
            });
            $instance.toolchainInfoButton.getEvents().setOnClick(() : void => {
                this.setLayer(DashboardPanelLayerType.TOOLCHAIN_INFO);
            });
            $instance.projectsButton.getEvents().setOnClick(() : void => {
                this.setLayer(DashboardPanelLayerType.PROJECTS);
            });
        }
    }
}
