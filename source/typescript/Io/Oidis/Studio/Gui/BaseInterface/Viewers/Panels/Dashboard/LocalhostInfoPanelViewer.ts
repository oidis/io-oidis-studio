/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import LocalhostInfoPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.LocalhostInfoPanel;
    import LocalhostInfoPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.LocalhostInfoPanelViewerArgs;

    export class LocalhostInfoPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : LocalhostInfoPanelViewerArgs {
            const args : LocalhostInfoPanelViewerArgs = new LocalhostInfoPanelViewerArgs();
            args.LocalhostStatus("Start WUI Localhost");
            args.LocationText("Open localhost name:");
            args.ServerRootText("Server root:");
            args.ServerLogsText("Server logs:");
            args.ConfigFilesText("Open config file:");
            args.CertsUpdate("Update SSL certificates");

            return args;
        }

        /* dev:end */

        constructor($args? : LocalhostInfoPanelViewerArgs) {
            super($args);
            this.setInstance(new LocalhostInfoPanel());
        }

        public getInstance() : LocalhostInfoPanel {
            return <LocalhostInfoPanel>super.getInstance();
        }

        public ViewerArgs($args? : LocalhostInfoPanelViewerArgs) : LocalhostInfoPanelViewerArgs {
            return <LocalhostInfoPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : LocalhostInfoPanel, $args : LocalhostInfoPanelViewerArgs) : void {
            $instance.localhostSwitch.Text($args.LocalhostStatus());
            $instance.location.Configuration().Name($args.LocationText());
            $instance.serverRoot.Configuration().Name($args.ServerRootText());
            $instance.serverLogs.Configuration().Name($args.ServerLogsText());
            $instance.configFiles.Configuration().Name($args.ConfigFilesText());
            $instance.certsUpdate.Text($args.CertsUpdate());
        }

        protected beforeLoad($instance : LocalhostInfoPanel) : void {
            $instance.localhostSwitch.Enabled(false);

            const locationArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.location.Configuration();
            locationArgs.AddItem("https://localhost.wuiframework.com");
            locationArgs.AddItem("http://localhost.wuiframework.com");
            locationArgs.AddItem("http://localhost");
            locationArgs.Hint("select location ...");
            locationArgs.Enabled(false);

            $instance.serverRoot.Enabled(false);
            $instance.serverLogs.Enabled(false);

            const configFilesArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.configFiles.Configuration();
            configFilesArgs.AddItem("httpd.conf (Apache)", 1);
            configFilesArgs.AddItem("php.ini", 2);
            configFilesArgs.AddItem("sendmail.ini", 3);
            configFilesArgs.Hint("select config ...");
            configFilesArgs.Enabled(false);
        }
    }
}
