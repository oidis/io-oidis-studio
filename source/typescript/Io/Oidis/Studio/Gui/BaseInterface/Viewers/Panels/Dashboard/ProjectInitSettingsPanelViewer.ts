/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ProjectInitSettingsPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ProjectInitSettingsPanel;
    import ProjectInitSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectInitSettingsPanelViewerArgs;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;

    export class ProjectInitSettingsPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectInitSettingsPanelViewerArgs {
            const args : ProjectInitSettingsPanelViewerArgs = new ProjectInitSettingsPanelViewerArgs();
            args.NamespaceText("Library namespace:");
            args.ExtendsText("Extends from:");
            args.TargetPathText("Destination path:");
            args.TargetPath("D:/__TEMP__");
            args.NameText("Library name:");
            args.TitleText("Library title:");
            args.DescriptionText("Library description:");
            args.RepositoryText("GIT repository url:");

            return args;
        }

        /* dev:end */

        constructor($args? : ProjectInitSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectInitSettingsPanel());
        }

        public getInstance() : ProjectInitSettingsPanel {
            return <ProjectInitSettingsPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.ProjectInit.ProjectInitSettingsPanelViewer#
         * @param {ProjectInitSettingsPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {ProjectInitSettingsPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : ProjectInitSettingsPanelViewerArgs) : ProjectInitSettingsPanelViewerArgs {
            return <ProjectInitSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectInitSettingsPanel, $args : ProjectInitSettingsPanelViewerArgs) : void {
            $instance.namespaceField.Configuration().Name($args.NamespaceText());
            $instance.extendsList.Configuration().Name($args.ExtendsText());
            $instance.targetField.Configuration().Name($args.TargetPathText());
            $instance.targetField.Value($args.TargetPath());
            $instance.nameField.Configuration().Name($args.NameText());
            $instance.titleField.Configuration().Name($args.TitleText());
            $instance.descriptionField.Configuration().Name($args.DescriptionText());
            $instance.repositoryField.Configuration().Name($args.RepositoryText());
        }

        protected beforeLoad($instance : ProjectInitSettingsPanel) : void {
            $instance.namespaceField.Value("Io.Oidis.Application");

            const listArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.extendsList.Configuration();
            listArgs.AddItem("WUI Commons", 1);
            listArgs.AddItem("WUI GUI", 2);
            listArgs.AddItem("WUI User Controls", 3);
            listArgs.AddItem("WUI Services", 4);
            listArgs.AddItem("WUI REST Commons", 5);
            listArgs.AddItem("WUI REST Services", 6);
            listArgs.AddItem("WUI XCpp Commons", 7);
            $instance.extendsList.Value(0);

            $instance.nameField.Value("WUI Designer Template");
            $instance.titleField.Value("WUI Designer - Template");
            $instance.descriptionField.Value("WUI Framework's Template generated by WUI Designer tool");
            $instance.repositoryField.Value("https://gitlab.com/oidis/io-oidis-application.git");
        }
    }
}
