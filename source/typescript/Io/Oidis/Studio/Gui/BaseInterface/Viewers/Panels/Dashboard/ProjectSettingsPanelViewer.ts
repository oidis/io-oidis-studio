/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ProjectSettingsPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ProjectSettingsPanel;
    import ProjectSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectSettingsPanelViewerArgs;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;

    export class ProjectSettingsPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectSettingsPanelViewerArgs {
            const args : ProjectSettingsPanelViewerArgs = new ProjectSettingsPanelViewerArgs();
            const item : TextFieldFormArgs = new TextFieldFormArgs();
            item.Name("test");
            item.Value("value");
            args.AddItem(item);
            args.SaveText("Save settings");
            return args;
        }

        /* dev:end */

        constructor($args? : ProjectSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectSettingsPanel());
        }

        public getInstance() : ProjectSettingsPanel {
            return <ProjectSettingsPanel>super.getInstance();
        }

        public ViewerArgs($args? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            return <ProjectSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectSettingsPanel, $args : ProjectSettingsPanelViewerArgs) : void {
            $instance.Value($args);
            $instance.save.Text($args.SaveText());
        }
    }
}
