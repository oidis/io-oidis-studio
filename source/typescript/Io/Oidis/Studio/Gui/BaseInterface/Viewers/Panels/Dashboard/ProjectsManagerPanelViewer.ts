/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import ProjectsManagerPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectsManagerPanelViewerArgs;
    import ProjectsManagerPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ProjectsManagerPanel;

    export class ProjectsManagerPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectsManagerPanelViewerArgs {
            const args : ProjectsManagerPanelViewerArgs = new ProjectsManagerPanelViewerArgs();
            args.RemoteSourcesText("WUI project type:");
            args.WorkplacePathText("Local WUI workplace:");
            args.InstallProjectButtonText("Install project");
            args.LocalSourcesText("Local WUI projects:");

            return args;
        }

        /* dev:end */

        constructor($args? : ProjectsManagerPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectsManagerPanel());
        }

        public getInstance() : ProjectsManagerPanel {
            return <ProjectsManagerPanel>super.getInstance();
        }

        public ViewerArgs($args? : ProjectsManagerPanelViewerArgs) : ProjectsManagerPanelViewerArgs {
            return <ProjectsManagerPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectsManagerPanel, $args : ProjectsManagerPanelViewerArgs) : void {
            $instance.remoteSources.Configuration().Name($args.RemoteSourcesText());
            $instance.localSources.Configuration().Name($args.LocalSourcesText());
            $instance.workplacePath.Configuration().Name($args.WorkplacePathText());
            $instance.installProjectButton.Text($args.InstallProjectButtonText());
        }

        protected beforeLoad($instance : ProjectsManagerPanel) : void {
            const remoteListArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.remoteSources.Configuration();
            remoteListArgs.AddItem("WUI Commons", "https://gitlab.com/oidis/io-oidis-commons.git");
            remoteListArgs.AddItem("WUI GUI", "https://gitlab.com/oidis/io-oidis-gui.git");
            remoteListArgs.AddItem("WUI User Controls", "https://gitlab.com/oidis/io-oidis-usercontrols.git");
            remoteListArgs.AddItem("WUI Services", "https://gitlab.com/oidis/io-oidis-services.git");
            remoteListArgs.AddItem("WUI REST Commons", "https://gitlab.com/oidis/io-oidis-rest-commons.git");
            remoteListArgs.AddItem("WUI REST Services", "https://gitlab.com/oidis/io-oidis-rest-services.git");
            remoteListArgs.AddItem("WUI XCpp Commons", "https://gitlab.com/oidis/io-oidis-xcppcommons.git");
            remoteListArgs.AddItem("WUI Launcher", "https://gitlab.com/oidis/io-oidis-launcher.git");
            remoteListArgs.AddItem("WUI Connector", "https://gitlab.com/oidis/io-oidis-connector.git");
            remoteListArgs.AddItem("WUI ChromiumRE", "https://gitlab.com/oidis/io-oidis-cromiumre.git");
            remoteListArgs.Hint("select project");
            (<any>$instance.remoteSources).getInputElement().MaxVisibleItemsCount(5);

            const pathArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.workplacePath.Configuration();
            pathArgs.AddItem("D:/WUIFramework/Projects");
            pathArgs.Hint("select WUI workplace");

            $instance.installProjectButton.Enabled(false);

            const localListArgs : DropDownListFormArgs = <DropDownListFormArgs>$instance.localSources.Configuration();
            localListArgs.Hint("select project");
            (<any>$instance.localSources).getInputElement().MaxVisibleItemsCount(5);
            $instance.localSources.Visible(false);
        }
    }
}
