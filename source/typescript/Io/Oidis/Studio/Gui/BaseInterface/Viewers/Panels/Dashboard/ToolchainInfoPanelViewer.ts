/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ToolchainInfoPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ToolchainInfoPanel;
    import ToolchainInfoPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ToolchainInfoPanelViewerArgs;

    export class ToolchainInfoPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ToolchainInfoPanelViewerArgs {
            const args : ToolchainInfoPanelViewerArgs = new ToolchainInfoPanelViewerArgs();
            args.CheckButtonText("Validate toolchain compatibility");
            args.HeaderText("WUI Framework toolchain info:");

            return args;
        }

        /* dev:end */

        constructor($args? : ToolchainInfoPanelViewerArgs) {
            super($args);
            this.setInstance(new ToolchainInfoPanel());
        }

        public getInstance() : ToolchainInfoPanel {
            return <ToolchainInfoPanel>super.getInstance();
        }

        public ViewerArgs($args? : ToolchainInfoPanelViewerArgs) : ToolchainInfoPanelViewerArgs {
            return <ToolchainInfoPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ToolchainInfoPanel, $args : ToolchainInfoPanelViewerArgs) : void {
            $instance.checkButton.Text($args.CheckButtonText());
            $instance.headerLabel.Text($args.HeaderText());
        }

        protected beforeLoad($instance : ToolchainInfoPanel) : void {
            $instance.checkButton.Enabled(false);
        }
    }
}
