/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerComponentPickerPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerComponentPickerPanel;
    import DesignerComponentPickerPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerComponentPickerPanelViewerArgs;

    export class DesignerComponentPickerPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerComponentPickerPanelViewerArgs {
            const args : DesignerComponentPickerPanelViewerArgs = new DesignerComponentPickerPanelViewerArgs();
            args.Structure([]);

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerComponentPickerPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerComponentPickerPanel());
        }

        public getInstance() : DesignerComponentPickerPanel {
            return <DesignerComponentPickerPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerComponentPickerPanelViewer#
         * @param {DesignerComponentPickerPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerComponentPickerPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerComponentPickerPanelViewerArgs) : DesignerComponentPickerPanelViewerArgs {
            return <DesignerComponentPickerPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DesignerComponentPickerPanel, $args : DesignerComponentPickerPanelViewerArgs) : void {
            $instance.setStructure($args.Structure(), 100);
        }
    }
}
