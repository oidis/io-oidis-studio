/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerComponentsPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerComponentsPanel;
    import DesignerComponentsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerComponentsPanelViewerArgs;
    import DesignerComponentType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DesignerComponentType;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;

    export class DesignerComponentsPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerComponentsPanelViewerArgs {
            const args : DesignerComponentsPanelViewerArgs = new DesignerComponentsPanelViewerArgs();
            args.UserControlsTypeHint("Choose user control type");
            args.UserControlsGroupHint("Choose user control group");
            args.AddButtonText("Add item");
            args.DeleteButtonText("Delete item");
            args.getComponentsType().Add("Components");
            args.getComponentsType().Add("UserControls");
            args.getComponentsType().Add("Panels");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerComponentsPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerComponentsPanel());
        }

        public getInstance() : DesignerComponentsPanel {
            return <DesignerComponentsPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerComponentsPanelViewer#
         * @param {DesignerComponentsPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerComponentsPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerComponentsPanelViewerArgs) : DesignerComponentsPanelViewerArgs {
            return <DesignerComponentsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected beforeLoad($instance : DesignerComponentsPanel, $args : DesignerComponentsPanelViewerArgs) : void {
            $instance.userControlsTypeList.Clear();
            $instance.userControlsTypeList.Add($args.getComponentsType().getItem(0), DesignerComponentType.COMPONENT);
            $instance.userControlsTypeList.Add($args.getComponentsType().getItem(1), DesignerComponentType.USER_CONTROL);
            $instance.userControlsTypeList.Add($args.getComponentsType().getItem(2), DesignerComponentType.PANEL);

            $instance.userControlsTypeList.getEvents().setOnChange(() : void => {
                $instance.SelectType($instance.userControlsTypeList.Value());
            });

            $instance.userControlsGroupList.getEvents().setOnChange(() : void => {
                $instance.Clear();
                $instance.SelectGroup($instance.userControlsGroupList.Value());
            });
        }

        protected argsHandler($instance : DesignerComponentsPanel, $args : DesignerComponentsPanelViewerArgs) : void {
            $instance.userControlsTypeList.Hint($args.UserControlsTypeHint());
            $instance.userControlsGroupList.Hint($args.UserControlsGroupHint());
            $instance.addButton.Text($args.AddButtonText());
            $instance.deleteButton.Text($args.DeleteButtonText());

            $instance.Clear();
            $args.getUserControlsType().foreach(($items : ArrayList<string>, $type : string) : void => {
                $instance.Add($items, $type);
            });
        }
    }
}
