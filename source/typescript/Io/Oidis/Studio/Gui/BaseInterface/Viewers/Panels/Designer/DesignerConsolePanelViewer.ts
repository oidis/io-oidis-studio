/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerConsolePanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerConsolePanel;
    import DesignerConsolePanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerConsolePanelViewerArgs;

    export class DesignerConsolePanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerConsolePanelViewerArgs {
            const args : DesignerConsolePanelViewerArgs = new DesignerConsolePanelViewerArgs();
            args.ContentText("Nothing has been printed yet");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerConsolePanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerConsolePanel());
        }

        public getInstance() : DesignerConsolePanel {
            return <DesignerConsolePanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerConsolePanelViewer#
         * @param {DesignerConsolePanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerConsolePanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerConsolePanelViewerArgs) : DesignerConsolePanelViewerArgs {
            return <DesignerConsolePanelViewerArgs>super.ViewerArgs($args);
        }

        protected beforeLoad($instance : DesignerConsolePanel) : void {
            $instance.Visible(false);
        }

        protected argsHandler($instance : DesignerConsolePanel, $args : DesignerConsolePanelViewerArgs) : void {
            $instance.contentLabel.Value($args.ContentText());
        }
    }
}
