/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerFooterPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerFooterPanel;
    import DesignerFooterPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerFooterPanelViewerArgs;

    export class DesignerFooterPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerFooterPanelViewerArgs {
            const args : DesignerFooterPanelViewerArgs = new DesignerFooterPanelViewerArgs();
            args.ComponentsButtonText("Components");
            args.HierarchyButtonText("Hierarchy");
            args.ConsoleButtonText("Console");
            args.PickerButtonText("Pick item");
            args.SelectButtonText("Select");
            args.PreviewButtonText("Preview");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerFooterPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerFooterPanel());
        }

        public getInstance() : DesignerFooterPanel {
            return <DesignerFooterPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerFooterPanelViewer#
         * @param {DesignerFooterPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerFooterPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerFooterPanelViewerArgs) : DesignerFooterPanelViewerArgs {
            return <DesignerFooterPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DesignerFooterPanel, $args : DesignerFooterPanelViewerArgs) : void {
            $instance.componentsButton.Text($args.ComponentsButtonText());
            $instance.hierarchyButton.Text($args.HierarchyButtonText());
            $instance.consoleButton.Text($args.ConsoleButtonText());
            $instance.pickerButton.Text($args.PickerButtonText());
            $instance.selectButton.Text($args.SelectButtonText());
            $instance.previewButton.Text($args.PreviewButtonText());
        }
    }
}
