/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerHeaderPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerHeaderPanel;
    import DesignerHeaderPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerHeaderPanelViewerArgs;
    import BaseViewerLayerType = Io.Oidis.Gui.Enums.BaseViewerLayerType;

    export class DesignerHeaderPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerHeaderPanelViewerArgs {
            const args : DesignerHeaderPanelViewerArgs = new DesignerHeaderPanelViewerArgs();
            args.BackButtonText("Back to components selection");
            args.SaveButtonText("Save");
            args.ZoomLabelText("Layer: ");
            args.ZoomLabelText("Zoom: ");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerHeaderPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerHeaderPanel());
        }

        public getInstance() : DesignerHeaderPanel {
            return <DesignerHeaderPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerHeaderPanelViewer#
         * @param {DesignerHeaderPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerHeaderPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerHeaderPanelViewerArgs) : DesignerHeaderPanelViewerArgs {
            return <DesignerHeaderPanelViewerArgs>super.ViewerArgs($args);
        }

        protected beforeLoad($instance : DesignerHeaderPanel) : void {
            const layer : string = BaseViewerLayerType.getKey(BaseViewerLayerType.DEFAULT);
            $instance.layerList.Add(layer);
            $instance.layerList.MaxVisibleItemsCount(10);
            $instance.layerList.Select(layer);
        }

        protected argsHandler($instance : DesignerHeaderPanel, $args : DesignerHeaderPanelViewerArgs) : void {
            $instance.backButton.Text($args.BackButtonText());
            $instance.saveButton.Text($args.SaveButtonText());
            $instance.layerLabel.Text($args.LayerLabelText());
            $instance.zoomLabel.Text($args.ZoomLabelText());
        }
    }
}
