/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerHierarchyPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerHierarchyPanel;
    import DesignerHierarchyPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerHierarchyPanelViewerArgs;

    export class DesignerHierarchyPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerHierarchyPanelViewerArgs {
            const args : DesignerHierarchyPanelViewerArgs = new DesignerHierarchyPanelViewerArgs();
            args.Structure([]);

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerHierarchyPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerHierarchyPanel());
        }

        public getInstance() : DesignerHierarchyPanel {
            return <DesignerHierarchyPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerHierarchyPanelViewer#
         * @param {DesignerHierarchyPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerHierarchyPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerHierarchyPanelViewerArgs) : DesignerHierarchyPanelViewerArgs {
            return <DesignerHierarchyPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DesignerHierarchyPanel, $args : DesignerHierarchyPanelViewerArgs) : void {
            $instance.setStructure($args.Structure());
        }
    }
}
