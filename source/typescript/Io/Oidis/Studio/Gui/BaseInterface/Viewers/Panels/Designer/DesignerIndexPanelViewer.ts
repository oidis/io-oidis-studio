/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerIndexPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerIndexPanel;
    import DesignerIndexPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerIndexPanelViewerArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DesignerIndexPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerIndexPanelViewerArgs {
            const args : DesignerIndexPanelViewerArgs = new DesignerIndexPanelViewerArgs();
            args.TargetText("file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-usercontrols");
            args.BrowseButtonText("Browse");
            args.SelectButtonText("Select WUI Project");
            args.ComponentsLabelText("Available components:");
            args.ComponentsListHint("Select component");
            args.TestsLabelText("Available tests:");
            args.TestsListHint("Select test");
            args.BlankButtonText("Create blank component");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerIndexPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerIndexPanel());
        }

        public getInstance() : DesignerIndexPanel {
            return <DesignerIndexPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerIndexPanelViewerArgs#
         * @param {DesignerIndexPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerIndexPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerIndexPanelViewerArgs) : DesignerIndexPanelViewerArgs {
            return <DesignerIndexPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DesignerIndexPanel, $args : DesignerIndexPanelViewerArgs) : void {
            $instance.targetField.Value($args.TargetText());
            $instance.browseButton.Text($args.BrowseButtonText());
            $instance.selectButton.Text($args.SelectButtonText());
            $instance.componentsLabel.Text($args.ComponentsLabelText());
            $instance.componentsList.Hint($args.ComponentsListHint());
            if ($instance.componentsList.getItemsCount() === 0) {
                $args.getComponentsList().foreach(($panel : string, $link? : string) : void => {
                    $instance.componentsList.Add(this.wrapItemText($panel), $link);
                });
            }

            $instance.testsLabel.Text($args.TestsLabelText());
            $instance.testsList.Hint($args.TestsListHint());
            if ($instance.testsList.getItemsCount() === 0) {
                $args.getTestsList().foreach(($test : string, $link? : string) : void => {
                    $instance.testsList.Add(this.wrapItemText($test), $link);
                });
            }

            $instance.blankButton.Text($args.BlankButtonText());
        }

        private wrapItemText($input : string, $maxNameLength : number = 100) : string {
            if (StringUtils.Length($input) > $maxNameLength) {
                $input = StringUtils.Substring($input, 0, $maxNameLength) + " " + StringUtils.Substring($input, $maxNameLength);
            }
            return $input;
        }
    }
}
