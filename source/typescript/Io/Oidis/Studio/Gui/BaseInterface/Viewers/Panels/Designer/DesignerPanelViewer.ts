/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerPanel;
    import DesignerPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerPanelViewerArgs;
    import ExceptionsManager = Io.Oidis.Commons.Exceptions.ExceptionsManager;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import DesignerComponentType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DesignerComponentType;
    import EventType = Io.Oidis.Gui.Enums.Events.EventType;
    import ElementOffset = Io.Oidis.Gui.Structures.ElementOffset;
    import EventArgs = Io.Oidis.Commons.Events.Args.EventArgs;
    import IPersistenceHandler = Io.Oidis.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Io.Oidis.Gui.PersistenceFactory;
    import PersistenceType = Io.Oidis.Gui.Enums.PersistenceType;
    import GuiCommons = Io.Oidis.Gui.Primitives.GuiCommons;
    import WindowManager = Io.Oidis.Gui.Utils.WindowManager;
    import GuiObjectManager = Io.Oidis.Gui.GuiObjectManager;
    import CropBox = Io.Oidis.UserControls.BaseInterface.Components.CropBox;
    import Size = Io.Oidis.Gui.Structures.Size;
    import ResizeEventArgs = Io.Oidis.Gui.Events.Args.ResizeEventArgs;
    import IDesignerProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerProtocol;
    import DesignerTaskType = Io.Oidis.Gui.Enums.DesignerTaskType;
    import IDesignerEventProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerEventProtocol;
    import IDesignerProjectMapProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerProjectMapProtocol;
    import IDesignerProjectMapItems = Io.Oidis.Gui.Interfaces.Designer.IDesignerProjectMapItems;
    import IDesignerProjectMapViewers = Io.Oidis.Gui.Interfaces.Designer.IDesignerProjectMapViewers;
    import IGuiCommonsArg = Io.Oidis.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import IDesignerItemArgsProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerItemArgsProtocol;
    import IDesignerItemArgProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerItemArgProtocol;
    import IDesignerItemProtocol = Io.Oidis.Gui.Interfaces.Designer.IDesignerItemProtocol;
    import IDesignerInstanceMapItem = Io.Oidis.Gui.Interfaces.Designer.IDesignerInstanceMapItem;
    import DesignerPanelLayerType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DesignerPanelLayerType;
    import MessageEventArgs = Io.Oidis.Commons.Events.Args.MessageEventArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DesignerPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {
        private persistence : IPersistenceHandler;

        /* dev:start */
        protected static getTestViewerArgs() : DesignerPanelViewerArgs {
            const args : DesignerPanelViewerArgs = new DesignerPanelViewerArgs();
            (<any>args).indexArgs = (<any>DesignerIndexPanelViewer).getTestViewerArgs();
            (<any>args).headerArgs = (<any>DesignerHeaderPanelViewer).getTestViewerArgs();
            (<any>args).componentArgs = (<any>DesignerComponentsPanelViewer).getTestViewerArgs();
            (<any>args).hierarchyArgs = (<any>DesignerHierarchyPanelViewer).getTestViewerArgs();
            (<any>args).pickerArgs = (<any>DesignerComponentPickerPanelViewer).getTestViewerArgs();
            (<any>args).consoleArgs = (<any>DesignerConsolePanelViewer).getTestViewerArgs();
            (<any>args).propertiesArgs = (<any>DesignerPropertiesPanelViewer).getTestViewerArgs();
            (<any>args).footerArgs = (<any>DesignerFooterPanelViewer).getTestViewerArgs();
            args.TargetPath("D:/__PROJECTS__/WUI/Source/com-wui-framework-usercontrols");
            args.getIndexArgs().TargetText(args.TargetPath());

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerPanel());
            this.persistence = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        }

        public getInstance() : DesignerPanel {
            return <DesignerPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerPanelViewer#
         * @param {DesignerPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerPanelViewerArgs) : DesignerPanelViewerArgs {
            return <DesignerPanelViewerArgs>super.ViewerArgs($args);
        }

        public getLayersType() : string {
            return DesignerPanelLayerType.ClassName();
        }

        protected argsHandler($instance : DesignerPanel, $args : DesignerPanelViewerArgs) : void {
            if (!ObjectValidator.IsEmptyOrNull($args.TargetPath())) {
                $instance.TargetPath("file:///" + $args.TargetPath() + "/build/target/index.html?WuiDesigner=true#/design/");
            }
            $instance.index.Value($args.getIndexArgs());
            $instance.header.Value($args.getHeaderArgs());
            $instance.components.Value($args.getComponentArgs());
            $instance.hierarchy.Value($args.getHierarchyArgs());
            $instance.picker.Value($args.getPickerArgs());
            $instance.console.Value($args.getConsoleArgs());
            $instance.properties.Value($args.getPropertiesArgs());
            $instance.footer.Value($args.getFooterArgs());
        }

        protected layerHandler($layer : DesignerPanelLayerType, $instance? : DesignerPanel) : void {
            switch ($layer) {
            case DesignerPanelLayerType.EDITOR:
                $instance.index.Visible(false);
                $instance.header.Visible(true);
                $instance.components.Visible(true);
                $instance.hierarchy.Visible(false);
                $instance.picker.Visible(false);
                $instance.properties.Visible(true);
                $instance.footer.Visible(true);
                $instance.footer.componentsButton.IsSelected(true);
                $instance.footer.consoleButton.IsSelected(false);
                $instance.footer.hierarchyButton.IsSelected(false);
                $instance.footer.selectButton.IsSelected(false);
                ElementManager.Show($instance.content);
                break;
            case DesignerPanelLayerType.COMPONENTS:
                $instance.footer.componentsButton.IsSelected(true);
                $instance.footer.hierarchyButton.IsSelected(false);
                $instance.components.Visible(true);
                $instance.hierarchy.Visible(false);
                break;
            case DesignerPanelLayerType.CONSOLE:
                $instance.footer.consoleButton.IsSelected(true);
                $instance.picker.Visible(false);
                $instance.console.Visible(true);
                $instance.ShowItemManagers(false);
                break;
            case DesignerPanelLayerType.HIERARCHY:
                $instance.footer.componentsButton.IsSelected(false);
                $instance.footer.hierarchyButton.IsSelected(true);
                $instance.components.Visible(false);
                $instance.hierarchy.Visible(true);
                break;
            case DesignerPanelLayerType.PREVIEW:
                $instance.ShowItemManagers(false);
                $instance.footer.consoleButton.IsSelected(false);
                $instance.footer.selectButton.IsSelected(false);
                $instance.components.deleteButton.Enabled(false);
                $instance.properties.ClearProperties();
                $instance.picker.Visible(false);
                $instance.console.Visible(false);
                break;
            case DesignerPanelLayerType.PICK_AREA:
                $instance.ShowItemManagers(false);
                $instance.picker.Visible(false);
                $instance.footer.selectButton.IsSelected(false);
                $instance.picker.Visible(true);
                break;
            case DesignerPanelLayerType.SELECT_AREA:
                $instance.ShowItemManagers(false);
                $instance.picker.Visible(false);
                $instance.footer.selectButton.IsSelected(true);
                $instance.picker.Visible(true);
                break;
            case DesignerPanelLayerType.DRAG_AREA:
                $instance.ShowItemManagers(false);
                $instance.footer.selectButton.IsSelected(false);
                $instance.components.deleteButton.Enabled(false);
                $instance.properties.ClearProperties();
                break;
            default:
                $instance.header.Visible(false);
                $instance.components.Visible(false);
                $instance.hierarchy.Visible(false);
                $instance.console.Visible(false);
                $instance.properties.Visible(false);
                $instance.footer.Visible(false);
                ElementManager.Hide($instance.content);
                $instance.ShowItemManagers(false);
                $instance.index.Visible(true);
                break;
            }
        }

        protected beforeLoad($instance : DesignerPanel, $args : DesignerPanelViewerArgs) : void {
            if (this.persistence.Exists("Path")) {
                this.setLayer(DesignerPanelLayerType.EDITOR);
            }

            WindowManager.getEvents().setOnMessage(($eventArgs : MessageEventArgs) : void => {
                const data : IDesignerProtocol = <IDesignerProtocol>$eventArgs.NativeEventArgs().data;
                this.taskHandler(data.task, data.data, $instance, $args);
            });

            WindowManager.getEvents().setOnResize(() : void => {
                this.resizeContent();
            });

            $instance.itemDrag.getEvents().setOnDragComplete(() : void => {
                const contentOffset : ElementOffset = ElementManager.getAbsoluteOffset($instance.content);
                const targetOffset : ElementOffset = $instance.itemDrag.getScreenPosition();

                this.sendMessage({
                    data: {
                        id  : $instance.properties.idField.Value(),
                        left: targetOffset.Left() - contentOffset.Left(),
                        top : targetOffset.Top() - contentOffset.Top()
                    },
                    task: DesignerTaskType.MOVE_ITEM
                });
            });

            $instance.itemSelector.getEvents().setOnResizeComplete(($eventArgs : ResizeEventArgs) : void => {
                this.setProperty(<string>$instance.properties.idField.Value(), "Width", $eventArgs.Width());
                this.setProperty(<string>$instance.properties.idField.Value(), "Height", $eventArgs.Height());
            });
            $instance.itemSelector.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                const parent : DesignerPanel = <DesignerPanel>(<CropBox>$eventArgs.Owner()).Parent();
                if (!$manager.IsActive(parent.itemDrag) && !$manager.IsHovered(parent.itemDrag)
                    && !$manager.IsActive(parent.itemSelector)) {
                    this.setLayer(DesignerPanelLayerType.DRAG_AREA);
                    this.showItemsPickArea();
                }
            });

            $instance.picker.getEvents().setOnDoubleClick(() : void => {
                if ($instance.footer.selectButton.IsSelected()) {
                    $instance.picker.Clear();
                } else {
                    $instance.picker.Visible(false);
                    this.sendSelectRootItemRequest();
                }
            });

            $instance.index.browseButton.getEvents().setOnClick(() : void => {
                $instance.index.directoryBrowser.Path($instance.index.targetField.Value());
            });
            $instance.index.directoryBrowser.getEvents().setOnPathRequest(() : void => {
                this.sendMessage({
                    data: $instance.index.directoryBrowser.Value(),
                    task: DesignerTaskType.GET_FILESYSTEM_PATH
                });
            });
            $instance.index.directoryBrowser.getEvents().setOnDirectoryRequest(() : void => {
                this.sendMessage({
                    data: $instance.index.directoryBrowser.Value(),
                    task: DesignerTaskType.GET_FILESYSTEM_DIRECTORY
                });
            });
            $instance.index.componentsList.getEvents().setOnChange(() : void => {
                this.showContent($instance.index.componentsList.Value());
            });
            $instance.index.testsList.getEvents().setOnChange(() : void => {
                this.showContent($instance.index.testsList.Value());
            });

            $instance.header.backButton.getEvents().setOnClick(() : void => {
                this.setLayer(DesignerPanelLayerType.DEFAULT);
            });
            $instance.header.saveButton.getEvents().setOnClick(() : void => {
                $instance.console.Clear();
                this.setLayer(DesignerPanelLayerType.CONSOLE);
                $instance.console.Append("Initializing WUI Builder ..." + StringUtils.NewLine());
                $instance.ShowContentLoader(true);
                this.sendMessage({
                    data: true,
                    task: DesignerTaskType.BUILD
                });
            });

            $instance.footer.componentsButton.getEvents().setOnClick(() : void => {
                this.setLayer(DesignerPanelLayerType.COMPONENTS);
            });
            $instance.footer.consoleButton.getEvents().setOnClick(() : void => {
                this.setLayer(DesignerPanelLayerType.CONSOLE);
            });
            $instance.footer.hierarchyButton.getEvents().setOnClick(() : void => {
                this.setLayer(DesignerPanelLayerType.HIERARCHY);
            });
            $instance.footer.pickerButton.getEvents().setOnClick(() : void => {
                this.showItemsPickArea();
            });
            $instance.footer.selectButton.getEvents().setOnClick(() : void => {
                this.sendInstanceMapRequest();
                this.setLayer(DesignerPanelLayerType.SELECT_AREA);
            });
            $instance.footer.previewButton.getEvents().setOnClick(() : void => {
                this.setLayer(DesignerPanelLayerType.PREVIEW);
            });

            $instance.console.getEvents().setOnDoubleClick(() : void => {
                this.setLayer(DesignerPanelLayerType.PREVIEW);
            });

            $instance.components.getEvents().setOnComplete(() : void => {
                this.resizeContent();
            });
            $instance.components.getEvents().setOnShow(() : void => {
                this.resizeContent();
            });
            $instance.components.deleteButton.getEvents().setOnClick(() : void => {
                this.setProperty(<string>$instance.properties.idField.Value(), "Visible", false);
                this.sendMessage({
                    data: {
                        id: $instance.properties.idField.Value()
                    },
                    task: DesignerTaskType.DELETE_ITEM
                });
                $instance.properties.ClearProperties();
                $instance.components.deleteButton.Enabled(false);
                $instance.ShowItemManagers(false);
            });
            $instance.components.dragElement.getEvents().setOnDragComplete(() : void => {
                const contentOffset : ElementOffset = ElementManager.getAbsoluteOffset($instance.content);
                const targetOffset : ElementOffset = $instance.components.dragElement.getScreenPosition();
                const componentsOffset : ElementOffset = $instance.components.getScreenPosition();
                ElementManager.setPosition($instance.components.dragElement, $instance.components.getSelected().getScreenPosition());

                this.sendMessage({
                    data: {
                        id  : $instance.components.userControlsGroupList.Value() + "." + $instance.components.getSelected().Text(),
                        left: targetOffset.Left() - contentOffset.Left() - componentsOffset.Left(),
                        top : targetOffset.Top() - contentOffset.Top() - componentsOffset.Top()
                    },
                    task: DesignerTaskType.ADD_ITEM
                });
                this.setLayer(DesignerPanelLayerType.DRAG_AREA);
            });
            $instance.components.addButton.getEvents().setOnClick(() : void => {
                this.sendMessage({
                    data: {
                        id: $instance.components.userControlsGroupList.Value() + "." + $instance.components.getSelected().Text()
                    },
                    task: DesignerTaskType.ADD_ITEM
                });
                this.setLayer(DesignerPanelLayerType.DRAG_AREA);
            });
            $instance.components.dragElement.getEvents().setOnDragStart(() : void => {
                this.setLayer(DesignerPanelLayerType.DRAG_AREA);
            });

            $instance.properties.getEvents().setOnComplete(() : void => {
                this.resizeContent();
            });
            $instance.properties.getEvents().setOnShow(() : void => {
                this.resizeContent();
            });
            $instance.properties.setOnPropertyChange(($id : string, $type : string, $value : any) : void => {
                this.setProperty($id, $type, $value);
            });
        }

        protected afterLoad($instance : DesignerPanel) : void {
            $instance.header.zoom.getEvents().setOnChange(() : void => {
                this.setSize();
            });
        }

        private taskHandler($task : DesignerTaskType, $data : any, $instance : DesignerPanel, $args : DesignerPanelViewerArgs) : void {
            switch ($task) {
            case DesignerTaskType.FAIL:
                ExceptionsManager.HandleException($data);
                break;
            case DesignerTaskType.ECHO:
                Echo.Print($data);
                $instance.console.Append($data);
                break;
            case DesignerTaskType.EVENT:
                switch ((<IDesignerEventProtocol>$data).type) {
                case EventType.ON_LOAD:
                    switch ((<IDesignerEventProtocol>$data).args) {
                    case "body":
                        this.setSize();
                        break;
                    case "root":
                        this.sendSelectRootItemRequest();
                        break;
                    default:
                        Echo.Printf("Unrecognized Designer onLoad event type: \"{0}\"", (<IDesignerEventProtocol>$data).args);
                        break;
                    }
                    break;

                case "onBuild":
                    this.setLayer(DesignerPanelLayerType.DRAG_AREA);
                    switch ((<IDesignerEventProtocol>$data).args) {
                    case "start":
                        if (!$instance.console.Visible()) {
                            $instance.console.Clear();
                        }
                        $instance.header.saveButton.Enabled(false);
                        ElementManager.setOpacity($instance.content, 10);
                        $instance.ShowContentLoader(true);
                        $instance.console.Visible(true);
                        break;
                    case "complete":
                        $instance.header.saveButton.Enabled(true);
                        ElementManager.setOpacity($instance.content, 100);
                        $instance.console.Visible(false);
                        this.sendReloadRequest();
                        break;
                    case "fail":
                        $instance.header.saveButton.Enabled(true);
                        ElementManager.setOpacity($instance.content, 100);
                        $instance.ShowContentLoader(false);
                        break;
                    default:
                        Echo.Printf("Unrecognized Designer onBuild event type: \"{0}\"",
                            (<IDesignerEventProtocol>$data).args);
                        break;
                    }
                    break;

                default:
                    Echo.Printf("Unrecognized Designer event type: \"{0}\"", (<IDesignerEventProtocol>$data).type);
                    break;
                }
                break;

            case DesignerTaskType.GET_PROJECT_MAP:
                const projectMap : IDesignerProjectMapProtocol =
                    <IDesignerProjectMapProtocol>$data;
                $instance.index.blankButton.getEvents().setOnClick(() : void => {
                    this.showContent(projectMap.blankViewer);
                });
                this.mapViewers(projectMap.viewers, $args.getIndexArgs().getComponentsList());
                this.mapViewers(projectMap.runtimeTests, $args.getIndexArgs().getTestsList());
                $args.getComponentArgs().getUserControlsType().Clear();
                $args.getComponentArgs().getUserControlsType().Add(new ArrayList<string>(), DesignerComponentType.COMPONENT);
                $args.getComponentArgs().getUserControlsType().Add(new ArrayList<string>(), DesignerComponentType.USER_CONTROL);
                $args.getComponentArgs().getUserControlsType().Add(new ArrayList<string>(), DesignerComponentType.PANEL);
                this.mapGuiObjects(
                    projectMap.guiObjects.components,
                    $args.getComponentArgs().getUserControlsType().getItem(DesignerComponentType.COMPONENT));
                this.mapGuiObjects(
                    projectMap.guiObjects.userControls,
                    $args.getComponentArgs().getUserControlsType().getItem(DesignerComponentType.USER_CONTROL));
                this.mapGuiObjects(
                    projectMap.guiObjects.panels,
                    $args.getComponentArgs().getUserControlsType().getItem(DesignerComponentType.PANEL));

                this.ViewerArgs($args);

                if (this.persistence.Exists("Path")) {
                    this.showContent(this.persistence.Variable("Path"));
                }
                break;

            case DesignerTaskType.SHOW_PROPERTIES:
                $instance.properties.ClearProperties();
                const itemArgs : IGuiCommonsArg[] = (<IDesignerItemArgsProtocol>$data).value;
                let index : number;
                for (index = 0; index < itemArgs.length; index++) {
                    $instance.properties.setProperty(itemArgs[index]);
                }
                break;
            case DesignerTaskType.ITEM_EXISTS:
                $instance.properties.setPropertyError($instance.properties.nameField,
                    <boolean>(<IDesignerItemArgProtocol>$data).value);
                break;
            case DesignerTaskType.MARK_ITEM:
                const markItem : IDesignerItemProtocol = <IDesignerItemProtocol>$data;
                $instance.ShowItemManagers(true);
                $instance.picker.Visible(false);
                $instance.footer.selectButton.IsSelected(false);
                $instance.SelectArea(
                    markItem.top, markItem.left,
                    markItem.width, markItem.height);
                $instance.itemDrag.Visible(markItem.draggable);
                $instance.itemSelector.Enabled(markItem.resizeable);
                break;
            case DesignerTaskType.GET_INSTANCE_MAP:
                const instanceMap : IDesignerInstanceMapItem[] =
                    <IDesignerInstanceMapItem[]>$data;
                $instance.hierarchy.setStructure(instanceMap);
                $instance.picker.setStructure(instanceMap, $instance.header.zoom.Value());
                const itemHandler : any = ($item : GuiCommons, $elementId : string) : void => {
                    $item.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                        if ($eventArgs.Owner().Parent() === $instance.hierarchy || !$instance.footer.selectButton.IsSelected()) {
                            this.sendMessage({
                                data: $elementId,
                                task: DesignerTaskType.SELECT_ITEM
                            });
                            $instance.console.Visible(false);
                            $instance.footer.consoleButton.IsSelected(false);
                            $instance.components.deleteButton.Enabled(true);
                            if ($eventArgs.Owner().Parent() === $instance.hierarchy) {
                                $instance.hierarchy.SelectItem(<GuiCommons>$eventArgs.Owner());
                            }
                        } else {
                            $instance.picker.SelectItem(<GuiCommons>$eventArgs.Owner());
                            $instance.hierarchy.SelectItem($instance.hierarchy.getItems().getItem($elementId));
                        }
                    });
                };
                $instance.hierarchy.getItems().foreach(itemHandler);
                $instance.picker.getItems().foreach(itemHandler);
                break;

            case DesignerTaskType.GET_FILESYSTEM_PATH:
                const structure : any = [
                    {
                        map : [
                            {
                                map  : [],
                                name : "D:/__PROJECTS__/WUI/Source/com-wui-framework-usercontrols",
                                type : "Directory",
                                value: "D:/__PROJECTS__/WUI/Source/com-wui-framework-usercontrols"
                            },
                            {
                                map  : [],
                                name : "D:/__PROJECTS__/WUI/Source/com-wui-framework-services",
                                type : "Directory",
                                value: "D:/__PROJECTS__/WUI/Source/com-wui-framework-services"
                            }
                        ],
                        name: "Recent projects",
                        type: "Recent"
                    }
                ];
                $data.forEach(($item : any) : void => {
                    structure.push($item);
                });
                $instance.index.directoryBrowser.setStructure(structure);
                break;
            case DesignerTaskType.GET_FILESYSTEM_DIRECTORY:
                $instance.index.directoryBrowser.setStructure($data.map, $data.path);
                break;

            default:
                Echo.Printf("Unrecognized Designer task: \"{0}\"", $task);
                Echo.Printf($data);
                break;
            }
        }

        private sendMessage($data : IDesignerProtocol) : void {
            try {
                this.getInstance().content.contentWindow.postMessage($data, "*");
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }

        private setProperty($id : string, $type : string, $value : any) : void {
            if (!ObjectValidator.IsEmptyOrNull($id)) {
                this.sendMessage({
                    data: {
                        id   : $id,
                        name : $type,
                        value: $value
                    },
                    task: DesignerTaskType.SET_PROPERTY
                });
            }
        }

        private setSize() : void {
            const instance : DesignerPanel = this.getInstance();
            this.sendMessage({
                data: {
                    height: instance.content.offsetHeight,
                    width : instance.content.offsetWidth,
                    zoom  : instance.header.zoom.Value()
                },
                task: DesignerTaskType.SET_SIZE
            });
        }

        private sendSelectRootItemRequest() : void {
            this.sendMessage({
                data: true,
                task: DesignerTaskType.SELECT_ROOT_ITEM
            });
        }

        private sendInstanceMapRequest() : void {
            this.sendMessage({
                data: true,
                task: DesignerTaskType.GET_INSTANCE_MAP
            });
        }

        private sendReloadRequest() : void {
            this.sendMessage({
                data: true,
                task: DesignerTaskType.RELOAD
            });
        }

        private resizeContent() : void {
            const instance : DesignerPanel = this.getInstance();
            const windowSize : Size = WindowManager.getSize();
            instance.Width(windowSize.Width() - 30);
            instance.Height(windowSize.Height());
            instance.header.Width(instance.Width());
            instance.footer.Width(instance.Width());
            let contentHeight : number = instance.Height() - 100;
            const contentWidth : number = instance.Width() - instance.components.Width() - instance.properties.Width() - 30;
            ElementManager.setCssProperty(instance.content, "width", contentWidth);
            instance.picker.Width(contentWidth);
            instance.console.Width(contentWidth);

            instance.Height(contentHeight);
            instance.index.Height(contentHeight);
            contentHeight -= 100;
            instance.components.Height(contentHeight);
            instance.hierarchy.Height(contentHeight);
            ElementManager.setCssProperty(instance.content, "height", contentHeight);
            instance.picker.Height(contentHeight);
            instance.console.Height(contentHeight);
            instance.properties.Height(contentHeight);
        }

        private showContent($path : string) : void {
            this.setLayer(DesignerPanelLayerType.EDITOR);
            this.getInstance().TargetPath($path);
            this.persistence.Variable("Path", $path);

            /// todo: create Designer task for ability to get list of Layers and also ability to switch layers
            // $instance.header.layerList.Clear();
            // $instance.header.layerList.Add();
            this.resizeContent();
            this.setSize();
            this.sendReloadRequest();
        }

        private showItemsPickArea() : void {
            this.sendInstanceMapRequest();
            this.setLayer(DesignerPanelLayerType.PICK_AREA);
        }

        private mapViewers($input : IDesignerProjectMapViewers, $output : ArrayList<string>) : void {
            $output.Clear();
            let index : number;
            for (index = 0; index < $input.names.length; index++) {
                $output.Add($input.names[index], $input.links[index]);
            }
        }

        private mapGuiObjects($input : IDesignerProjectMapItems, $output : ArrayList<string>) : void {
            $output.Clear();
            let index : number;
            for (index = 0; index < $input.names.length; index++) {
                $output.Add($input.names[index]);
            }
        }
    }
}
