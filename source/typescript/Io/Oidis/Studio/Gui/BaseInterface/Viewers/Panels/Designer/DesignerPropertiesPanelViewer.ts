/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerPropertiesPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerPropertiesPanel;
    import DesignerPropertiesPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerPropertiesPanelViewerArgs;
    import BaseViewerLayerType = Io.Oidis.Gui.Enums.BaseViewerLayerType;

    export class DesignerPropertiesPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DesignerPropertiesPanelViewerArgs {
            const args : DesignerPropertiesPanelViewerArgs = new DesignerPropertiesPanelViewerArgs();
            args.IdText("Id:");
            args.NameText("Name:");
            args.StyleClassText("CSS class:");
            args.EnabledText("Enabled");
            args.VisibleText("Visible");
            args.TitleText("Tooltip:");
            args.ValueText("Value:");
            args.WidthText("Width:");
            args.HeightText("Height:");
            args.ErrorText("Error state");
            args.TabIndexText("Tab index:");
            args.TopText("Top:");
            args.LeftText("Left:");
            args.GuiTypeText("GUI type:");
            args.GuiTypeHint("Choose type");

            return args;
        }

        /* dev:end */

        constructor($args? : DesignerPropertiesPanelViewerArgs) {
            super($args);
            this.setInstance(new DesignerPropertiesPanel());
        }

        public getInstance() : DesignerPropertiesPanel {
            return <DesignerPropertiesPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerPropertiesPanelViewer#
         * @param {DesignerPropertiesPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {DesignerPropertiesPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : DesignerPropertiesPanelViewerArgs) : DesignerPropertiesPanelViewerArgs {
            return <DesignerPropertiesPanelViewerArgs>super.ViewerArgs($args);
        }

        protected layerHandler($layer : BaseViewerLayerType, $instance : DesignerPropertiesPanel) : void {
            switch ($layer) {

            default:
                $instance.titleField.Visible(false);
                $instance.valueField.Visible(false);
                $instance.widthField.Visible(false);
                $instance.heightField.Visible(false);
                $instance.enabledBox.Visible(false);
                $instance.errorBox.Visible(false);
                $instance.tabIndexField.Visible(false);
                $instance.topField.Visible(false);
                $instance.leftField.Visible(false);
                $instance.guiTypeList.Visible(false);
                break;
            }
        }

        protected argsHandler($instance : DesignerPropertiesPanel, $args : DesignerPropertiesPanelViewerArgs) : void {
            $instance.idField.Configuration().Name($args.IdText());
            $instance.nameField.Configuration().Name($args.NameText());
            $instance.styleClassField.Configuration().Name($args.StyleClassText());
            $instance.enabledBox.Configuration().Name($args.EnabledText());
            $instance.visibleBox.Configuration().Name($args.VisibleText());
            $instance.titleField.Configuration().Name($args.TitleText());
            $instance.valueField.Configuration().Name($args.ValueText());
            $instance.widthField.Configuration().Name($args.WidthText());
            $instance.heightField.Configuration().Name($args.HeightText());
            $instance.errorBox.Configuration().Name($args.ErrorText());
            $instance.tabIndexField.Configuration().Name($args.TabIndexText());
            $instance.topField.Configuration().Name($args.TopText());
            $instance.leftField.Configuration().Name($args.LeftText());
            $instance.guiTypeList.Configuration().Name($args.GuiTypeText());
            $instance.guiTypeList.Configuration().Hint($args.GuiTypeHint());
        }
    }
}
