/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Installer {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import InstallerInitialScreenPanel =
        Io.Oidis.Studio.Gui.BaseInterface.Panels.Installer.InstallerInitialScreenPanel;
    import InstallerInitialScreenPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Installer.InstallerInitialScreenPanelViewerArgs;

    export class InstallerInitialScreenPanelViewer extends Io.Oidis.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : InstallerInitialScreenPanelViewerArgs {
            const args : InstallerInitialScreenPanelViewerArgs = new InstallerInitialScreenPanelViewerArgs();
            args.InstallationLabelText("WUI Framework installation");
            args.InstallPathText("WUI Framework root:");
            args.WorkplacePathText("Local WUI workplace:");
            args.InstallButtonText("Install");

            return args;
        }

        /* dev:end */

        constructor($args? : InstallerInitialScreenPanelViewerArgs) {
            super($args);
            this.setInstance(new InstallerInitialScreenPanel());
        }

        public getInstance() : InstallerInitialScreenPanel {
            return <InstallerInitialScreenPanel>super.getInstance();
        }

        /**
         * @method ViewerArgs
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Installer.InstallerInitialScreenPanelViewer#
         * @param {InstallerInitialScreenPanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {InstallerInitialScreenPanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : InstallerInitialScreenPanelViewerArgs) : InstallerInitialScreenPanelViewerArgs {
            return <InstallerInitialScreenPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : InstallerInitialScreenPanel, $args : InstallerInitialScreenPanelViewerArgs) : void {
            $instance.installationLabel.Text($args.InstallationLabelText());
            $instance.installPath.Configuration().Name($args.InstallPathText());
            $instance.workplacePath.Configuration().Name($args.WorkplacePathText());
            $instance.installButton.Text($args.InstallButtonText());
        }

        protected beforeLoad($instance : InstallerInitialScreenPanel) : void {
            $instance.installButton.Enabled(false);
        }
    }
}
