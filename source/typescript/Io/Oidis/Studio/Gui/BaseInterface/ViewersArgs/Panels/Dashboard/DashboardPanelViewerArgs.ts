/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    export class DashboardPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private localhostInfoButtonText : string;
        private toolchainInfoButtonText : string;
        private projectsButtonText : string;
        private headerLabelText : string;
        private localhostInfoArgs : LocalhostInfoPanelViewerArgs;
        private toolchainInfoArgs : ToolchainInfoPanelViewerArgs;
        private projectsArgs : ProjectsManagerPanelViewerArgs;

        constructor() {
            super();

            this.localhostInfoButtonText = "";
            this.toolchainInfoButtonText = "";
            this.projectsButtonText = "";
            this.headerLabelText = "";
            this.localhostInfoArgs = new LocalhostInfoPanelViewerArgs();
            this.toolchainInfoArgs = new ToolchainInfoPanelViewerArgs();
            this.projectsArgs = new ProjectsManagerPanelViewerArgs();
        }

        public LocalhostInfoButtonText($value? : string) : string {
            return this.localhostInfoButtonText = Property.String(this.localhostInfoButtonText, $value);
        }

        public ToolchainInfoButtonText($value? : string) : string {
            return this.toolchainInfoButtonText = Property.String(this.toolchainInfoButtonText, $value);
        }

        public ProjectsButtonText($value? : string) : string {
            return this.projectsButtonText = Property.String(this.projectsButtonText, $value);
        }

        public HeaderLabelText($value? : string) : string {
            return this.headerLabelText = Property.String(this.headerLabelText, $value);
        }

        public LocalhostInfoArgs($value? : LocalhostInfoPanelViewerArgs) : LocalhostInfoPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(LocalhostInfoPanelViewerArgs)) {
                this.localhostInfoArgs = $value;
            }
            return this.localhostInfoArgs;
        }

        public ToolchainInfoArgs($value? : ToolchainInfoPanelViewerArgs) : ToolchainInfoPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ToolchainInfoPanelViewerArgs)) {
                this.toolchainInfoArgs = $value;
            }
            return this.toolchainInfoArgs;
        }

        public ProjectsArgs($value? : ProjectsManagerPanelViewerArgs) : ProjectsManagerPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ProjectsManagerPanelViewerArgs)) {
                this.projectsArgs = $value;
            }
            return this.projectsArgs;
        }
    }
}
