/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    export class LocalhostInfoPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private localhostStatus : string;
        private locationText : string;
        private serverRootText : string;
        private serverLogsText : string;
        private configFilesText : string;
        private certsUpdate : string;

        constructor() {
            super();

            this.localhostStatus = "";
            this.locationText = "";
            this.serverRootText = "";
            this.serverLogsText = "";
            this.configFilesText = "";
            this.certsUpdate = "";
        }

        public LocalhostStatus($value? : string) : string {
            return this.localhostStatus = Property.String(this.localhostStatus, $value);
        }

        public LocationText($value? : string) : string {
            return this.locationText = Property.String(this.locationText, $value);
        }

        public ServerRootText($value? : string) : string {
            return this.serverRootText = Property.String(this.serverRootText, $value);
        }

        public ServerLogsText($value? : string) : string {
            return this.serverLogsText = Property.String(this.serverLogsText, $value);
        }

        public ConfigFilesText($value? : string) : string {
            return this.configFilesText = Property.String(this.configFilesText, $value);
        }

        public CertsUpdate($value? : string) : string {
            return this.certsUpdate = Property.String(this.certsUpdate, $value);
        }
    }
}
