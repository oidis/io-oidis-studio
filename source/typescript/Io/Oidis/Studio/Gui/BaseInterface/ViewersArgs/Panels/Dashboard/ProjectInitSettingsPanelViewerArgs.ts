/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} ProjectInitSettingsPanelViewerArgs
     */

    export class ProjectInitSettingsPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private namespaceText : string;
        private extendsText : string;
        private targetPathText : string;
        private targetPath : string;
        private nameText : string;
        private titleText : string;
        private descriptionText : string;
        private repositoryText : string;

        /**
         * @class ProjectInitSettingsPanelViewerArgs
         * @classdesc ProjectInitSettingsPanelViewerArgs is structure handling ProjectInitSettingsPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard
         */
        constructor() {
            super();

            this.namespaceText = "";
            this.extendsText = "";
            this.targetPathText = "";
            this.targetPath = "";
            this.nameText = "";
            this.titleText = "";
            this.descriptionText = "";
            this.repositoryText = "";
        }

        public NamespaceText($value? : string) : string {
            return this.namespaceText = Property.String(this.namespaceText, $value);
        }

        public ExtendsText($value? : string) : string {
            return this.extendsText = Property.String(this.extendsText, $value);
        }

        public TargetPathText($value? : string) : string {
            return this.targetPathText = Property.String(this.targetPathText, $value);
        }

        public TargetPath($value? : string) : string {
            return this.targetPath = Property.String(this.targetPath, $value);
        }

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public TitleText($value? : string) : string {
            return this.titleText = Property.String(this.titleText, $value);
        }

        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }

        public RepositoryText($value? : string) : string {
            return this.repositoryText = Property.String(this.repositoryText, $value);
        }
    }
}
