/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import BaseFormInputArgs = Io.Oidis.UserControls.Structures.BaseFormInputArgs;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import Property = Io.Oidis.Commons.Utils.Property;

    export class ProjectSettingsPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private items : ArrayList<BaseFormInputArgs>;
        private saveText : string;

        constructor() {
            super();

            this.items = new ArrayList<BaseFormInputArgs>();
            this.saveText = "";
        }

        public AddItem($value : BaseFormInputArgs) : void {
            this.items.Add($value);
        }

        public Items($value? : ArrayList<BaseFormInputArgs>) : ArrayList<BaseFormInputArgs> {
            if (ObjectValidator.IsSet($value)) {
                this.items = $value;
            }
            return this.items;
        }

        public SaveText($value? : string) : string {
            return this.saveText = Property.String(this.saveText, $value);
        }
    }
}
