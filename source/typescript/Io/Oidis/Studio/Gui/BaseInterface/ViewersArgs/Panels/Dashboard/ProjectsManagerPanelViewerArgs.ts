/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    export class ProjectsManagerPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private remoteSourcesText : string;
        private localSourcesText : string;
        private installProjectButtonText : string;
        private workplacePathText : string;

        constructor() {
            super();

            this.remoteSourcesText = "";
            this.localSourcesText = "";
            this.installProjectButtonText = "";
            this.workplacePathText = "";
        }

        public RemoteSourcesText($value? : string) : string {
            return this.remoteSourcesText = Property.String(this.remoteSourcesText, $value);
        }

        public LocalSourcesText($value? : string) : string {
            return this.localSourcesText = Property.String(this.localSourcesText, $value);
        }

        public InstallProjectButtonText($value? : string) : string {
            return this.installProjectButtonText = Property.String(this.installProjectButtonText, $value);
        }

        public WorkplacePathText($value? : string) : string {
            return this.workplacePathText = Property.String(this.workplacePathText, $value);
        }
    }
}
