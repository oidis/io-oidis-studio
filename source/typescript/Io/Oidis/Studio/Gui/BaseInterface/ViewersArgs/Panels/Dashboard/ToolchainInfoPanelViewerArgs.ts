/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    export class ToolchainInfoPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private checkButtonText : string;
        private headerText : string;

        constructor() {
            super();

            this.checkButtonText = "";
            this.headerText = "";
        }

        public CheckButtonText($value? : string) : string {
            return this.checkButtonText = Property.String(this.checkButtonText, $value);
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }
    }
}
