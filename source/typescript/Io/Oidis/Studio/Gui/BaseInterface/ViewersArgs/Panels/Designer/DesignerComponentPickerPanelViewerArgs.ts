/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    /**
     * @typedef {Object} DesignerComponentPickerPanelViewerArgs
     */

    export class DesignerComponentPickerPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private structure : any[];

        /**
         * @class DesignerComponentPickerPanelViewerArgs
         * @classdesc DesignerComponentPickerPanelViewerArgs is structure handling DesignerComponentPickerPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.structure = [];
        }

        public Structure($value? : any[]) : any[] {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.structure = $value;
            }
            return this.structure;
        }
    }
}
