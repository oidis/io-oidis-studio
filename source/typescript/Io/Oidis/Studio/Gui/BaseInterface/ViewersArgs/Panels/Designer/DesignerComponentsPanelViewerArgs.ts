/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;

    /**
     * @typedef {Object} DesignerComponentsPanelViewerArgs
     */

    export class DesignerComponentsPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private userControlsTypeHint : string;
        private userControlsGroupHint : string;
        private addButtonText : string;
        private deleteButtonText : string;
        private readonly componentsType : ArrayList<string>;
        private readonly userControlsType : ArrayList<ArrayList<string>>;

        /**
         * @class DesignerComponentsPanelViewerArgs
         * @classdesc DesignerComponentsPanelViewerArgs is structure handling DesignerComponentsPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.userControlsTypeHint = "";
            this.userControlsGroupHint = "";
            this.addButtonText = "";
            this.deleteButtonText = "";
            this.componentsType = new ArrayList<string>();
            this.userControlsType = new ArrayList<ArrayList<string>>();
        }

        public UserControlsTypeHint($value? : string) : string {
            return this.userControlsTypeHint = Property.String(this.userControlsTypeHint, $value);
        }

        public UserControlsGroupHint($value? : string) : string {
            return this.userControlsGroupHint = Property.String(this.userControlsGroupHint, $value);
        }

        public AddButtonText($value? : string) : string {
            return this.addButtonText = Property.String(this.addButtonText, $value);
        }

        public DeleteButtonText($value? : string) : string {
            return this.deleteButtonText = Property.String(this.deleteButtonText, $value);
        }

        public getComponentsType() : ArrayList<string> {
            return this.componentsType;
        }

        public getUserControlsType() : ArrayList<ArrayList<string>> {
            return this.userControlsType;
        }
    }
}
