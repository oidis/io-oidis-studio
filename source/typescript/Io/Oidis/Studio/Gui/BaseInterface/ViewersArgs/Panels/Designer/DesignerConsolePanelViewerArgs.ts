/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} DesignerConsolePanelViewerArgs
     */

    export class DesignerConsolePanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private contentText : string;

        /**
         * @class DesignerConsolePanelViewerArgs
         * @classdesc DesignerConsolePanelViewerArgs is structure handling DesignerConsolePanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.contentText = "";
        }

        public ContentText($value? : string) : string {
            return this.contentText = Property.String(this.contentText, $value);
        }
    }
}
