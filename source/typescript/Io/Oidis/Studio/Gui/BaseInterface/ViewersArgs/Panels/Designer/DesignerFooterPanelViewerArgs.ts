/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} DesignerFooterPanelViewerArgs
     */

    export class DesignerFooterPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private componentsButtonText : string;
        private hierarchyButtonText : string;
        private consoleButtonText : string;
        private pickerButtonText : string;
        private selectButtonText : string;
        private previewButtonText : string;

        /**
         * @class DesignerFooterPanelViewerArgs
         * @classdesc DesignerFooterPanelViewerArgs is structure handling DesignerFooterPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.componentsButtonText = "";
            this.hierarchyButtonText = "";
            this.consoleButtonText = "";
            this.pickerButtonText = "";
            this.selectButtonText = "";
            this.previewButtonText = "";
        }

        public ComponentsButtonText($value? : string) : string {
            return this.componentsButtonText = Property.String(this.componentsButtonText, $value);
        }

        public HierarchyButtonText($value? : string) : string {
            return this.hierarchyButtonText = Property.String(this.hierarchyButtonText, $value);
        }

        public ConsoleButtonText($value? : string) : string {
            return this.consoleButtonText = Property.String(this.consoleButtonText, $value);
        }

        public PickerButtonText($value? : string) : string {
            return this.pickerButtonText = Property.String(this.pickerButtonText, $value);
        }

        public SelectButtonText($value? : string) : string {
            return this.selectButtonText = Property.String(this.selectButtonText, $value);
        }

        public PreviewButtonText($value? : string) : string {
            return this.previewButtonText = Property.String(this.previewButtonText, $value);
        }
    }
}
