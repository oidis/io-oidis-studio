/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} DesignerHeaderPanelViewerArgs
     */

    export class DesignerHeaderPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private backButtonText : string;
        private saveButtonText : string;
        private layerLabelText : string;
        private zoomLabelText : string;

        /**
         * @class DesignerHeaderPanelViewerArgs
         * @classdesc DesignerHeaderPanelViewerArgs is structure handling DesignerHeaderPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.backButtonText = "";
            this.saveButtonText = "";
            this.layerLabelText = "";
            this.zoomLabelText = "";
        }

        public BackButtonText($value? : string) : string {
            return this.backButtonText = Property.String(this.backButtonText, $value);
        }

        public SaveButtonText($value? : string) : string {
            return this.saveButtonText = Property.String(this.saveButtonText, $value);
        }

        public LayerLabelText($value? : string) : string {
            return this.layerLabelText = Property.String(this.layerLabelText, $value);
        }

        public ZoomLabelText($value? : string) : string {
            return this.zoomLabelText = Property.String(this.zoomLabelText, $value);
        }
    }
}
