/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;

    /**
     * @typedef {Object} DesignerIndexPanelViewerArgs
     */

    export class DesignerIndexPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private targetText : string;
        private browseButtonText : string;
        private selectButtonText : string;
        private componentsLabelText : string;
        private componentsListHint : string;
        private readonly componentsList : ArrayList<string>;
        private testsLabelText : string;
        private testsListHint : string;
        private readonly testsList : ArrayList<string>;
        private blankButtonText : string;

        /**
         * @class DesignerIndexPanelViewerArgs
         * @classdesc DesignerIndexPanelViewerArgs is structure handling DesignerIndexPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.targetText = "";
            this.browseButtonText = "";
            this.selectButtonText = "";
            this.componentsLabelText = "";
            this.componentsListHint = "";
            this.componentsList = new ArrayList<string>();
            this.testsLabelText = "";
            this.testsListHint = "";
            this.testsList = new ArrayList<string>();
            this.blankButtonText = "";
        }

        public TargetText($value? : string) : string {
            return this.targetText = Property.String(this.targetText, $value);
        }

        public BrowseButtonText($value? : string) : string {
            return this.browseButtonText = Property.String(this.browseButtonText, $value);
        }

        public SelectButtonText($value? : string) : string {
            return this.selectButtonText = Property.String(this.selectButtonText, $value);
        }

        public ComponentsLabelText($value? : string) : string {
            return this.componentsLabelText = Property.String(this.componentsLabelText, $value);
        }

        public ComponentsListHint($value? : string) : string {
            return this.componentsListHint = Property.String(this.componentsListHint, $value);
        }

        public getComponentsList() : ArrayList<string> {
            return this.componentsList;
        }

        public TestsLabelText($value? : string) : string {
            return this.testsLabelText = Property.String(this.testsLabelText, $value);
        }

        public TestsListHint($value? : string) : string {
            return this.testsListHint = Property.String(this.testsListHint, $value);
        }

        public getTestsList() : ArrayList<string> {
            return this.testsList;
        }

        public BlankButtonText($value? : string) : string {
            return this.blankButtonText = Property.String(this.blankButtonText, $value);
        }
    }
}
