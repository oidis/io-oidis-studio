/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} DesignerPanelViewerArgs
     */

    export class DesignerPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private readonly indexArgs : DesignerIndexPanelViewerArgs;
        private readonly headerArgs : DesignerHeaderPanelViewerArgs;
        private readonly componentArgs : DesignerComponentsPanelViewerArgs;
        private readonly hierarchyArgs : DesignerHierarchyPanelViewerArgs;
        private readonly pickerArgs : DesignerComponentPickerPanelViewerArgs;
        private readonly consoleArgs : DesignerConsolePanelViewerArgs;
        private readonly propertiesArgs : DesignerPropertiesPanelViewerArgs;
        private readonly footerArgs : DesignerFooterPanelViewerArgs;
        private targetPath : string;

        /**
         * @class DesignerPanelViewerArgs
         * @classdesc DesignerPanelViewerArgs is structure handling DesignerPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels
         */
        constructor() {
            super();

            this.indexArgs = new DesignerIndexPanelViewerArgs();
            this.headerArgs = new DesignerHeaderPanelViewerArgs();
            this.componentArgs = new DesignerComponentsPanelViewerArgs();
            this.hierarchyArgs = new DesignerHierarchyPanelViewerArgs();
            this.pickerArgs = new DesignerComponentPickerPanelViewerArgs();
            this.consoleArgs = new DesignerConsolePanelViewerArgs();
            this.propertiesArgs = new DesignerPropertiesPanelViewerArgs();
            this.footerArgs = new DesignerFooterPanelViewerArgs();
            this.targetPath = "";
        }

        public getIndexArgs() : DesignerIndexPanelViewerArgs {
            return this.indexArgs;
        }

        public getHeaderArgs() : DesignerHeaderPanelViewerArgs {
            return this.headerArgs;
        }

        public getComponentArgs() : DesignerComponentsPanelViewerArgs {
            return this.componentArgs;
        }

        public getHierarchyArgs() : DesignerHierarchyPanelViewerArgs {
            return this.hierarchyArgs;
        }

        public getPickerArgs() : DesignerComponentPickerPanelViewerArgs {
            return this.pickerArgs;
        }

        public getConsoleArgs() : DesignerConsolePanelViewerArgs {
            return this.consoleArgs;
        }

        public getPropertiesArgs() : DesignerPropertiesPanelViewerArgs {
            return this.propertiesArgs;
        }

        public getFooterArgs() : DesignerFooterPanelViewerArgs {
            return this.footerArgs;
        }

        public TargetPath($value? : string) : string {
            return this.targetPath = Property.String(this.targetPath, $value);
        }
    }
}
