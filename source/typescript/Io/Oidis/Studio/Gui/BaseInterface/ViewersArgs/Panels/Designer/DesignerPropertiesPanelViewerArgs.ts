/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} DesignerPropertiesPanelViewerArgs
     */

    export class DesignerPropertiesPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private idText : string;
        private nameText : string;
        private styleClassText : string;
        private enabledText : string;
        private visibleText : string;
        private titleText : string;
        private valueText : string;
        private widthText : string;
        private heightText : string;
        private errorText : string;
        private tabIndexText : string;
        private topText : string;
        private leftText : string;
        private guiTypeText : string;
        private guiTypeHint : string;

        /**
         * @class DesignerPropertiesPanelViewerArgs
         * @classdesc DesignerPropertiesPanelViewerArgs is structure handling DesignerPropertiesPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer
         */
        constructor() {
            super();

            this.idText = "";
            this.nameText = "";
            this.styleClassText = "";
            this.enabledText = "";
            this.visibleText = "";
            this.titleText = "";
            this.valueText = "";
            this.widthText = "";
            this.heightText = "";
            this.errorText = "";
            this.tabIndexText = "";
            this.topText = "";
            this.leftText = "";
            this.guiTypeText = "";
            this.guiTypeHint = "";
        }

        public IdText($value? : string) : string {
            return this.idText = Property.String(this.idText, $value);
        }

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public StyleClassText($value? : string) : string {
            return this.styleClassText = Property.String(this.styleClassText, $value);
        }

        public EnabledText($value? : string) : string {
            return this.enabledText = Property.String(this.enabledText, $value);
        }

        public VisibleText($value? : string) : string {
            return this.visibleText = Property.String(this.visibleText, $value);
        }

        public TitleText($value? : string) : string {
            return this.titleText = Property.String(this.titleText, $value);
        }

        public ValueText($value? : string) : string {
            return this.valueText = Property.String(this.valueText, $value);
        }

        public WidthText($value? : string) : string {
            return this.widthText = Property.String(this.widthText, $value);
        }

        public HeightText($value? : string) : string {
            return this.heightText = Property.String(this.heightText, $value);
        }

        public ErrorText($value? : string) : string {
            return this.errorText = Property.String(this.errorText, $value);
        }

        public TabIndexText($value? : string) : string {
            return this.tabIndexText = Property.String(this.tabIndexText, $value);
        }

        public TopText($value? : string) : string {
            return this.topText = Property.String(this.topText, $value);
        }

        public LeftText($value? : string) : string {
            return this.leftText = Property.String(this.leftText, $value);
        }

        public GuiTypeText($value? : string) : string {
            return this.guiTypeText = Property.String(this.guiTypeText, $value);
        }

        public GuiTypeHint($value? : string) : string {
            return this.guiTypeHint = Property.String(this.guiTypeHint, $value);
        }
    }
}
