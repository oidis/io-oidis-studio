/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Installer {
    "use strict";
    import Property = Io.Oidis.Commons.Utils.Property;

    /**
     * @typedef {Object} InstallerInitialScreenPanelViewerArgs
     */

    export class InstallerInitialScreenPanelViewerArgs extends Io.Oidis.Gui.Primitives.BasePanelViewerArgs {
        private installationLabelText : string;
        private installPathText : string;
        private workplacePathText : string;
        private installButtonText : string;

        /**
         * @class InstallerInitialScreenPanelViewerArgs
         * @classdesc InstallerInitialScreenPanelViewerArgs is structure handling InstallerInitialScreenPanel content.
         * @memberOf Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Installer
         */
        constructor() {
            super();

            this.installationLabelText = "";
            this.installPathText = "";
            this.workplacePathText = "";
            this.installButtonText = "";
        }

        public InstallationLabelText($value? : string) : string {
            return this.installationLabelText = Property.String(this.installationLabelText, $value);
        }

        public InstallPathText($value? : string) : string {
            return this.installPathText = Property.String(this.installPathText, $value);
        }

        public InstallButtonText($value? : string) : string {
            return this.installButtonText = Property.String(this.installButtonText, $value);
        }

        public WorkplacePathText($value? : string) : string {
            return this.workplacePathText = Property.String(this.workplacePathText, $value);
        }
    }
}
