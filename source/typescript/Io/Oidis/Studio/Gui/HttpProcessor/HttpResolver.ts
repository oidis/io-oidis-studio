/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui.HttpProcessor {
    "use strict";

    export class HttpResolver extends Io.Oidis.Studio.UserControls.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.Oidis.Studio.Gui.Index;
            resolvers["/index"] = Io.Oidis.Studio.Gui.Index;
            resolvers["/web/"] = Io.Oidis.Studio.Gui.Index;
            return resolvers;
        }
    }
}
