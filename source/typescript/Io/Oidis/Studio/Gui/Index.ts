/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui {
    "use strict";
    import StaticPageContentManger = Io.Oidis.Gui.Utils.StaticPageContentManager;
    import Viewers = Io.Oidis.Studio.Gui.BaseInterface.Viewers;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>WUI Studio GUI Library</h1>" +
                "<h3>WUI Framework's library focused on GUI for WUI Studio</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Pages</H3>" +

                "<H3>Dialogs</H3>" +

                "<H3>Panels</H3>" +

                "<H4>Designer</H4>" +
                "<a href=\"" + Viewers.Panels.Designer.DesignerIndexPanelViewer.CallbackLink(true) + "\">Designer Index</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Designer.DesignerComponentsPanelViewer.CallbackLink(true) + "\">Designer Components</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Designer.DesignerPropertiesPanelViewer.CallbackLink(true) + "\">Designer Properties</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Designer.DesignerComponentPickerPanelViewer.CallbackLink(true) + "\">" +
                "Designer Component Picker</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Designer.DesignerConsolePanelViewer.CallbackLink(true) + "\">Designer Console</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Designer.DesignerPanelViewer.CallbackLink(true) + "\">Designer</a>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +

                "<H4>Dashboard</H4>" +
                "<a href=\"" + Viewers.Panels.Dashboard.ProjectInitSettingsPanelViewer.CallbackLink(true) + "\">" +
                "Project Init Settings</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Dashboard.LocalhostInfoPanelViewer.CallbackLink(true) + "\">" +
                "Localhost Info</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Dashboard.ToolchainInfoPanelViewer.CallbackLink(true) + "\">" +
                "Toolchain Info</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Dashboard.ProjectsManagerPanelViewer.CallbackLink(true) + "\">" +
                "Projects Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Panels.Dashboard.DashboardPanelViewer.CallbackLink(true) + "\">" +
                "Dashboard</a>" +
                StringUtils.NewLine() +

                "<H4>Installer</H4>" +
                "<a href=\"" + Viewers.Panels.Installer.InstallerInitialScreenPanelViewer.CallbackLink(true) + "\">" +
                "Initial Screen</a>" +
                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +

                StringUtils.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI Studio - GUI Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
