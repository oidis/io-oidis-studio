/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Gui {
    "use strict";
    import HttpResolver = Io.Oidis.Studio.Gui.HttpProcessor.HttpResolver;

    /**
     * Loader class provides handling of application content singleton.
     */
    export class Loader extends Io.Oidis.Studio.UserControls.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver(this.getEnvironmentArgs().getProjectName());
        }
    }
}
