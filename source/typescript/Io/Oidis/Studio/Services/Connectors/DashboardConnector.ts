/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Connectors {
    "use strict";
    import WebServiceClientType = Io.Oidis.Services.Enums.WebServiceClientType;
    import BaseConnector = Io.Oidis.Services.Primitives.BaseConnector;
    import IProject = Io.Oidis.Commons.Interfaces.IProject;
    import JsonUtils = Io.Oidis.Commons.Utils.JsonUtils;
    import IAcknowledgePromise = Io.Oidis.Services.Connectors.IAcknowledgePromise;

    export class DashboardConnector extends BaseConnector {

        constructor($port : number) {
            super(3, "http://localhost:" + $port + "/connector.config.jsonp", WebServiceClientType.WUI_DESKTOP_CONNECTOR);
        }

        public getSchema() : IProjectSchemaPromise {
            return this.invoke("getSchema").DataFormatter(($data : any) : any => {
                return JsonUtils.ParseRefSymbols($data);
            });
        }

        public getProjectConfig() : IProjectConfigPromise {
            return this.invoke("getProjectConfig").DataFormatter(($data : any) : any => {
                return JsonUtils.ParseRefSymbols($data);
            });
        }

        public SaveProjectConfig($config : IProject) : IAcknowledgePromise {
            return this.invoke("SaveProjectConfig", $config);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_DESKTOP_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Io.Oidis.Builder.Connectors.DashboardConnector";
            return namespaces;
        }
    }

    export interface IProjectSchemaPromise {
        Then($callback : ($schema : any) => void) : void;
    }

    export interface IProjectConfigPromise {
        Then($callback : ($config : IProject) => void) : void;
    }
}
