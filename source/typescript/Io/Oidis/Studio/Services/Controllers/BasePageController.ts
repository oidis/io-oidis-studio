/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import EventType = Io.Oidis.Commons.Enums.Events.EventType;
    import TerminalConnector = Io.Oidis.Services.Connectors.TerminalConnector;
    import FileSystemHandlerConnector = Io.Oidis.Services.Connectors.FileSystemHandlerConnector;
    import ProgressEventArgs = Io.Oidis.Commons.Events.Args.ProgressEventArgs;
    import ReportServiceConnector = Io.Oidis.Services.Connectors.ReportServiceConnector;
    import FileSystemDownloadOptions = Io.Oidis.Services.Connectors.FileSystemDownloadOptions;
    import HttpMethodType = Io.Oidis.Commons.Enums.HttpMethodType;
    import WebServiceClientFactory = Io.Oidis.Services.WebServiceApi.WebServiceClientFactory;
    import ObjectEncoder = Io.Oidis.Commons.Utils.ObjectEncoder;
    import WindowHandlerConnector = Io.Oidis.Services.Connectors.WindowHandlerConnector;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import Icon = Io.Oidis.UserControls.BaseInterface.UserControls.Icon;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import ImageButton = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.ImageButton;
    import ResizeBar = Io.Oidis.UserControls.BaseInterface.Components.ResizeBar;
    import ImageButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.IconType;
    import UpdatesManagerConnector = Io.Oidis.Services.Connectors.UpdatesManagerConnector;

    export class BasePageController extends Io.Oidis.Services.HttpProcessor.Resolvers.BasePageController {
        protected restService : ReportServiceConnector;
        protected fileSystem : FileSystemHandlerConnector;
        protected terminal : TerminalConnector;

        constructor() {
            super();

            this.setPageTitle("WUI Studio v" + this.getEnvironmentArgs().getProjectVersion());
            this.loaderIcon.Visible(false);
            this.appIcon.Visible(false);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.ROUND);
            this.minimizeButton.IconName(IconType.MINUS);
            this.minimizeButton.StyleClassName("AppMinimize");
            this.maximizeButton.Visible(false);
            this.closeButton.GuiType(ImageButtonType.ROUND);
            this.closeButton.IconName(IconType.CROSS);
            this.closeButton.StyleClassName("AppClose");

            this.restService = new ReportServiceConnector(true,
                "https://localhost.wuiframework.com/com-wui-framework-rest-services/xorigin/" +
                "connector.config.jsonp");
            this.terminal = new TerminalConnector();
            this.fileSystem = new FileSystemHandlerConnector();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                if (this.getEnvironmentArgs().IsProductionMode()) {
                    LogIt.setOnPrint(($message : string) : void => {
                        Loader.getInstance().ApplicationLog(Loader.getInstance().ApplicationLog() + $message);
                        this.restService.LogIt($message);
                    });
                }
            });

            super.resolver();
        }

        protected selfinstall() : ISelfinstallPromise {
            const callbacks : any = {
                onChange($args : ProgressEventArgs) : void {
                    // declare default callback
                },
                onValidate($success : boolean) : void {
                    // declare default callback
                },
                selfinstallThen($success : boolean, $message? : string) : void {
                    // declare default callback
                }
            };
            new UpdatesManagerConnector()
                .UpdateExists(
                    this.getEnvironmentArgs().getProjectName(), this.getEnvironmentArgs().getReleaseName(),
                    this.getEnvironmentArgs().getPlatform(), this.getEnvironmentArgs().getProjectVersion(),
                    new Date(this.getEnvironmentArgs().getBuildTime()).getTime())
                .Then(($status : boolean) : void => {
                    callbacks.onValidate($status);
                    if ($status) {
                        this.fileSystem
                            .Download(<FileSystemDownloadOptions>{
                                body   : "jsonData=" + JSON.stringify({
                                    data  : ObjectEncoder.Base64(JSON.stringify({
                                        args: ObjectEncoder.Base64(JSON.stringify([
                                            this.getEnvironmentArgs().getProjectName(), this.getEnvironmentArgs().getReleaseName(),
                                            this.getEnvironmentArgs().getPlatform()
                                        ])),
                                        name: "Io.Oidis.Rest.Services.Utils.SelfupdatesManager.Download"
                                    })),
                                    id    : new Date().getTime(),
                                    origin: "http://localhost",
                                    type  : "LiveContentWrapper.InvokeMethod"
                                }),
                                headers: {
                                    "charset"     : "UTF-8",
                                    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                                    "user-agent"  : this.getRequest().getUserAgent()
                                },
                                method : HttpMethodType.POST,
                                url    : WebServiceClientFactory.getClientById(this.restService.getId()).getServerUrl()
                            })
                            .OnChange(($eventArgs : ProgressEventArgs) : void => {
                                callbacks.onChange($eventArgs);
                            })
                            .Then(($downloadPath : string) : void => {
                                const eventArgs : ProgressEventArgs = new ProgressEventArgs();
                                eventArgs.CurrentValue(-1);
                                callbacks.onChange(eventArgs);
                                this.fileSystem.getTempPath().Then(($tmp : string) : void => {
                                    const destinationPath : string = $tmp + "/" +
                                        this.getEnvironmentArgs().getProjectName() + "-" + new Date().getTime();
                                    this.fileSystem.Unpack($downloadPath, <any>{
                                        output: destinationPath
                                    }).Then(() : void => {
                                        this.fileSystem.Delete($downloadPath).Then(() : void => {
                                            this.fileSystem.Exists(destinationPath).Then(($status : boolean) : void => {
                                                if ($status) {
                                                    callbacks.selfinstallThen(true);
                                                    const windowManager : WindowHandlerConnector = new WindowHandlerConnector();
                                                    windowManager.Hide().Then(() : void => {
                                                        this.terminal
                                                            .Execute("\"" + this.getEnvironmentArgs().getAppName() + ".exe\"",
                                                                [], destinationPath)
                                                            .Then(($exitCode : number) : void => {
                                                                if ($exitCode === 0) {
                                                                    windowManager.Close();
                                                                }
                                                            });
                                                    });
                                                } else {
                                                    callbacks.selfinstallThen(false, "Unable to prepare selfinstall package.");
                                                }
                                            });
                                        });
                                    });
                                });
                            });
                    }
                });
            const promise : ISelfinstallPromise = {
                OnChange($callback : ($args : ProgressEventArgs) => void) : IInstallUnistallPromise {
                    callbacks.onChange = $callback;
                    return promise;
                },
                OnValidate($callback : ($success : boolean) => void) : ISelfinstallPromise {
                    callbacks.onValidate = $callback;
                    return promise;
                },
                Then($callback : ($success : boolean, $message? : string) => void) : void {
                    callbacks.selfinstallThen = $callback;
                }
            };
            return promise;
        }

        protected getLoaderIconClass() : any {
            return Icon;
        }

        protected getLoaderTextClass() : any {
            return Label;
        }

        protected getAppImageButtonClass() : any {
            return ImageButton;
        }

        protected getAppIconClass() : any {
            return Icon;
        }

        protected getAppTitleTextClass() : any {
            return Label;
        }

        protected getAppResizeBarClass() : any {
            return ResizeBar;
        }
    }

    export interface IInstallUnistallPromise {
        Then($callback : ($success : boolean, $message? : string) => void) : void;
    }

    export interface ISelfinstallPromise {
        OnValidate($callback : ($success : boolean) => void) : ISelfinstallPromise;

        OnChange($callback : ($args : ProgressEventArgs) => void) : IInstallUnistallPromise;

        Then($callback : ($success : boolean, $message? : string) => void) : void;
    }
}
