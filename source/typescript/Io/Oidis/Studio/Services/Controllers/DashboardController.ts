/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import EventType = Io.Oidis.Commons.Enums.Events.EventType;
    import LocalhostInfoPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.LocalhostInfoPanel;
    import ButtonType = Io.Oidis.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import DashboardPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard.DashboardPanelViewer;
    import DashboardPanelViewerArgs = Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.DashboardPanelViewerArgs;
    import DashboardPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.DashboardPanel;
    import ToolchainInfoPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ToolchainInfoPanel;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import Convert = Io.Oidis.Commons.Utils.Convert;
    import InstallationRecipeDAO = Io.Oidis.Services.DAO.InstallationRecipeDAO;
    import InstallationProtocolType = Io.Oidis.Services.Enums.InstallationProtocolType;
    import InstallationProgressEventArgs = Io.Oidis.Services.Events.Args.InstallationProgressEventArgs;
    import InstallationProgressCode = Io.Oidis.Services.Enums.InstallationProgressCode;
    import IInstallationPackage = Io.Oidis.Services.Interfaces.DAO.IInstallationPackage;
    import LanguageType = Io.Oidis.Commons.Enums.LanguageType;
    import ProjectsManagerPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ProjectsManagerPanel;
    import DropDownList = Io.Oidis.UserControls.BaseInterface.UserControls.DropDownList;
    import ProgressEventArgs = Io.Oidis.Commons.Events.Args.ProgressEventArgs;
    import WindowHandlerConnector = Io.Oidis.Services.Connectors.WindowHandlerConnector;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import IEventsHandler = Io.Oidis.Gui.Interfaces.IEventsHandler;
    import DashboardPanelLayerType = Io.Oidis.Studio.Gui.BaseInterface.Enums.Panels.DashboardPanelLayerType;
    import IFileSystemItemProtocol = Io.Oidis.Commons.Interfaces.IFileSystemItemProtocol;
    import FileSystemItemType = Io.Oidis.Commons.Enums.FileSystemItemType;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import IWindowHandlerNotifyIcon = Io.Oidis.Services.Connectors.IWindowHandlerNotifyIcon;
    import NotifyBalloonIconType = Io.Oidis.Services.Enums.NotifyBalloonIconType;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DashboardController extends BasePageController {

        constructor() {
            super();

            this.setPageTitle("WUI Dashboard v" + this.getEnvironmentArgs().getProjectVersion());
            this.setPageIconSource("resource/graphics/Io/Oidis/Studio/Gui/Dashboard.ico");
            this.setModelClassName(DashboardPanelViewer);
        }

        public getModel() : DashboardPanelViewer {
            return <DashboardPanelViewer>super.getModel();
        }

        public getModelArgs() : DashboardPanelViewerArgs {
            return (<any>DashboardPanelViewer).getTestViewerArgs();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                const model : DashboardPanelViewer = this.getModel();
                const args : DashboardPanelViewerArgs = this.getModelArgs();
                const instance : DashboardPanel = model.getInstance();
                const localhostInfo : LocalhostInfoPanel = instance.localhostInfo;
                const toolchainInfo : ToolchainInfoPanel = instance.toolchainInfo;
                const projectsManager : ProjectsManagerPanel = instance.projects;
                let projectsRoot : string = "D:/__PROJECTS__/WUI/Source";
                let builderPath : string = "";
                let serverPath : string = "";
                let localhostPath : string = "";
                let msys2Path : string = "";
                let serviceState : boolean = false;
                /// todo: validate installation by InstallDAO if ok validate repository state

                if (this.getRequest().IsWuiJre()) {
                    this.selfinstall()
                        .OnValidate(($status : boolean) : void => {
                            if ($status) {
                                /// todo: handle selfupdate
                            }
                        })
                        .OnChange(($eventArgs : ProgressEventArgs) : void => {
                            if ($eventArgs.CurrentValue() !== -1) {
                                Echo.Printf("progress: {0}%", "" +
                                    Convert.ToFixed(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue(), 0));
                            } else {
                                Echo.Printf("progress: unpredictable");
                            }
                        })
                        .Then(($status : boolean) : void => {
                            if ($status) {
                                Echo.Printf("progress: 100%");
                            }
                        });

                    let notifyExists : boolean = false;
                    const windowManager : WindowHandlerConnector = new WindowHandlerConnector();
                    this.minimizeButton.getEvents().setOnClick(() : void => {
                        windowManager.Hide().Then(() : void => {
                            if (!notifyExists) {
                                const iconPath : string = "D:\\__PROJECTS__\\WUI\\Source\\com-wui-framework-studio\\" +
                                    "resource\\graphics\\Com\\Wui\\Framework\\Studio\\Gui\\Dashboard.ico";
                                windowManager.CreateNotifyIcon(<IWindowHandlerNotifyIcon>{
                                    balloon    : {
                                        message : "Dashboard has been hidden to notification area.",
                                        title   : "WUI Dashboard",
                                        type    : NotifyBalloonIconType.USER,
                                        userIcon: iconPath
                                    },
                                    contextMenu: {
                                        items: [
                                            {
                                                label   : "Open",
                                                name    : "open",
                                                position: 0
                                            },
                                            {
                                                label   : "Exit",
                                                name    : "exit",
                                                position: 1
                                            }
                                        ]
                                    },
                                    icon       : iconPath,
                                    tip        : "WUI Dashboard"
                                });
                                notifyExists = true;
                            }
                        });
                    });

                    windowManager.getEvents().OnNotifyIconClick(() : void => {
                        windowManager.Show();
                    });

                    windowManager.getEvents().OnNotifyIconContextItemSelected(($request : string) : void => {
                        if ($request === "exit") {
                            windowManager.Close();
                        } else if ($request === "open") {
                            windowManager.Show();
                        }
                    });
                }

                const getWuiProjects : any = () : void => {
                    this.fileSystem.Exists(projectsRoot).Then(($status : boolean) : void => {
                        if (!$status) {
                            projectsManager.localSources.Visible(false);
                            model.setLayer(DashboardPanelLayerType.PROJECTS);
                        } else {
                            const projectsList : string[] = [];
                            this.fileSystem.getDirectoryContent(projectsRoot)
                                .Then(($map : IFileSystemItemProtocol[]) : void => {
                                    const nextItem : any = ($index : number) : void => {
                                        if ($index < $map.length) {
                                            const item : IFileSystemItemProtocol = $map[$index];
                                            if (item.type === FileSystemItemType.DIRECTORY) {
                                                this.fileSystem.Exists(projectsRoot + "/" + item.name + "/package.conf.json")
                                                    .Then(($status : boolean) : void => {
                                                        if ($status) {
                                                            projectsList.push(item.name);
                                                        }
                                                        nextItem($index + 1);
                                                    });
                                            } else {
                                                nextItem($index + 1);
                                            }
                                        } else {
                                            if (projectsList.length === 0) {
                                                projectsManager.localSources.Visible(false);
                                                model.setLayer(DashboardPanelLayerType.PROJECTS);
                                            } else {
                                                projectsManager.localSources.Clear();
                                                projectsList.forEach(($project : string) : void => {
                                                    (<DropDownListFormArgs>projectsManager.localSources.Configuration())
                                                        .AddItem($project, projectsRoot + "/" + $project);
                                                });
                                                projectsManager.localSources.Visible(true);
                                            }
                                        }
                                    };
                                    nextItem(0);
                                });
                        }
                    });
                };

                this.terminal.Spawn("wui", ["--path"]).Then(($exitCode : number, $stdout : string) : void => {
                    if (ObjectValidator.IsInteger($exitCode)) {
                        if ($exitCode === 0) {
                            builderPath = StringUtils.Remove($stdout, "\\r\\n", "\\n", "\r\n", "\n", " ");
                            this.fileSystem.Exists(builderPath + "/external_modules/apache").Then(($status : boolean) : void => {
                                instance.localhostInfoButton.Visible($status);
                                getWuiProjects();
                            });
                        } else {
                            instance.localhostInfoButton.Visible(false);
                            model.setLayer(DashboardPanelLayerType.PROJECTS);
                        }
                    }
                });

                const getLocalhostInfo : IEventsHandler = () : void => {
                    this.terminal.Spawn("sc", ["query", "WUI_Framework_Apache2.4"])
                        .Then(($exitCode : number, $stdout : string) : void => {
                            serviceState = false;
                            localhostInfo.localhostSwitch.Enabled(false);
                            if ($exitCode === 0) {
                                if (StringUtils.ContainsIgnoreCase($stdout, "RUNNING")) {
                                    localhostInfo.localhostSwitch.GuiType(ButtonType.RED);
                                    localhostInfo.localhostSwitch.Text("Stop WUI Localhost");
                                    localhostInfo.localhostSwitch.Enabled(true);
                                    serviceState = true;

                                    this.terminal.Spawn("reg", [
                                        "query", "HKLM\\System\\CurrentControlSet\\Services\\WUI_Framework_Apache2.4", "/v", "ImagePath"
                                    ]).Then(($exitCode : number, $stdout : string) : void => {
                                        if ($exitCode === 0) {
                                            const params : string[] = StringUtils.Split(StringUtils.Replace($stdout, "  ", " "), " ");
                                            let binBase : string = StringUtils.Replace(
                                                StringUtils.Remove(params[6], "\\\"", "\""), "\\", "/");
                                            binBase = StringUtils.Substring(binBase,
                                                0, StringUtils.IndexOf(binBase, "/external_modules/"));
                                            serverPath = binBase + "/external_modules";
                                            localhostPath = binBase + "/wui_modules/com-wui-framework-localhost";
                                            localhostInfo.serverRoot.Enabled(true);
                                            localhostInfo.serverLogs.Enabled(true);
                                            localhostInfo.configFiles.Enabled(true);
                                            localhostInfo.serverRoot.Value(localhostPath + "/www");
                                            localhostInfo.serverLogs.Value(binBase + "/log");
                                        }
                                    });
                                } else if (StringUtils.ContainsIgnoreCase($stdout, "STOP_PENDING", "START_PENDING")) {
                                    localhostInfo.getEvents().FireAsynchronousMethod(getLocalhostInfo, 250);
                                } else {
                                    localhostInfo.localhostSwitch.GuiType(ButtonType.GREEN);
                                    localhostInfo.localhostSwitch.Text("Start WUI Localhost");
                                    localhostInfo.localhostSwitch.Enabled(true);
                                }
                            }
                            localhostInfo.location.Enabled(serviceState);
                        });
                };

                instance.localhostInfoButton.getEvents().setOnClick(getLocalhostInfo);

                localhostInfo.localhostSwitch.getEvents().setOnClick(() : void => {
                    this.terminal.Spawn("sc", [serviceState ? "stop" : "start", "WUI_Framework_Apache2.4"])
                        .Then(($exitCode : number) : void => {
                            if ($exitCode === 0) {
                                getLocalhostInfo();
                            }
                        });
                });

                localhostInfo.certsUpdate.getEvents().setOnClick(() : void => {
                    localhostInfo.certsUpdate.Enabled(false);
                    localhostInfo.localhostSwitch.Enabled(false);
                    /// todo: certs update requires correct base project or task refactoring
                    this.terminal.Spawn("wui", ["grunt", "certs-update"], <string>localhostInfo.serverRoot.Value())
                        .Then(($exitCode : number) : void => {
                            if ($exitCode === 0) {
                                localhostInfo.certsUpdate.Enabled(true);
                                getLocalhostInfo();
                            }
                        });
                });

                (<DropDownList>(<any>localhostInfo.location).getInputElement()).getEvents().setOnSelect(() : void => {
                    this.getHttpManager().ReloadTo(<string>localhostInfo.location.Value(), true);
                });
                localhostInfo.serverRoot.getEvents().setOnDoubleClick(() : void => {
                    this.terminal.Execute("start", [StringUtils.Replace(<string>localhostInfo.serverRoot.Value(), "/", "\\")]);
                });
                localhostInfo.serverLogs.getEvents().setOnDoubleClick(() : void => {
                    this.terminal.Execute("start", [StringUtils.Replace(<string>localhostInfo.serverLogs.Value(), "/", "\\")]);
                });
                (<DropDownList>(<any>localhostInfo.configFiles).getInputElement()).getEvents().setOnSelect(() : void => {
                    switch (StringUtils.ToInteger(<string>localhostInfo.configFiles.Value())) {
                    case 1:
                        this.terminal.Open(serverPath + "/apache/Apache24/conf/httpd.conf");
                        break;
                    case 2:
                        this.terminal.Open(serverPath + "/php/php.ini");
                        break;
                    case 3:
                        this.terminal.Open(serverPath + "/sendmail/sendmail.ini");
                        break;
                    default:
                        break;
                    }
                });

                const formatVersion : ($appName : string, $appVersion : string) => string =
                    ($appName : string, $appVersion : string) : string => {
                        const EOL : string = StringUtils.NewLine();
                        return "<hr><b>" + $appName + " version:</b>" + EOL + EOL + $appVersion;
                    };

                let progressText : string = "Collecting information, please wait ...";
                const spawnChain : ISpawnChain[] = [];
                const getVersion : ($appName : string, $arg : string, $done : ($value : string) => void) => IGetVersionPromise =
                    ($appName : string, $arg : string, $done : ($value : string) => void) : IGetVersionPromise => {
                        spawnChain.push({appName: $appName, args: [$arg], cwd: null, done: $done});

                        const promise : any = {
                            Then: ($callback : () => void) : void => {
                                const nextSpawn : ($index : number) => void = ($index : number) : void => {
                                    if ($index < spawnChain.length) {
                                        const data : ISpawnChain = spawnChain[$index];
                                        if (StringUtils.Contains(data.cwd, "{serverPath}", "{msys2}")) {
                                            data.cwd = StringUtils.Replace(data.cwd, "{serverPath}", serverPath);
                                            data.cwd = StringUtils.Replace(data.cwd, "{msys2}", msys2Path);
                                        }
                                        this.terminal.Spawn(data.appName, data.args, data.cwd)
                                            .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                                if (ObjectValidator.IsInteger($exitCode)) {
                                                    if ($exitCode === 0) {
                                                        data.done($stdout + $stderr);
                                                    }
                                                    nextSpawn($index + 1);
                                                }
                                            });
                                    } else {
                                        $callback();
                                    }
                                };
                                nextSpawn(0);
                            },
                            getVersion($appName : string, $arg : string, $done : () => void) : IGetVersionPromise {
                                spawnChain.push({appName: $appName, args: [$arg], cwd: null, done: $done});
                                return promise;
                            },
                            readCmd($appName : string, $args : string[], $cwd : string,
                                    $done : () => void) : IGetVersionPromise {
                                spawnChain.push({appName: $appName, args: $args, cwd: $cwd, done: $done});
                                return promise;
                            }
                        };
                        return promise;
                    };
                const getToolchainInfo : IEventsHandler = () : void => {
                    let progressTick : any;
                    instance.getEvents().FireAsynchronousMethod(() : void => {
                        toolchainInfo.setContent(progressText);
                        progressTick = setInterval(() : void => {
                            progressText += ".";
                            toolchainInfo.setContent(progressText);
                        }, 250);
                    }, 250);

                    /// todo: implement validation and collecting method directly in wui builder
                    let info : string = "";
                    getVersion("wui", "--version", ($value : string) : void => {
                        info += $value;
                    })
                        .getVersion("wui", "--path", ($value : string) : void => {
                            info += " at " + $value + StringUtils.NewLine() + StringUtils.NewLine();
                        })
                        .getVersion("php", "--version", ($value : string) : void => {
                            info += formatVersion("PHP", $value);
                        })
                        .getVersion("phpunit", "--version", ($value : string) : void => {
                            info += formatVersion("PHPUnit", $value);
                        })
                        .getVersion("phpdoc", "--version", ($value : string) : void => {
                            info += formatVersion("PHPDocumentor", $value);
                        })
                        .readCmd("reg", [
                            "query", "HKLM\\System\\CurrentControlSet\\Services\\WUI_Framework_Apache2.4", "/v", "ImagePath"
                        ], null, ($value : string) : void => {
                            const params : string[] = StringUtils.Split(StringUtils.Replace($value, "  ", " "), " ");
                            serverPath = StringUtils.Replace(StringUtils.Remove(params[6], "\""), "\\", "/");
                            serverPath = StringUtils.Substring(serverPath, 0, StringUtils.IndexOf(serverPath, "/apache/"));
                        })
                        .readCmd("httpd.exe", ["-v"], "{serverPath}/apache/Apache24/bin", ($value : string) : void => {
                            info += formatVersion("Apache", $value);
                        })
                        .getVersion("node", "-v", ($value : string) : void => {
                            info += formatVersion("Node.js", $value);
                        })
                        .getVersion("git", "--version", ($value : string) : void => {
                            info += formatVersion("GIT", $value);
                        })
                        .getVersion("java", "-version", ($value : string) : void => {
                            info += formatVersion("JAVA", $value);
                        })
                        .getVersion("python", "--version", ($value : string) : void => {
                            info += formatVersion("Python", $value);
                        })
                        .getVersion("cmake", "--version", ($value : string) : void => {
                            info += formatVersion("Cmake", $value);
                        })
                        .readCmd("where", ["msys2_shell"], null, ($value : string) : void => {
                            msys2Path = StringUtils.Replace(StringUtils.Remove((<any>$value).trim(), "\\msys2_shell.cmd"), "\\", "/");
                        })
                        .readCmd("sh.exe", ["-c", "\"gcc --version\""], "{msys2}/usr/bin", ($value : string) : void => {
                            info += formatVersion("MinGW-gcc", $value);
                        })
                        // .readCmd("reg", [
                        //     "query", "\"HKLM\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\"", "/v", "Release"
                        // ], null,  ($value : string) : void => {
                        //     let releaseKey : number = Convert.StringToHexadecimal($value);
                        //     let version : string = "No 4.5 or later version detected";
                        //     if (releaseKey >= 378389) {
                        //         version = "4.5 or later";
                        //     }
                        //     if (releaseKey >= 378675) {
                        //         version = "4.5.1 or later";
                        //     }
                        //     if (releaseKey >= 379893) {
                        //         version = "4.5.2 or later";
                        //     }
                        //     if (releaseKey >= 393273) {
                        //         version = "4.6 RC or later";
                        //     }
                        //     info += formatVersion(".NET", version);
                        // })
                        .getVersion("cppcheck", "--version", ($value : string) : void => {
                            info += formatVersion("Cppcheck", $value);
                        })
                        .getVersion("doxygen", "--version", ($value : string) : void => {
                            info += formatVersion("Doxygen", $value);
                        })
                        .Then(() : void => {
                            clearInterval(progressTick);
                            toolchainInfo.setContent(info);
                            toolchainInfo.checkButton.Enabled(true);
                        });
                };
                instance.toolchainInfoButton.getEvents().setOnClick(getToolchainInfo);

                const installDao : InstallationRecipeDAO = new InstallationRecipeDAO();
                installDao.setConfigurationPath("resource/data/Io/Oidis/Studio/Services/Configuration/WuiFrameworkWindows.jsonp");
                let installInfo : string = "";
                let installationStatus : boolean = true;
                installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
                    if ($eventArgs.ProgressCode() === InstallationProgressCode.VALIDATED) {
                        const info : IInstallationPackage = installDao.getPackage(installDao.getChain()[$eventArgs.CurrentValue() - 1]);
                        if (!ObjectValidator.IsEmptyOrNull(info)) {
                            if (!$eventArgs.Status()) {
                                installInfo += ObjectValidator.IsEmptyOrNull(info.description) ? info.name : info.description;
                                if (!ObjectValidator.IsEmptyOrNull(info.version)) {
                                    installInfo += " -> " + info.version;
                                }
                                installInfo += StringUtils.NewLine();
                                installationStatus = false;
                            }
                            if ($eventArgs.CurrentValue() === $eventArgs.RangeEnd()) {
                                toolchainInfo.checkButton.Enabled(true);
                                if (installationStatus) {
                                    toolchainInfo.checkButton.Text("Toolchain is up to date");
                                    toolchainInfo.checkButton.GuiType(ButtonType.GREEN);
                                } else {
                                    toolchainInfo.checkButton.Text("Update is required!");
                                    toolchainInfo.checkButton.GuiType(ButtonType.RED);
                                    toolchainInfo.setContent(
                                        "Packages, which requires update:" + StringUtils.NewLine() + StringUtils.NewLine() +
                                        installInfo);
                                }
                            }
                        }
                    }
                });
                toolchainInfo.checkButton.getEvents().setOnClick(() : void => {
                    installInfo = "";
                    installationStatus = true;
                    toolchainInfo.checkButton.Enabled(false);
                    toolchainInfo.setContent("Processing validation ...");
                    installDao.Load(LanguageType.EN, () : void => {
                        installDao.RunInstallationChain(InstallationProtocolType.VALIDATE);
                    });
                });

                if (!(<DropDownListFormArgs>projectsManager.workplacePath.Configuration()).getValues().Contains(projectsRoot)) {
                    (<DropDownListFormArgs>projectsManager.workplacePath.Configuration()).AddItem(projectsRoot);
                }
                projectsManager.workplacePath.Value(projectsRoot);

                (<DropDownList>(<any>projectsManager.workplacePath).getInputElement()).getEvents().setOnSelect(() : void => {
                    projectsRoot = <string>projectsManager.workplacePath.Value();
                    getWuiProjects();
                });

                (<DropDownList>(<any>projectsManager.remoteSources).getInputElement()).getEvents().setOnSelect(() : void => {
                    projectsManager.installProjectButton.Enabled(true);
                });

                projectsManager.installProjectButton.getEvents().setOnClick(() : void => {
                    projectsManager.installProjectButton.Enabled(false);
                    this.fileSystem.CreateDirectory(<string>projectsManager.workplacePath.Value()).Then(() : void => {
                        projectsManager.ConsoleVisible(true);
                        this.terminal.Spawn("git", [
                            "clone", "--recursive", <string>projectsManager.remoteSources.Value()
                        ], <string>projectsManager.workplacePath.Value())
                            .OnMessage(($data : string) : void => {
                                projectsManager.PrintToConsole($data);
                            })
                            .Then(($exitCode : number) : void => {
                                if (ObjectValidator.IsInteger($exitCode) && $exitCode === 0) {
                                    let projectPath : string = <string>projectsManager.remoteSources.Value();
                                    projectPath = StringUtils.Substring(projectPath,
                                        StringUtils.IndexOf(projectPath, "/", false) + 1);
                                    projectPath = <string>projectsManager.workplacePath.Value() + "/" +
                                        StringUtils.Remove(projectPath, ".git");
                                    projectPath = StringUtils.Replace(projectPath, "/", "\\");

                                    this.terminal.Spawn("wui", ["grunt", "dev-skip-test"], projectPath)
                                        .OnMessage(($data : string) : void => {
                                            projectsManager.PrintToConsole($data);
                                        })
                                        .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                            if (ObjectValidator.IsInteger($exitCode)) {
                                                if ($exitCode === 0) {
                                                    projectsManager.ConsoleVisible(false);
                                                    projectsManager.installProjectButton.Enabled(true);
                                                    this.terminal.Execute("start", [
                                                        StringUtils.Replace(projectPath + "/build/target", "/", "\\")
                                                    ]);
                                                } else {
                                                    projectsManager.PrintToConsole($stdout + $stderr);
                                                }
                                            }
                                        });
                                }
                            });
                    });
                });
                (<DropDownList>(<any>projectsManager.localSources).getInputElement()).getEvents().setOnSelect(() : void => {
                    this.terminal.Execute("start", [StringUtils.Replace(<string>projectsManager.localSources.Value(), "/", "\\")]);
                });
            });

            super.resolver();
        }
    }

    export interface IGetVersionPromise {
        getVersion($appName : string, $arg : string, $done : ($value : string) => void) : IGetVersionPromise;

        readCmd($appName : string, $args : string[], $cwd : string, $done : ($value : string) => void) : IGetVersionPromise;

        Then($callback : () => void) : void;
    }

    export abstract class ISpawnChain {
        public appName : string;
        public args : string[];
        public cwd : string;
        public done : ($value : string) => void;
    }
}
