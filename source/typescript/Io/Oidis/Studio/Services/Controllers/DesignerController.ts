/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import DesignerPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DesignerPanelViewer;
    import DesignerPanelViewerArgs = Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DesignerPanelViewerArgs;
    import DesignerPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Designer.DesignerPanel;
    import BasePageDAO = Io.Oidis.Services.DAO.BasePageDAO;

    export class DesignerController extends Io.Oidis.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();

            this.setPageTitle("WUI Designer v" + this.getEnvironmentArgs().getProjectVersion());
            this.setModelClassName(DesignerPanelViewer);
        }

        public getModel() : DesignerPanelViewer {
            return <DesignerPanelViewer>super.getModel();
        }

        public getModelArgs() : DesignerPanelViewerArgs {
            return (<any>DesignerPanelViewer).getTestViewerArgs();
        }

        protected afterLoad($instance? : DesignerPanel, $args? : DesignerPanelViewerArgs, $dao? : BasePageDAO) : void {
            /// handle load
        }
    }
}
