/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ImageButtonType = Io.Oidis.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import Icon = Io.Oidis.UserControls.BaseInterface.UserControls.Icon;
    import Label = Io.Oidis.UserControls.BaseInterface.UserControls.Label;
    import ImageButton = Io.Oidis.UserControls.BaseInterface.UserControls.ImageButton;
    import ResizeBar = Io.Oidis.UserControls.BaseInterface.Components.ResizeBar;
    import EventType = Io.Oidis.Gui.Enums.Events.EventType;
    import ReportServiceConnector = Io.Oidis.Services.Connectors.ReportServiceConnector;
    import IReportProtocol = Io.Oidis.Services.Connectors.IReportProtocol;
    import FileSystemHandlerConnector = Io.Oidis.Services.Connectors.FileSystemHandlerConnector;
    import IEventsHandler = Io.Oidis.Commons.Interfaces.IEventsHandler;
    import ErrorEventArgs = Io.Oidis.Commons.Events.Args.ErrorEventArgs;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class ErrorPageController extends Io.Oidis.Services.HttpProcessor.Resolvers.ExceptionPageController {
        private manualReportEnabled : boolean;

        constructor() {
            super();

            this.setPageIconSource("resource/graphics/GpuSdk.ico");
            this.appIcon.Visible(false);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.GENERAL);
            this.maximizeButton.Visible(false);
            this.closeButton.GuiType(ImageButtonType.GENERAL);
            this.manualReportEnabled = false;
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                const connector : ReportServiceConnector = new ReportServiceConnector();
                const data : IReportProtocol = {
                    appId      : StringUtils.getSha1(
                        this.getEnvironmentArgs().getAppName() + this.getEnvironmentArgs().getProjectVersion()),
                    appName    : this.getEnvironmentArgs().getAppName(),
                    appVersion : this.getEnvironmentArgs().getProjectVersion(),
                    log        : Loader.getInstance().ApplicationLog(),
                    printScreen: "",
                    timeStamp  : new Date().getTime(),
                    trace      : this.getTrace()
                };

                connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    try {
                        LogIt.Error($eventArgs.ToString("", false));
                    } catch (ex) {
                        LogIt.Debug("Unable to consume Report service connector error.", ex);
                    }
                });
                connector.getEvents().OnTimeout(() : void => {
                    LogIt.Error("Report service connector is unreachable.");
                });

                const sendReport : any = () : void => {
                    connector
                        .CreateReport(data)
                        .Then(($success : boolean) : void => {
                            if ($success) {
                                Loader.getInstance().ApplicationLog("");
                            }
                        });
                };

                const fileSystem : FileSystemHandlerConnector = new FileSystemHandlerConnector();
                const errorEventHandler : IEventsHandler = () : void => {
                    sendReport();
                };
                fileSystem.getEvents().OnError(errorEventHandler);
                fileSystem.getEvents().OnTimeout(errorEventHandler);

                const today : Date = new Date();
                const year : string = today.getFullYear() + "";
                const monthNumber : number = today.getMonth() + 1;
                const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
                const day : string = today.getDate() < 10 ? "0" + today.getDate() : today.getDate() + "";
                const logFile : string = "log/" + year + "/" + month + "/" + day + "_" + month + "_" + year + ".txt";
                fileSystem.getWuiHostLocation().Then(($path : string) : void => {
                    fileSystem.Read($path + "/" + logFile).Then(($data : string) : void => {
                        const currentLength : number = StringUtils.Length($data);
                        if (currentLength > 0) {
                            const maxLength : number = 1024 * 10;
                            if (currentLength > maxLength) {
                                $data = StringUtils.Substring($data, currentLength - maxLength);
                            }
                            data.log += "\r\n-------------------- WUI Connector --------------------\r\n\r\n" + $data;
                        }
                        sendReport();
                    });
                });
            });
            super.resolver();
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return Label;
        }

        /**
         * @return {ImageButton} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.UserControls.ImageButton
         */
        protected getAppImageButtonClass() : any {
            return ImageButton;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
         */
        protected getAppIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
         */
        protected getAppTitleTextClass() : any {
            return Label;
        }

        /**
         * @return {IResizeBar} This method should return class with
         * interface Io.Oidis.Gui.Interfaces.Components.IResizeBar
         */
        protected getAppResizeBarClass() : any {
            return ResizeBar;
        }
    }
}
