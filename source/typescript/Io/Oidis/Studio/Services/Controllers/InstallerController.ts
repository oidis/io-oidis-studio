/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import EventType = Io.Oidis.Commons.Enums.Events.EventType;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import Convert = Io.Oidis.Commons.Utils.Convert;
    import InstallationRecipeDAO = Io.Oidis.Services.DAO.InstallationRecipeDAO;
    import InstallationProgressEventArgs = Io.Oidis.Services.Events.Args.InstallationProgressEventArgs;
    import IInstallationPackage = Io.Oidis.Services.Interfaces.DAO.IInstallationPackage;
    import ProgressEventArgs = Io.Oidis.Commons.Events.Args.ProgressEventArgs;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import InstallerInitialScreenPanelViewer =
        Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Installer.InstallerInitialScreenPanelViewer;
    import InstallerInitialScreenPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Installer.InstallerInitialScreenPanelViewerArgs;
    import InstallerInitialScreenPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Installer.InstallerInitialScreenPanel;
    import InstallationProtocolType = Io.Oidis.Services.Enums.InstallationProtocolType;
    import LanguageType = Io.Oidis.Commons.Enums.LanguageType;
    import IInstallationRecipeWuiPaths = Io.Oidis.Studio.Services.Interfaces.DAO.IInstallationRecipeWuiPaths;
    import IInstallationRecipe = Io.Oidis.Studio.Services.Interfaces.DAO.IInstallationRecipe;

    export class InstallerController extends BasePageController {

        constructor() {
            super();

            this.setPageTitle("WUI Installer v" + this.getEnvironmentArgs().getProjectVersion());
            this.setPageIconSource("resource/graphics/Io/Oidis/Studio/Gui/Installer.ico");
            this.setModelClassName(InstallerInitialScreenPanelViewer);
        }

        public getModel() : InstallerInitialScreenPanelViewer {
            return <InstallerInitialScreenPanelViewer>super.getModel();
        }

        public getModelArgs() : InstallerInitialScreenPanelViewerArgs {
            return (<any>InstallerInitialScreenPanelViewer).getTestViewerArgs();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                const model : InstallerInitialScreenPanelViewer = this.getModel();
                const args : InstallerInitialScreenPanelViewerArgs = this.getModelArgs();
                const instance : InstallerInitialScreenPanel = model.getInstance();

                if (this.getRequest().IsWuiJre()) {
                    this.selfinstall()
                        .OnValidate(($status : boolean) : void => {
                            if (!$status) {
                                instance.installButton.Enabled(true);
                            }
                        })
                        .OnChange(($eventArgs : ProgressEventArgs) : void => {
                            if ($eventArgs.CurrentValue() !== -1) {
                                instance.progressBar.RangeEnd(100);
                                instance.progressBar.Value(Convert.ToFixed(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue(), 0));
                            }
                        })
                        .Then(($status : boolean) : void => {
                            if ($status) {
                                instance.progressBar.Value(100);
                            }
                        });
                } else {
                    instance.installButton.Enabled(true);
                }

                const installDao : InstallationRecipeDAO = new InstallationRecipeDAO();
                installDao.setConfigurationPath("resource/data/Io/Oidis/Studio/Services/Configuration/WuiFrameworkWindows.jsonp");
                installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
                    instance.progressBar.RangeEnd($eventArgs.RangeEnd());
                    const info : IInstallationPackage = installDao.getPackage(installDao.getChain()[$eventArgs.CurrentValue() - 1]);
                    if (!ObjectValidator.IsEmptyOrNull(info)) {
                        let status : string = ObjectValidator.IsEmptyOrNull(info.description) ? info.name : info.description;
                        if (!ObjectValidator.IsEmptyOrNull(info.version)) {
                            status += " (" + info.version + ")";
                        }
                        instance.statusLabel.Text(status);
                        instance.progressBar.Value($eventArgs.CurrentValue());
                    }
                    if ($eventArgs.CurrentValue() === $eventArgs.RangeEnd()) {
                        instance.installButton.Text("Install");
                    }
                });

                instance.installPath.Value("C:/WUIFramework");
                instance.workplacePath.Value("C:/WUIFramework/Projects");
                instance.statusLabel.Text("");
                instance.installButton.getEvents().setOnClick(() : void => {
                    instance.installButton.Text("Cancel");
                    installDao.Load(LanguageType.EN, () : void => {
                        const wuiPaths : IInstallationRecipeWuiPaths =
                            (<IInstallationRecipe>this.getDao().getStaticConfiguration()).wuiPaths;
                        wuiPaths.root = <string>instance.installPath.Value();
                        wuiPaths.workplace = <string>instance.workplacePath.Value();
                        installDao.RunInstallationChain(InstallationProtocolType.INSTALL);
                    });
                });
            });

            super.resolver();
        }
    }
}
