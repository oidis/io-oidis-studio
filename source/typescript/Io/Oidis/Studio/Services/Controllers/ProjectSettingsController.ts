/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import BasePageDAO = Io.Oidis.Services.DAO.BasePageDAO;
    import ProjectSettingsPanelViewer = Io.Oidis.Studio.Gui.BaseInterface.Viewers.Panels.Dashboard.ProjectSettingsPanelViewer;
    import ProjectSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectSettingsPanelViewerArgs;
    import ProjectSettingsPanel = Io.Oidis.Studio.Gui.BaseInterface.Panels.Dashboard.ProjectSettingsPanel;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import ProjectSettingsDAO = Io.Oidis.Studio.Services.DAO.Pages.ProjectSettingsDAO;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;

    export class ProjectSettingsController extends Io.Oidis.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();

            this.setPageTitle("WUI Dashboard");
            this.setModelClassName(ProjectSettingsPanelViewer);
            this.setDao(new ProjectSettingsDAO());
        }

        public getModel() : ProjectSettingsPanelViewer {
            return <ProjectSettingsPanelViewer>super.getModel();
        }

        protected getDao() : ProjectSettingsDAO {
            return <ProjectSettingsDAO>super.getDao();
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($GET.KeyExists("port")) {
                this.getDao().Port(StringUtils.ToInteger($GET.getItem("port")));
            }
            super.argsHandler($GET, $POST);
        }

        protected afterLoad($instance? : ProjectSettingsPanel, $args? : ProjectSettingsPanelViewerArgs, $dao? : BasePageDAO) : void {
            $instance.save.getEvents().setOnClick(() : void => {
                this.getDao().getModelArgs().Items($instance.Value().Items());
                this.getDao().Save(($status : boolean) : void => {
                    $instance.save.IsSelected(true);
                    if ($status) {
                        $instance.save.Text("Saved successfully");
                    } else {
                        $instance.save.Text("Failed to saved config");
                    }
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        $instance.save.Text($args.SaveText());
                        $instance.save.IsSelected(false);
                    }, 1000);
                });
            });
        }
    }
}
