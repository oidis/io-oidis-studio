/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.DAO.Pages {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import BasePageDAO = Io.Oidis.Services.DAO.BasePageDAO;
    import LanguageType = Io.Oidis.Commons.Enums.LanguageType;
    import DashboardConnector = Io.Oidis.Studio.Services.Connectors.DashboardConnector;
    import Property = Io.Oidis.Commons.Utils.Property;
    import ProjectSettingsPanelViewerArgs =
        Io.Oidis.Studio.Gui.BaseInterface.ViewersArgs.Panels.Dashboard.ProjectSettingsPanelViewerArgs;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import IProject = Io.Oidis.Commons.Interfaces.IProject;
    import TextFieldFormArgs = Io.Oidis.UserControls.Structures.TextFieldFormArgs;
    import BaseFormInputArgs = Io.Oidis.UserControls.Structures.BaseFormInputArgs;
    import CheckBoxFormArgs = Io.Oidis.UserControls.Structures.CheckBoxFormArgs;
    import DropDownListFormArgs = Io.Oidis.UserControls.Structures.DropDownListFormArgs;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import BaseObject = Io.Oidis.Commons.Primitives.BaseObject;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import JsonUtils = Io.Oidis.Commons.Utils.JsonUtils;
    import IProjectSettingsLocalization = Io.Oidis.Studio.Services.Interfaces.DAO.IProjectSettingsLocalization;

    export class ProjectSettingsDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/Oidis/Studio/Services/Localization/ProjectSettingsLocalization.jsonp";
        private port : number;
        private connector : DashboardConnector;
        private model : SettingNode;

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IProjectSettingLocalization":
                return ProjectSettingsDAO;
            default:
                break;
            }
            return BasePageDAO;
        }

        constructor() {
            super();
            this.port = 3667;
        }

        public Port($value? : number) : number {
            return this.port = Property.PositiveInteger(this.port, $value);
        }

        public getPageConfiguration() : IProjectSettingsLocalization {
            return <IProjectSettingsLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : ProjectSettingsPanelViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const args : ProjectSettingsPanelViewerArgs = new ProjectSettingsPanelViewerArgs();
                const config : IProjectSettingsLocalization = this.getPageConfiguration();
                args.SaveText(config.save);

                this.modelArgs = args;
            }
            return <ProjectSettingsPanelViewerArgs>this.modelArgs;
        }

        public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
            super.Load($language, () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.connector)) {
                    this.connector = new DashboardConnector(this.port);
                    this.connector.getSchema().Then(($schema : any) : void => {
                        this.connector.getProjectConfig().Then(($config : IProject) : void => {
                            this.model = this.parseProperties($schema.properties, $config, "");
                            this.getModelArgs().Items(this.model.ToFormArgs());
                            $asyncHandler();
                        });
                    });
                } else {
                    $asyncHandler();
                }
            }, $force);
        }

        public Save($callback : ($status : boolean) => void) : any {
            return this.connector.SaveProjectConfig(this.model.ToJson()).Then($callback);
        }

        protected getResourcesHandler() : any {
            return Resources;
        }

        private parseProperties($schema : any, $config : IProject, $parentName : string) : SettingNode {
            const children : any = {};
            const node : SettingNode = new SettingNode();
            node.Schema($schema);
            node.Value(children);
            let name : string;
            for (name in $schema) {
                if ($schema.hasOwnProperty(name)) {
                    children[name] = this.parseSchema($schema[name], $config, name, $parentName);
                }
            }
            return node;
        }

        private parseSchema($schema : any, $config : any, $name : string, $parentName : string) : SettingNode {
            let node : SettingNode;
            const config : any = !ObjectValidator.IsEmptyOrNull($config) && $config.hasOwnProperty($name) ? $config[$name] : null;
            if (ObjectValidator.IsSet($schema.oneOf) ||
                ObjectValidator.IsSet($schema.anyOf)) {
                let index : number = 0;
                if (ObjectValidator.IsSet($schema.default)) {
                    index = $schema.default;
                }
                ["oneOf", "anyOf"].forEach(($keyword : string) : void => {
                    if ($schema.hasOwnProperty($keyword)) {
                        node = this.parseSchema($schema[$keyword][index], $config, $name, $parentName);
                        node.Schema($schema[$keyword][index]);
                    }
                });
            } else if (ObjectValidator.IsSet($schema.allOf)) {
                $schema.allOf.forEach(($schema : any) : void => {
                    node = this.parseSchema($schema, $config, $name, $parentName);
                    node.Schema($schema);
                });
            } else if ($schema.type === "object") {
                node = this.parseObject($schema, $parentName + $name + ".", config);
            } else if ($schema.type === "array") {
                node = this.parseArray($schema, $parentName + $name, config);
            } else {
                node = this.parsePrimitive($schema, $parentName + $name, config);
            }
            return node;
        }

        private parseObject($schema : any, $name : string, $value : any) : SettingNode {
            let node : SettingNode;
            switch ($schema.format) {
            case "IProjectTarget":
            case "IProjectDependency":
                node = this.parseProperties($schema.properties, $value, $name);
                break;
            default:
                if (ObjectValidator.IsSet($schema.additionProperties)) {
                    const children : any = {};
                    node = new SettingNode();
                    node.Schema($schema);
                    node.Value(children);
                    let property : string;
                    for (property in $value) {
                        if ($value.hasOwnProperty(property)) {
                            const schema : any = {};
                            schema[property] = $schema.additionProperties;
                            children[property] = this.parseProperties(schema, $value, $name);
                        }
                    }
                } else {
                    node = this.parseProperties($schema.properties, $value, $name);
                }
                break;
            }
            return node;
        }

        private parseArray($schema : any, $name : string, $value : any) : SettingNode {
            const children : any[] = [];
            const node : SettingNode = new SettingNode();
            if (ObjectValidator.IsEmptyOrNull($value)) {
                $value = $schema.default;
            }
            if (ObjectValidator.IsEmptyOrNull($value)) {
                $value = [];
            }
            let index : number = 0;
            $value.forEach(($item : any) : void => {
                if ($schema.items.type === "object") {
                    children.push(this.parseObject($schema.items, $name + "[" + index + "].", $item));
                } else {
                    children.push(this.parsePrimitive($schema.items, $name + "[" + index + "]", $item));
                }
                index++;
            });
            node.Schema($schema);
            node.Value(children);
            return node;
        }

        private parsePrimitive($schema : any, $name : string, $value : any) : SettingNode {
            const node : SettingNode = new SettingNode();
            node.Schema($schema);
            node.Key($name);
            node.Value($value);
            return node;
        }
    }

    class SettingNode extends BaseObject {
        private key : string;
        private value : any;
        private schema : any;
        private formArgs : BaseFormInputArgs;

        public Schema($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.schema = $value;
            }
            return this.schema;
        }

        public Key($value? : string) : string {
            return this.key = Property.String(this.key, $value);
        }

        public Value($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.value = $value;
            }
            return this.value;
        }

        public ToFormArgs() : ArrayList<BaseFormInputArgs> {
            const output : ArrayList<BaseFormInputArgs> = new ArrayList<BaseFormInputArgs>();
            if (!ObjectValidator.IsEmptyOrNull(this.schema)) {
                let args : BaseFormInputArgs;
                switch (this.schema.type) {
                case "boolean":
                    args = new CheckBoxFormArgs();
                    break;
                case "integer":
                case "number":
                    args = new TextFieldFormArgs();
                    break;
                case "string":
                    args = new TextFieldFormArgs();
                    break;
                case "array":
                    this.processFormArray(output);
                    break;
                case "object":
                    this.processFormObject(output);
                    break;
                default:
                    if (ObjectValidator.IsObject(this.value)) {
                        this.processFormObject(output);
                    } else if (ObjectValidator.IsArray(this.value)) {
                        this.processFormArray(output);
                    } else if (!ObjectValidator.IsEmptyOrNull(this.schema.enum)) {
                        args = new DropDownListFormArgs();
                        this.schema.enum.forEach(($item : string) : void => {
                            (<DropDownListFormArgs>args).AddItem($item);
                        });
                    } else {
                        args = new TextFieldFormArgs();
                    }
                    break;
                }

                if (!ObjectValidator.IsEmptyOrNull(args)) {
                    args.ForceSetValue(true);
                    args.Name(this.key + ":");
                    if (!ObjectValidator.IsEmptyOrNull(this.value)) {
                        if (!ObjectValidator.IsString(this.value) && !ObjectValidator.IsBoolean(this.value)) {
                            args.Value(JSON.stringify(JsonUtils.Clone(this.value)));
                        } else {
                            args.Value(this.value);
                        }
                    } else if (this.schema.type === "boolean") {
                        args.Value(this.schema.default);
                    } else {
                        args.Hint(this.schema.default);
                    }
                    output.Add(args);
                    this.formArgs = args;
                }
            } else if (!ObjectValidator.IsEmptyOrNull(this.key)) {
                LogIt.Warning("Missing schema for: " + this.key);
            }
            return output;
        }

        public ToJson() : any {
            let output : any = {};
            if (!ObjectValidator.IsEmptyOrNull(this.schema)) {
                switch (this.schema.type) {
                case "boolean":
                case "integer":
                case "number":
                case "string":
                    output = this.processJsonPrimitive(this.schema.type);
                    break;
                case "array":
                    output = [];
                    this.processJsonArray(output);
                    break;
                case "object":
                    this.processJsonObject(output);
                    break;
                default:
                    if (ObjectValidator.IsObject(this.value)) {
                        this.processJsonObject(output);
                    } else if (ObjectValidator.IsArray(this.value)) {
                        output = [];
                        this.processJsonArray(output);
                    } else {
                        output = this.processJsonPrimitive(this.schema.type);
                    }
                    break;
                }
            } else if (!ObjectValidator.IsEmptyOrNull(this.key)) {
                LogIt.Warning("Missing schema for: " + this.key);
            }
            return output;
        }

        private processFormArray($output : ArrayList<BaseFormInputArgs>) : void {
            this.value.forEach(($value : SettingNode) : void => {
                $value.ToFormArgs().foreach(($arg : BaseFormInputArgs) : void => {
                    $output.Add($arg);
                });
            });
        }

        private processFormObject($output : ArrayList<BaseFormInputArgs>) : void {
            let property : string;
            for (property in this.value) {
                if (this.value.hasOwnProperty(property)) {
                    this.value[property].ToFormArgs().foreach(($arg : BaseFormInputArgs) : void => {
                        $output.Add($arg);
                    });
                }
            }
        }

        private processJsonArray($output : any[]) : void {
            this.value.forEach(($value : SettingNode) : void => {
                $output.push($value.ToJson());
            });
        }

        private processJsonObject($output : any) : void {
            let property : string;
            for (property in this.value) {
                if (this.value.hasOwnProperty(property)) {
                    $output[property] = this.value[property].ToJson();
                }
            }
        }

        private processJsonPrimitive($type : string) : any {
            switch ($type) {
            case "boolean":
                return ObjectValidator.IsEmptyOrNull(this.formArgs) ? this.value : StringUtils.ToBoolean(this.formArgs.Value());
            case "integer":
                return ObjectValidator.IsEmptyOrNull(this.formArgs) ? this.value : StringUtils.ToInteger(this.formArgs.Value());
            case "number":
                return ObjectValidator.IsEmptyOrNull(this.formArgs) ? this.value : StringUtils.ToDouble(this.formArgs.Value());
            case "string":
                return ObjectValidator.IsEmptyOrNull(this.formArgs) ? this.value : this.formArgs.Value();
            default:
                return null;
            }
        }
    }
}
