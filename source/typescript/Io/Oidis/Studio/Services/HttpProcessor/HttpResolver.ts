/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.HttpProcessor {
    "use strict";
    import HttpRequestConstants = Io.Oidis.Commons.Enums.HttpRequestConstants;
    import EventsManager = Io.Oidis.UserControls.Events.EventsManager;
    import HttpManager = Io.Oidis.Services.HttpProcessor.HttpManager;

    export class HttpResolver extends Io.Oidis.Services.HttpProcessor.HttpResolver {

        public getEvents() : EventsManager {
            return <EventsManager>super.getEvents();
        }

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.Oidis.Studio.Services.Index;
            resolvers["/Installer"] = Io.Oidis.Studio.Services.Controllers.InstallerController;
            resolvers["/Designer"] = Io.Oidis.Studio.Services.Controllers.DesignerController;
            resolvers["/Dashboard"] = Io.Oidis.Studio.Services.Controllers.DashboardController;
            resolvers["/Settings"] = Io.Oidis.Studio.Services.Controllers.ProjectSettingsController;
            resolvers["/Settings/{port}"] = Io.Oidis.Studio.Services.Controllers.ProjectSettingsController;

            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                resolvers["/web/"] = Index;
                resolvers["/index"] = Io.Oidis.Studio.Services.Index;
            } else {
                resolvers["/web/"] = null;
                resolvers["/index"] = null;
                resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] =
                    Io.Oidis.Studio.Services.Controllers.ErrorPageController;
                resolvers["/ServerError/Http/NotFound"] = Io.Oidis.Studio.Services.Controllers.ErrorPageController;
                resolvers["/ServerError/Http/DefaultPage"] = Io.Oidis.Studio.Services.Controllers.ErrorPageController;
            }
            return resolvers;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }
    }
}
