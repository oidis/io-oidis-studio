/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services {
    "use strict";
    import StaticPageContentManger = Io.Oidis.Gui.Utils.StaticPageContentManager;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>WUI Studio Services Library</h1>" +
                "<h3>WUI Framework's library focused on services and business logic for WUI Studio</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Pages</H3>" +
                "<a href=\"#/Installer\">Installer</a>" +
                StringUtils.NewLine() +
                "<a href=\"#/Dashboard\">Dashboard</a>" +
                StringUtils.NewLine() +
                "<a href=\"#/Designer\">Designer</a>" +
                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +

                "<a href=\"" + RuntimeTests.CodeGeneratorRequestTest.CallbackLink() + "\">Code Generator Request test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.InstallationProcessTest.CallbackLink() + "\">Installation process test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.DirectoryBrowserTest.CallbackLink(true) + "\">Directory browser test</a>" +
                StringUtils.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI Studio Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
