/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Interfaces.DAO {
    "use strict";
    import IInstallationEnvironment = Io.Oidis.Services.Interfaces.DAO.IInstallationEnvironment;

    export abstract class IInstallationRecipe extends IInstallationEnvironment {
        public wuiPaths : IInstallationRecipeWuiPaths;
    }

    export abstract class IInstallationRecipeWuiPaths {
        public root : string;
        public workplace : string;
        public builder : string;
        public modules : IInstallationRecipeWuiModules;
    }

    export abstract class IInstallationRecipeWuiModules {
        public git : string;
        public nodejs : string;
    }
}
