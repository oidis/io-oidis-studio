/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Io.Oidis.Services.Interfaces.DAO.IBasePageLocalization;

    export abstract class IProjectSettingsLocalization extends IBasePageLocalization {
        public readonly save : string;
    }
}
