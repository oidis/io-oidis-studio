/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.Services {
    "use strict";
    import HttpResolver = Io.Oidis.Studio.Services.HttpProcessor.HttpResolver;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import HttpManager = Io.Oidis.Services.HttpProcessor.HttpManager;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    /**
     * Loader class provides handling of application content singleton.
     */
    export class Loader extends Io.Oidis.Services.Loader {
        private appLog : string = "";

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        constructor() {
            super();
            this.appLog = "";
            LogIt.setOnPrint(($message : string) : void => {
                this.appLog += $message;
            });
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getHttpManager() : HttpManager {
            return <HttpManager>super.getHttpManager();
        }

        public ApplicationLog($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.appLog = $value;
            }
            return this.appLog;
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver(this.getEnvironmentArgs().getProjectName());
        }
    }
}
