/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.Oidis.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ObjectDecoder = Io.Oidis.Commons.Utils.ObjectDecoder;
    import ErrorEventArgs = Io.Oidis.Commons.Events.Args.ErrorEventArgs;
    import BasePanel = Io.Oidis.UserControls.Primitives.BasePanel;
    import ElementManager = Io.Oidis.Gui.Utils.ElementManager;
    import TextField = Io.Oidis.UserControls.BaseInterface.UserControls.TextField;
    import IGuiElement = Io.Oidis.Gui.Interfaces.Primitives.IGuiElement;
    import GuiElement = Io.Oidis.Gui.Primitives.GuiElement;
    import DropDownList = Io.Oidis.UserControls.BaseInterface.UserControls.DropDownList;
    import Size = Io.Oidis.Gui.Structures.Size;
    import WindowManager = Io.Oidis.Gui.Utils.WindowManager;
    import ElementOffset = Io.Oidis.Gui.Structures.ElementOffset;
    import WuiBuilderConnector = Io.Oidis.Commons.Connectors.WuiBuilderConnector;
    import GuiBasePanel = Io.Oidis.Gui.Primitives.BasePanel;
    import IEventsHandler = Io.Oidis.Gui.Interfaces.IEventsHandler;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class CodeGeneratorRequestTest extends Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private connector : WuiBuilderConnector;

        public testCreateNewPanel() : void {
            const interfaceName : DropDownList = new DropDownList();
            interfaceName.Hint("specify interface name");
            interfaceName.Width(400);
            interfaceName.Add("BaseInterface");
            interfaceName.Add("MobileInterface");
            interfaceName.Select(0);

            const extendFrom : DropDownList = new DropDownList();
            extendFrom.Hint("specify parent panel");
            extendFrom.Width(400);
            extendFrom.Add(GuiBasePanel.ClassName());
            extendFrom.Add(BasePanel.ClassName());
            extendFrom.Select(1);

            const namespaceValue : TextField = new TextField();
            namespaceValue.Width(400);
            namespaceValue.Hint("specify relative namespace");
            namespaceValue.Value("NewPanel");

            const output : IGuiElement = this.addElement()
                .Add(interfaceName)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(extendFrom)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(namespaceValue)
                .Add(this.addElement().setAttribute("clear", "both"));

            Echo.Print(output.Draw(""));

            this.addButton("Create Panel", () : void => {
                this.connector.Send("CreateNewPanel", {
                    extends       : extendFrom.Value(),
                    interfaceName : interfaceName.Value(),
                    panelNamespace: namespaceValue.Value()
                }, this.responseHandler);
            });
        }

        public testEvents() : void {
            this.addButton("Add OnReloadTo listener", () : void => {
                this.connector.AddEventListener("ReloadTo", ($args? : any) : void => {
                    Echo.Printf("Fired OnReload event with args: {0}", $args);
                });
            });
        }

        public testNewProject() : void {
            const namespaceValue : TextField = new TextField();
            namespaceValue.Width(400);
            namespaceValue.Hint("specify namespace");
            namespaceValue.Value("Io.Oidis.Application");

            const extendFrom : DropDownList = new DropDownList();
            extendFrom.Hint("extends from");
            extendFrom.Width(400);
            extendFrom.Add("WUI Commons", 1);
            extendFrom.Add("WUI GUI", 2);
            extendFrom.Add("WUI User Controls", 3);
            extendFrom.Add("WUI Services", 4);
            extendFrom.Add("WUI REST Commons", 5);
            extendFrom.Add("WUI REST Services", 6);
            extendFrom.Add("WUI XCpp Commons", 7);
            extendFrom.Select(0);

            const path : TextField = new TextField();
            path.Width(400);
            path.Hint("specify path");
            path.Value("D:/__TEMP__");

            const name : TextField = new TextField();
            name.Width(400);
            name.Hint("specify name");
            name.Value("WUI Designer Template");

            const title : TextField = new TextField();
            title.Width(400);
            title.Hint("specify application title");
            title.Value("WUI Designer - Template");

            const description : TextField = new TextField();
            description.Width(400);
            description.Hint("specify application description");
            description.Value("WUI Framework's Template generated by WUI Designer tool");

            const repo : TextField = new TextField();
            repo.Width(400);
            repo.Hint("specify GIT repository");
            repo.Value("https://gitlab.com/oidis/io-oidis-application.git");

            const output : IGuiElement = this.addElement()
                .Add(namespaceValue)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(extendFrom)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(path)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(name)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(title)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(description)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(repo)
                .Add(this.addElement().setAttribute("clear", "both"))
                .Add(this.addElement("OnResolverBackground")
                    .Visible(false)
                    .setAttribute("background-color", "black")
                    .setAttribute("opacity", "0.50")
                    .setAttribute("position", "fixed")
                    .setAttribute("top", "0")
                    .setAttribute("left", "0")
                    .setAttribute("z-index:", "10000")
                    .setAttribute("width", "100%")
                    .setAttribute("height", "1000px")
                )
                .Add(this.addElement("OnResolverConsole")
                    .Visible(false)
                    .setAttribute("border", "1px solid black")
                    .setAttribute("background-color", "silver")
                    .setAttribute("color", "black")
                    .setAttribute("font-size", "10px")
                    .setAttribute("font-family", "Consolas, serif;")
                    .setAttribute("overflow", "auto")
                    .setAttribute("position", "fixed")
                    .setAttribute("top", "9px")
                    .setAttribute("left", "9px")
                    .setAttribute("z-index:", "10000")
                    .setAttribute("padding", "10px")
                );

            Echo.Print(output.Draw(""));

            const updateConsoleSize : () => void = () : void => {
                const size : Size = WindowManager.getSize();
                ElementManager.setSize("OnResolverBackground", size.Width(), size.Height());
                ElementManager.setSize("OnResolverConsole", size.Width() - 40, size.Height() - 40);
            };

            const hideConsole : IEventsHandler = () : void => {
                ElementManager.Hide("OnResolverBackground");
                ElementManager.Hide("OnResolverConsole");
                this.connector.RemoveEventListener("OnResolverStart");
                this.connector.RemoveEventListener("OnResolverMessage");
            };

            this.connector.getEvents().OnError(hideConsole);

            this.addButton("Init Project", () : void => {
                this.connector.AddEventListener("OnResolverStart", () : void => {
                    ElementManager.Show("OnResolverBackground");
                    ElementManager.Show("OnResolverConsole");
                    updateConsoleSize();
                });

                this.connector.AddEventListener("OnConsoleData", ($data : string) : void => {
                    ElementManager.AppendHtml("OnResolverConsole", $data);
                    ElementManager.getElement("OnResolverConsole").scrollTop = ElementManager.getElement("OnResolverConsole").scrollHeight;
                });

                this.connector.Send("ProjectInit", {
                        description: description.Value(),
                        extends    : extendFrom.Value(),
                        name       : name.Value(),
                        namespace  : namespaceValue.Value(),
                        path       : path.Value(),
                        repository : repo.Value(),
                        title      : title.Value()
                    },
                    ($data : any) : void => {
                        hideConsole();
                        this.responseHandler($data);
                    });
            });
        }

        protected before() : void {
            this.connector = WuiBuilderConnector.Connect();

            this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
                const output : IGuiElement = this.addElement()
                    .setAttribute("color", "red")
                    .setAttribute("text-align", "center")
                    .setAttribute("border", "1px solid red")
                    .setAttribute("padding", "10px")
                    .Add($args.Message());
                Echo.Print(output.Draw(""));
            });

            const updateMessagePosition : () => void = () : void => {
                // const size : Size = WindowManager.getSize();
                // ElementManager.setSize("OnBuildBackground", size.Width(), size.Height());
                // const messageSize : Size = new Size(ElementManager.getElement("OnBuildMessage"));
                // const position : ElementOffset = new ElementOffset(
                //     (size.Height() - messageSize.Height()) / 2,
                //     (size.Width() - messageSize.Width()) / 2
                // );
                // ElementManager.setPosition("OnBuildMessage", position);
            };

            this.connector.getEvents().OnStart(() : void => {
                this.connector.getEvents().OnBuildStart(() : void => {
                    ElementManager.Show("OnBuildBackground");
                    ElementManager.Show("OnBuildMessage");
                    updateMessagePosition();
                });

                this.connector.getEvents().OnFail(($message : string) : void => {
                    ElementManager.Hide("OnBuildBackground");
                    ElementManager.Hide("OnBuildMessage");
                    const output : IGuiElement = this.addElement()
                        .setAttribute("color", "red")
                        .setAttribute("text-align", "center")
                        .setAttribute("border", "1px solid red")
                        .setAttribute("padding", "10px")
                        .Add($message);
                    Echo.Print(output.Draw(""));
                });

                this.connector.getEvents().OnWarning(($message : string) : void => {
                    ElementManager.AppendHtml("OnBuildMessage", StringUtils.NewLine() + $message);
                    updateMessagePosition();
                });
            });

            const output : IGuiElement = this.addElement()
                .Add(this.addElement("OnBuildBackground")
                    .Visible(false)
                    .setAttribute("background-color", "black")
                    .setAttribute("opacity", "0.50")
                    .setAttribute("position", "fixed")
                    .setAttribute("top", "0")
                    .setAttribute("left", "0")
                    .setAttribute("z-index:", "10000")
                    .setAttribute("width", "100%")
                    .setAttribute("height", "1000px")
                )
                .Add(this.addElement("OnBuildMessage")
                    .Visible(false)
                    .setAttribute("background-color", "black")
                    .setAttribute("color", "white")
                    .setAttribute("position", "fixed")
                    .setAttribute("top", "0")
                    .setAttribute("left", "0")
                    .setAttribute("z-index:", "10000")
                    .setAttribute("float", "left")
                    .setAttribute("padding", "10px")
                    .Add("Build in progress, please wait ...")
                );

            Echo.Print(output.Draw(""));
        }

        private responseHandler($data : any) : void {
            Echo.Printf("{0} {1}", $data.type, ObjectDecoder.Base64($data.data));
        }

        private addElement($id? : string) : IGuiElement {
            return new GuiElement().Id($id);
        }
    }
}
/* dev:end */
