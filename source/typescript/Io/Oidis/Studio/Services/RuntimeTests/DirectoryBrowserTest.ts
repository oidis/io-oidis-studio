/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.Oidis.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import FileSystemHandlerConnector = Io.Oidis.Services.Connectors.FileSystemHandlerConnector;
    import DirectoryBrowserEventArgs = Io.Oidis.Gui.Events.Args.DirectoryBrowserEventArgs;
    import IFileSystemItemProtocol = Io.Oidis.Commons.Interfaces.IFileSystemItemProtocol;
    import DirectoryBrowserViewer = Io.Oidis.UserControls.BaseInterface.Viewers.UserControls.DirectoryBrowserViewer;
    import DirectoryBrowser = Io.Oidis.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DirectoryBrowserTest extends DirectoryBrowserViewer {

        /* dev:start */
        protected testImplementation($instance? : DirectoryBrowser) : void {
            const connector : FileSystemHandlerConnector = new FileSystemHandlerConnector();
            $instance.Width(800);
            $instance.Height(500);
            if (this.getHttpManager().getRequest().IsOnServer(true)) {
                $instance.Path("/com-wui-framework-rest-services/resource/data");
                // $instance.Path("/com-wui-framework-rest-services/resource/data/temp");
            } else {
                $instance.Path("C:/");
                // $instance.Path("C:/Windows");
                // $instance.Path("C:/Windows/System32/temp");
            }

            $instance.getEvents().setOnChange(($eventArgs : DirectoryBrowserEventArgs) : void => {
                Echo.Printf("selected item: {0}", $eventArgs.Value());
            });

            $instance.getEvents().setOnDirectoryRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
                connector.getDirectoryContent($eventArgs.Value()).Then(($map : IFileSystemItemProtocol[]) : void => {
                    $instance.setStructure($map, $eventArgs.Value());
                });
            });

            $instance.getEvents().setOnPathRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
                connector.getPathMap($eventArgs.Value()).Then(($map : IFileSystemItemProtocol[]) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($map)) {
                        Echo.Printf($map);
                        $instance.setStructure($map);
                    }
                });
            });

            $instance.getEvents().setOnDeleteRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
                const path : string = $eventArgs.Owner().Value();
                const directory : string = StringUtils.Substring(path, 0, StringUtils.IndexOf(path, "/", false));

                connector.Delete(path).Then(($status : boolean) : void => {
                    if ($status) {
                        connector.getDirectoryContent(directory).Then(($map : IFileSystemItemProtocol[]) : void => {
                            $instance.setStructure($map, directory);
                        });
                    } else {
                        Echo.Printf("Unable to delete directory: {0}", $eventArgs.Owner().Value());
                    }
                });
            });

            $instance.getEvents().setOnCreateDirectoryRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
                connector.CreateDirectory($eventArgs.Owner().Value() + "/New Folder").Then(($status : boolean) : void => {
                    if ($status) {
                        connector.getDirectoryContent($eventArgs.Owner().Value()).Then(($map : IFileSystemItemProtocol[]) : void => {
                            $instance.setStructure($map, $eventArgs.Owner().Value());
                        });
                    } else {
                        Echo.Printf("Unable to create directory: {0}", $eventArgs.Owner().Value());
                    }
                });
            });

            $instance.getEvents().setOnRenameRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
                const oldValue : string = $eventArgs.Owner().Value();
                const newValue : string =
                    StringUtils.Substring(oldValue, 0, StringUtils.IndexOf(oldValue, "/", false)) + "/" + $eventArgs.Value();
                const directory : string = StringUtils.Substring(oldValue, 0, StringUtils.IndexOf(oldValue, "/", false));

                connector.Rename(oldValue, newValue).Then(($status : boolean) : void => {
                    if ($status) {
                        connector.getDirectoryContent(directory).Then(($map : IFileSystemItemProtocol[]) : void => {
                            $instance.setStructure($map, directory);
                        });
                    } else {
                        Echo.Printf("Unable to rename: {0}", $eventArgs.Owner().Value());
                    }
                });
            });
        }
    }
}
/* dev:end */
