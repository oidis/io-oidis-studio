/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.Oidis.Studio.Services.RuntimeTests {
    "use strict";
    import IInstallationRecipe = Io.Oidis.Studio.Services.Interfaces.DAO.IInstallationRecipe;
    import IInstallationRecipeWuiPaths = Io.Oidis.Studio.Services.Interfaces.DAO.IInstallationRecipeWuiPaths;
    import IRuntimeTestPromise = Io.Oidis.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class InstallationProcessTest extends Io.Oidis.Services.RuntimeTests.InstallationProcessTest {

        constructor() {
            super();
            this.setScript("resource/data/Io/Oidis/Studio/Services/Configuration/WuiFrameworkWindows.jsonp");
            this.setChain([
                // "Netfx4Full",
                "VCRedist2005x86",
                "VCRedist2008x86",
                "VCRedist2012x86",
                "VCRedist2015x86"
                // "Git",
                // "NodeJs",
                // "Java",
                // "Chrome",
                // "WUIBuilder",
                // "WUIDashboard"
            ], true);
        }

        protected before() : IRuntimeTestPromise {
            const promise : IRuntimeTestPromise = super.before();
            this.getDao().getEvents().setOnStart(() : void => {
                const wuiPaths : IInstallationRecipeWuiPaths = (<IInstallationRecipe>this.getDao().getStaticConfiguration()).wuiPaths;
                if (this.getRequest().IsWuiJre()) {
                    wuiPaths.root = "C:/WUIFramework";
                    wuiPaths.workplace = "C:/WUIFramework/Projects";
                } else {
                    wuiPaths.root = "D:/__TEMP__/WUIFramework";
                    wuiPaths.workplace = "D:/__TEMP__/WUIFramework/Projects";
                }
            });
            return promise;
        }
    }
}
/* dev:end */
