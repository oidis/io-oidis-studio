/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class ImageButtonType extends Io.Oidis.Commons.Primitives.BaseEnum {
        public static readonly ROUND : string = "Round";
    }
}
