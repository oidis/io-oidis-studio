/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.UserControls {
    "use strict";
    import ButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Property = Io.Oidis.Commons.Utils.Property;

    export class Button extends Io.Oidis.UserControls.BaseInterface.UserControls.Button {

        constructor($buttonType? : ButtonType, $id? : string) {
            super($buttonType, $id);
        }

        public GuiType($buttonType? : ButtonType) : ButtonType {
            return <ButtonType>super.GuiType($buttonType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ButtonType, ButtonType.GRAY_MEDIUM);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of button type instead of StyleClassName method.");
            return false;
        }
    }
}
