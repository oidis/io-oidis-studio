/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.UserControls {
    "use strict";
    import IconType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Property = Io.Oidis.Commons.Utils.Property;

    export class Icon extends Io.Oidis.UserControls.BaseInterface.UserControls.Icon {

        constructor($iconType? : IconType, $id? : string) {
            super($iconType, $id);
        }

        public IconType($type? : IconType) : IconType {
            return <IconType>super.IconType($type);
        }

        protected iconTypeValueSetter($value : any) : any {
            return Property.EnumType(this.IconType(), $value, IconType, IconType.MINUS);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!IconType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use IconType method for set of icon type instead of StyleClassName method.");
            return false;
        }
    }
}
