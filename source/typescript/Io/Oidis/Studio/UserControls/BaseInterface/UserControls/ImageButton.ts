/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.UserControls {
    "use strict";
    import ImageButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import Property = Io.Oidis.Commons.Utils.Property;

    export class ImageButton extends Io.Oidis.UserControls.BaseInterface.UserControls.ImageButton {

        constructor($imageButtonType? : ImageButtonType, $id? : string) {
            super($imageButtonType, $id);
        }

        public GuiType($imageButtonType? : ImageButtonType) : ImageButtonType {
            return <ImageButtonType>super.GuiType($imageButtonType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ImageButtonType, ImageButtonType.ROUND);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ImageButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of image button type instead of StyleClassName method.");
            return false;
        }
    }
}
