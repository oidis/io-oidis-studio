/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Io.Oidis.Gui.Primitives.BaseViewerArgs;
    import Button = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ButtonType;

    export class ButtonViewer extends Io.Oidis.UserControls.BaseInterface.Viewers.UserControls.ButtonViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Button(ButtonType.GRAY_MEDIUM));
        }

        public getInstance() : Button {
            return <Button>super.getInstance();
        }
    }
}
