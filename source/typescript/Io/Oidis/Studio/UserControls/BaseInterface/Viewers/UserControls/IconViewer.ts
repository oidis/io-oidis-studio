/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Io.Oidis.Gui.Primitives.BaseViewerArgs;
    import Icon = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.Icon;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import IconType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.IconType;
    import GuiElement = Io.Oidis.Gui.Primitives.GuiElement;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class IconViewer extends Io.Oidis.UserControls.BaseInterface.Viewers.UserControls.IconViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Icon());
        }

        public getInstance() : Icon {
            return <Icon>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            this.setInstance(null);
            const output : GuiElement = new GuiElement();
            IconType.getProperties().forEach(($type : string) : void => {
                $type = IconType[$type];
                const icon : Icon = new Icon($type);
                icon.StyleClassName("testCssClass");
                output
                    .Add(StringUtils.NewLine() + $type + "<hr>")
                    .Add(icon);
            });
            return "" +
                "<style>" +
                "body{overflow: auto; margin: inherit; padding-left: 10px;} " +
                ".testCssClass {background-color: #64FF00; padding: 40px; float: left; clear: both;}" +
                "</style>" +
                output.Draw(StringUtils.NewLine(false));
        }

        /* dev:end */
    }
}
