/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Io.Oidis.Gui.Primitives.BaseViewerArgs;
    import ImageButton = Io.Oidis.Studio.UserControls.BaseInterface.UserControls.ImageButton;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import ImageButtonType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Io.Oidis.Studio.UserControls.BaseInterface.Enums.UserControls.IconType;

    export class ImageButtonViewer extends Io.Oidis.UserControls.BaseInterface.Viewers.UserControls.ImageButtonViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new ImageButton(ImageButtonType.ROUND));
        }

        public getInstance() : ImageButton {
            return <ImageButton>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : ImageButton = this.getInstance();
            object.StyleClassName("testCssClass");
            object.IconName(IconType.ARROW_LEFT);

            return "" +
                "<style>" +
                "body{overflow: auto; margin: inherit; padding-left: 10px;} " +
                ".testCssClass {background-color: #64FF00; padding: 40px; float: left; clear: both;}" +
                "</style>";
        }

        /* dev:end */
    }
}
