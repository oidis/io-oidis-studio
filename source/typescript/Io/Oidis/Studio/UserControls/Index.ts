/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015-2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio.UserControls {
    "use strict";
    import StaticPageContentManger = Io.Oidis.Gui.Utils.StaticPageContentManager;
    import Viewers = Io.Oidis.Studio.UserControls.BaseInterface.Viewers;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>Vision SDK UserControls Library</h1>" +
                "<h3>WUI Framework library focused on user controls with design for Vision SDK.</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Components</H3>" +

                "<H3>User Controls</H3>" +
                "<a href=\"" + Viewers.UserControls.IconViewer.CallbackLink(true) + "\">Icon</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ImageButtonViewer.CallbackLink(true) + "\">ImageButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ButtonViewer.CallbackLink(true) + "\">Button</a>" +
                StringUtils.NewLine() +

                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +

                StringUtils.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI Studio - UserControls Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
