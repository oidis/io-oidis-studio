/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.Oidis.Studio.Services.Connectors {
    "use strict";
    export let IProjectSchemaPromise : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Io.Oidis.Studio.Services.Connectors {
    "use strict";
    export let IProjectConfigPromise : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    export let IInstallUnistallPromise : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    export let ISelfinstallPromise : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "OnValidate",
                "OnChange",
                "Then"
            ]);
        }();
}

namespace Io.Oidis.Studio.Services.Controllers {
    "use strict";
    export let IGetVersionPromise : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "getVersion",
                "readCmd",
                "Then"
            ]);
        }();
}

/* tslint:enable */
