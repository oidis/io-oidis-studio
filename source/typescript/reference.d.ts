/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-usercontrols/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-services/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="interfacesMap.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Enums/Panels/DesignerComponentType.ts" />
/// <reference path="Io/Oidis/Studio/Services/DAO/Resources.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Enums/UserControls/ButtonType.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Enums/UserControls/IconType.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Enums/UserControls/ImageButtonType.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Enums/Panels/DashboardPanelLayerType.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Enums/Panels/DesignerPanelLayerType.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/LocalhostInfoPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/ProjectsManagerPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/ToolchainInfoPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/ProjectInitSettingsPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerComponentPickerPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerConsolePanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerFooterPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerHeaderPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerHierarchyPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerPropertiesPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Installer/InstallerInitialScreenPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/Oidis/Studio/Services/Interfaces/DAO/IInstallationRecipe.ts" />
/// <reference path="Io/Oidis/Studio/Services/Interfaces/DAO/IProjectSettingsLocalization.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/Loader.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/DashboardPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerComponentsPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Designer/DesignerIndexPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Services/Index.ts" />
/// <reference path="Io/Oidis/Studio/Gui/Loader.ts" />
/// <reference path="Io/Oidis/Studio/Services/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/UserControls/Button.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/UserControls/ImageButton.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/UserControls/Icon.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/ViewersArgs/Panels/Dashboard/ProjectSettingsPanelViewerArgs.ts" />
/// <reference path="Io/Oidis/Studio/Services/RuntimeTests/InstallationProcessTest.ts" />
/// <reference path="Io/Oidis/Studio/Services/Connectors/DashboardConnector.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Viewers/UserControls/ButtonViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerFooterPanel.ts" />
/// <reference path="Io/Oidis/Studio/Services/Loader.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Viewers/UserControls/ImageButtonViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/ProjectInitSettingsPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerFooterPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/BaseInterface/Viewers/UserControls/IconViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/ToolchainInfoPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/ProjectInitSettingsPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/RuntimeTests/DirectoryBrowserTest.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/LocalhostInfoPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerConsolePanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/ToolchainInfoPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerHeaderPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/LocalhostInfoPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerConsolePanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Installer/InstallerInitialScreenPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/ProjectsManagerPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerComponentPickerPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerHierarchyPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerIndexPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerHeaderPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Installer/InstallerInitialScreenPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/ProjectSettingsPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/ProjectsManagerPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerComponentPickerPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerHierarchyPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerIndexPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/ErrorPageController.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/ProjectSettingsPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/RuntimeTests/CodeGeneratorRequestTest.ts" />
/// <reference path="Io/Oidis/Studio/UserControls/Index.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerPropertiesPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerPropertiesPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerComponentsPanel.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/BasePageController.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerComponentsPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/DAO/Pages/ProjectSettingsDAO.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Dashboard/DashboardPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Dashboard/DashboardPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/ProjectSettingsController.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/InstallerController.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/DashboardController.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Panels/Designer/DesignerPanel.ts" />
/// <reference path="Io/Oidis/Studio/Gui/BaseInterface/Viewers/Panels/Designer/DesignerPanelViewer.ts" />
/// <reference path="Io/Oidis/Studio/Services/Controllers/DesignerController.ts" />
/// <reference path="Io/Oidis/Studio/Gui/Index.ts" />
// generated-code-end
