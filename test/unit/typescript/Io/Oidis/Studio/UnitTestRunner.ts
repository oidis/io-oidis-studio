/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Jakub Cieslar
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.Oidis.Studio {
    "use strict";
    import UnitTestLoader = Io.Oidis.Commons.UnitTestLoader;

    export class UnitTestRunner extends Io.Oidis.UnitTestRunner {
        protected initLoader() : void {
            super.initLoader(UnitTestLoader);
        }
    }
}
