/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* tslint:disable:no-reference */
///<reference path='../../../dependencies/com-wui-framework-commons/test/unit/typescript/reference.d.ts'/>
///<reference path='../../../source/typescript/reference.d.ts'/>
///<reference path='Io/Oidis/Studio/UnitTestRunner.ts'/>
